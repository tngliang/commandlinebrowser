﻿namespace Test
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    
    using CommandLineBrowser;
    using Fidelity;

    class Program
    {
        static void Main(string[] args)
        {
            using (HttpSession httpSession = new HttpSession(true, "..\\..\\.."))
            {
                Session session = new Session(null, null, httpSession);
                AccountSummary summary = (AccountSummary)session.Login();
                TradeStockETFs tradePage = (TradeStockETFs)session.Navigate(summary.RetirementAccounts[Session.RegRead(Key.ROTH)]["stockInit"]);
                
                OrderVerification verification;
                TradeStockETFsConfirm confirmation;

                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "AAXJ", 27, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "ACWI", 98, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "ACWX", 92, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "BKF", 34, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IDV", 163, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IFGL", 75, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IGOV", 11, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJH", 24, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJJ", 47, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJK", 13, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJR", 34, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJS", 54, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IJT", 3, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "ILF", 105, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IVE", 82, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IWC", 77, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IWD", 77, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "SCZ", 21, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
               
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "AGG", 41, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "AGZ", 46, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "EWJ", 277, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "GVI", 55, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "HYG", 32, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission == 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IEF", 36, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "ITOT", 30, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IVV", 17, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IVW", 71, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IYR", 13, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "LQD", 72, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "MBB", 7, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "OEF", 26, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "PFF", 110, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "SHY", 15, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "SUB", 8, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "TIP", 49, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "TLT", 236, TradeStockETFs.OrderType.Market, false));
                if (verification.EstimatedCommission <= 0.0)
                    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
               
                //OptionChain optionChain = OptionChain.Create(session);
                //OptionChain msftOptionChain = (OptionChain)session.Navigate(optionChain.Search("MSFT", true));
                //foreach (StockOption option in msftOptionChain.Options)
                //{
                //    Console.WriteLine("{0} {1} {2} {3}", option.Symbol, option.Type.ToString(), option.Strike, option.Expiration.ToString(), option.Underlying.Name);
                //}
            }
        }
    }
}


                //OrderVerification verification;
                //TradeStockETFsConfirm confirmation;

                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "AGG", 40, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "AGZ", 28, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "CMF", 70, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "DVY", 94, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "EMB", 20, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "GVI", 30, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "HYG", 47, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IEF", 95, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IFGL", 31, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IGOV", 21, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "ISHG", 67, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IVW", 23, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IWD", 9, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "IYR", 35, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "LQD", 70, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "PFF", 146, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "SHY", 11, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "TIP", 38, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Sell, "TLT", 176, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());

                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "AAXJ", 27, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "ACWI", 98, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "ACWX", 92, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "BKF", 34, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "EEM", 54, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission == 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "EFA", 80, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "EWJ", 44, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IDV", 163, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IEF", 84, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJH", 30, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJJ", 43, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJK", 23, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJR", 64, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJS", 54, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IJT", 56, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "ILF", 105, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "ITOT", 80, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IVE", 32, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IVV", 7, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWB", 26, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWC", 77, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWD", 9, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWF", 54, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWM", 58, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWN", 66, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWO", 47, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IWV", 28, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "OEF", 30, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
                //verification = (OrderVerification)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "SCZ", 158, TradeStockETFs.OrderType.Market, false));
                //if (verification.EstimatedCommission <= 0.0)
                //    confirmation = (TradeStockETFsConfirm)session.Navigate(verification.Confirm());
