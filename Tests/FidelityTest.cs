﻿namespace CommandLineBrowserTest
{
    using System;
    using System.Text;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using CommandLineBrowser;
    using Fidelity;

    /// <summary>
    /// Summary description for FidelityTest
    /// </summary>
    [TestClass]
    public class FidelityTest
    {
        public FidelityTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void AccountSummaryPageTest()
        {
            using (HttpSession session = new HttpSession(true, "..\\..\\.."))
            {
                Session loginSession = new Session(null, null, session);
                AccountSummary summaryPage = (AccountSummary)loginSession.Login();
                Assert.IsNotNull(summaryPage);

                IDictionary<string, Account> investmentAccounts = summaryPage.InvestmentAccounts;
                Assert.IsNotNull(investmentAccounts);
                Assert.IsNotNull(investmentAccounts[Session.RegRead(Key.Taxable)]);
                Assert.AreEqual(Session.RegRead(Key.TaxableAcctNum), investmentAccounts[Session.RegRead(Key.Taxable)].Number);
                Assert.AreEqual("Brokerage", investmentAccounts[Session.RegRead(Key.Taxable)].Type);

                IDictionary<string, Account> retirementAccounts = summaryPage.RetirementAccounts;
                Assert.IsNotNull(retirementAccounts);
                Assert.IsNotNull(retirementAccounts[Session.RegRead(Key.IRA)]);
                Assert.AreEqual(Session.RegRead(Key.IRAAcctNum), retirementAccounts[Session.RegRead(Key.IRA)].Number);
                Assert.AreEqual("Brokerage", retirementAccounts[Session.RegRead(Key.IRA)].Type);
                Assert.AreEqual(Session.RegRead(Key.ROTHAcctNum), retirementAccounts[Session.RegRead(Key.ROTH)].Number);
                Assert.AreEqual("Brokerage", retirementAccounts[Session.RegRead(Key.ROTH)].Type);
            }
        }

        [TestMethod]
        public void AccountBalancePageTest()
        {
            using (HttpSession session = new HttpSession(true, "..\\..\\.."))
            {
                Session loginSession = new Session(null, null, session);

                AccountSummary summary = (AccountSummary)loginSession.Login();
                Assert.IsNotNull(summary);

                AccountBalance individualBalance = (AccountBalance)loginSession.Navigate(summary.InvestmentAccounts[Session.RegRead(Key.Taxable)]["BrokerageBalances"]);
                Assert.IsNotNull(individualBalance);

                Assert.IsFalse(double.IsNaN(individualBalance.TotalAccountValue));
                Assert.IsFalse(double.IsNaN(individualBalance.CashCore));
                Assert.IsFalse(double.IsNaN(individualBalance.CashCreditDebit));
                Assert.IsFalse(double.IsNaN(individualBalance.MarginCreditDebit));
                Assert.IsFalse(double.IsNaN(individualBalance.ShortCredit));
                Assert.IsFalse(double.IsNaN(individualBalance.HeldInCash));
                Assert.IsFalse(double.IsNaN(individualBalance.HeldInMargin));
                Assert.IsFalse(double.IsNaN(individualBalance.HeldShort));
                Assert.IsFalse(double.IsNaN(individualBalance.HeldInOptions));
                Assert.IsFalse(double.IsNaN(individualBalance.MarginBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.NonMarginBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.CommittedToOpenOrders));
                Assert.IsFalse(double.IsNaN(individualBalance.AvailableToWithdrawCashOnly));
                Assert.IsFalse(double.IsNaN(individualBalance.AvailableToWithdrawCashAndBorrowingOnMargin));
                Assert.IsFalse(double.IsNaN(individualBalance.MarginEquity));
                Assert.IsFalse(double.IsNaN(individualBalance.MarginEquityPercentage));
                Assert.IsFalse(double.IsNaN(individualBalance.HouseSurplus));
                Assert.IsFalse(double.IsNaN(individualBalance.ExchangeSurplus));
                Assert.IsFalse(double.IsNaN(individualBalance.SMA));
                Assert.IsFalse(double.IsNaN(individualBalance.CashBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.SettledCash));
                Assert.IsFalse(double.IsNaN(individualBalance.CorporateBondsBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.MunicipalBondsBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.GovernmentBondsBuyingPower));
                Assert.IsFalse(double.IsNaN(individualBalance.OptionsInTheMoney));
                Assert.IsFalse(double.IsNaN(individualBalance.CashCoveredPutReserve));

                AccountBalance rolloverIRABalance = (AccountBalance)loginSession.Navigate(summary.RetirementAccounts[Session.RegRead(Key.IRA)]["BrokerageBalances"]);
                Assert.IsNotNull(rolloverIRABalance);

                Assert.IsFalse(double.IsNaN(rolloverIRABalance.TotalAccountValue));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.CashCore));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.CashCreditDebit));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.HeldInCash));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.HeldInOptions));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.CommittedToOpenOrders));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.SettledCash));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.OptionsInTheMoney));
                Assert.IsFalse(double.IsNaN(rolloverIRABalance.CashCoveredPutReserve));

                AccountBalance rothIRABalance = (AccountBalance)loginSession.Navigate(summary.RetirementAccounts[Session.RegRead(Key.ROTH)]["BrokerageBalances"]);
                Assert.IsNotNull(rothIRABalance);

                Assert.IsFalse(double.IsNaN(rothIRABalance.TotalAccountValue));
                Assert.IsFalse(double.IsNaN(rothIRABalance.CashCore));
                Assert.IsFalse(double.IsNaN(rothIRABalance.HeldInCash));
                Assert.IsFalse(double.IsNaN(rothIRABalance.HeldInOptions));
                Assert.IsFalse(double.IsNaN(rothIRABalance.CommittedToOpenOrders));
                Assert.IsFalse(double.IsNaN(rothIRABalance.AvailableToWithdrawCashOnly));
                Assert.IsFalse(double.IsNaN(rothIRABalance.SettledCash));
                Assert.IsFalse(double.IsNaN(rothIRABalance.OptionsInTheMoney));
                Assert.IsFalse(double.IsNaN(rothIRABalance.CashCoveredPutReserve));
            }
        }

        [TestMethod]
        public void AccountPositionPageTest()
        {
            using (HttpSession session = new HttpSession(true, "..\\..\\.."))
            {
                Session loginSession = new Session(null, null, session);
                
                AccountSummary summary = (AccountSummary)loginSession.Login();
                Assert.IsNotNull(summary);

                AccountPosition individualPage = (AccountPosition)loginSession.Navigate(summary.InvestmentAccounts[Session.RegRead(Key.Taxable)]["brokerageAccountPositions"]);
                Assert.IsNotNull(individualPage);
                foreach (KeyValuePair<string, Position> pair in individualPage.GetPositions())
                {
                    Position position = pair.Value;
                    Assert.IsNotNull(position);
                    Assert.IsNotNull(position.Symbol);
                    Assert.IsFalse(double.IsNaN(position.Quantity));
                    Assert.IsFalse(double.IsNaN(position.Price));
                    Assert.IsFalse(double.IsNaN(position.Value));
                }

                AccountPosition rolloverIRAPage = (AccountPosition)loginSession.Navigate(summary.RetirementAccounts[Session.RegRead(Key.IRA)]["brokerageAccountPositions"]);
                Assert.IsNotNull(rolloverIRAPage);
                foreach (KeyValuePair<string, Position> pair in rolloverIRAPage.GetPositions())
                {
                    Position position = pair.Value;
                    Assert.IsNotNull(position);
                    Assert.IsNotNull(position.Symbol);
                    Assert.IsFalse(double.IsNaN(position.Quantity));
                    Assert.IsFalse(double.IsNaN(position.Price));
                    Assert.IsFalse(double.IsNaN(position.Value));
                }

                AccountPosition rothIRAPage = (AccountPosition)loginSession.Navigate(summary.RetirementAccounts[Session.RegRead(Key.ROTH)]["brokerageAccountPositions"]);
                Assert.IsNotNull(rothIRAPage);
                foreach (KeyValuePair<string, Position> pair in rothIRAPage.GetPositions())
                {
                    Position position = pair.Value;
                    Assert.IsNotNull(position);
                    Assert.IsNotNull(position.Symbol);
                    Assert.IsFalse(double.IsNaN(position.Quantity));
                    Assert.IsFalse(double.IsNaN(position.Price));
                    Assert.IsFalse(double.IsNaN(position.Value));
                }
            }
        }

        [TestMethod]
        public void TradeStockETFsPageTest()
        {
            using (HttpSession httpSession = new HttpSession(true, "..\\..\\.."))
            {
                Session session = new Session(null, null, httpSession);
                
                AccountSummary summary = (AccountSummary)session.Login();
                Assert.IsNotNull(summary);

                TradeStockETFs tradePage = (TradeStockETFs)session.Navigate(summary.InvestmentAccounts[Session.RegRead(Key.Taxable)]["stockInit"]);
                Assert.IsNotNull(tradePage);

                TradeMessage errorMessage = (TradeMessage)session.Navigate(tradePage.Trade(TradeStockETFs.TradeAction.Buy, "IVV", 1, TradeStockETFs.OrderType.Limit, false, 0.00));
                Assert.IsNotNull(errorMessage);
                Assert.IsNotNull(errorMessage.Message);
            }
        }

        [TestMethod]
        public void OrderDetailsPageTest()
        {
            using (HttpSession httpSession = new HttpSession(true, "..\\..\\.."))
            {
                Session session = new Session(null, null, httpSession);

                AccountSummary summary = (AccountSummary)session.Login();
                Assert.IsNotNull(summary);

                OrderDetails orderDetails = (OrderDetails)session.Navigate(summary.InvestmentAccounts[Session.RegRead(Key.Taxable)]["orderStatus"]);
                foreach (OrderStatus status in orderDetails.OrderStatuses)
                {
                    Assert.IsNotNull(status.OrderNumber);
                    Assert.IsNotNull(status.Description);
                }
            }
        }

        [TestMethod]
        public void GetActiveQuoteTest()
        {
            using (HttpSession httpSession = new HttpSession(true, "..\\..\\.."))
            {
                Session session = new Session(null, null, httpSession);
                ActiveQuote quote = new ActiveQuote("IVV", session);
                Assert.IsNotNull(quote);
                Assert.IsTrue(quote.Links.Count > 0);
                Assert.IsTrue(quote.Quotes.Count > 0);
                Assert.IsTrue(quote.Quotes.ContainsKey("Date"));
                Assert.IsFalse(double.IsNaN((double)quote["Bid"]));
                Assert.IsFalse(double.IsNaN((double)quote["Ask"]));
            }
        }

        [TestMethod]
        public void GetOptionsTest()
        {
            using (HttpSession httpSession = new HttpSession(true, "..\\..\\.."))
            {
                Session session = new Session(null, null, httpSession);
                AccountSummary summary = (AccountSummary)session.Login();
                Assert.IsNotNull(summary);
                OptionChain optionChain = OptionChain.Create(session);
                Assert.IsNotNull(optionChain);
                
                OptionChain msftOptionChain = (OptionChain)session.Navigate(optionChain.Search("MSFT", true));
                Assert.IsNotNull(msftOptionChain);
                HashSet<StockOption> msftOptions = msftOptionChain.Options;
                Assert.IsNotNull(msftOptions);
                Assert.IsTrue(msftOptions.Count > 0);
                foreach (StockOption option in msftOptions)
                {
                    Assert.IsNotNull(option.Symbol);
                    Assert.IsFalse(double.IsNaN(option.Strike));
                    Assert.IsFalse(option.Expiration < DateTime.Today);
                    Assert.IsFalse(option.Quantity < 0 || 100 < option.Quantity);
                    Assert.IsFalse(double.IsNaN(option.Last));
                    Assert.IsFalse(double.IsNaN(option.Change));
                    Assert.IsFalse(double.IsNaN(option.Bid));
                    Assert.IsFalse(double.IsNaN(option.Ask));
                    Assert.IsFalse(option.Volume < 0);
                    Assert.IsFalse(option.OpenInterest < 0);
                    Assert.IsFalse(option.ImpliedVolatility < 0);
                    Assert.IsFalse(double.IsNaN(option.Delta));
                    Assert.IsFalse(option.Type == OptionType.Call && option.Delta < 0);
                    Assert.IsFalse(option.Type == OptionType.Put && option.Delta > 0);
                    Assert.IsFalse(double.IsNaN(option.Gamma) || option.Gamma < 0);
                    Assert.IsFalse(double.IsNaN(option.Theta) || 0 < option.Theta);
                    Assert.IsFalse(double.IsNaN(option.Vega) || option.Vega < 0);
                    Assert.IsFalse(double.IsNaN(option.Rho));
                }

                OptionChain spyOptionChain = (OptionChain)session.Navigate(optionChain.Search("SPY", true));
                Assert.IsNotNull(spyOptionChain);
                HashSet<StockOption> spyOptions = spyOptionChain.Options;
                Assert.IsNotNull(spyOptions);
                Assert.IsTrue(spyOptions.Count > 0);
                foreach (StockOption option in spyOptions)
                {
                    Assert.IsNotNull(option.Symbol);
                    Assert.IsFalse(double.IsNaN(option.Strike));
                    Assert.IsFalse(option.Expiration < DateTime.Today);
                    Assert.IsFalse(option.Quantity < 0 || 100 < option.Quantity);
                    Assert.IsFalse(double.IsNaN(option.Last));
                    Assert.IsFalse(double.IsNaN(option.Change));
                    Assert.IsFalse(double.IsNaN(option.Bid));
                    Assert.IsFalse(double.IsNaN(option.Ask));
                    Assert.IsFalse(option.Volume < 0);
                    Assert.IsFalse(option.OpenInterest < 0);
                    Assert.IsFalse(option.ImpliedVolatility < 0);
                    Assert.IsFalse(double.IsNaN(option.Delta));
                    Assert.IsFalse(option.Type == OptionType.Call && option.Delta < 0);
                    Assert.IsFalse(option.Type == OptionType.Put && option.Delta > 0);
                    Assert.IsFalse(double.IsNaN(option.Gamma) || option.Gamma < 0);
                    Assert.IsFalse(double.IsNaN(option.Theta) || 0 < option.Theta);
                    Assert.IsFalse(double.IsNaN(option.Vega) || option.Vega < 0);
                    Assert.IsFalse(double.IsNaN(option.Rho));
                }
            }
        }
    }
}
