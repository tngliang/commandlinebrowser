﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;

    public class CookieParser
    {
        public static IEnumerable<Cookie> Parse(Uri uri, string cookieString)
        {
            if (uri == null || string.IsNullOrEmpty(cookieString))
            {
                return new Cookie[] { };
            }

            Stack<Cookie> cookies = new Stack<Cookie>();
            State state = new NewCookieState(uri, cookieString);
            while (state != null)
            {
                state = state.Process(cookies);
            }

            return cookies;
        }

        private static bool ProcessNameValue(Cookie cookie, string name, string value)
        {
            if (!string.IsNullOrEmpty(name))
            {
                name = name.Trim();
            }

            if (string.Compare(name, "Domain", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$Domain", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (!string.IsNullOrEmpty(value) && value.All(c => char.IsLetterOrDigit(c) || c == '.'))
                {
                    cookie.Domain = value;
                    return true;
                }
            }
            else if (string.Compare(name, "Expires", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$Expires", StringComparison.OrdinalIgnoreCase) == 0)
            {
                DateTime expiry;
                if (DateTime.TryParse(value, out expiry))
                {
                    cookie.Expires = expiry;
                    return true;
                }
                else
                {
                    foreach (string part in value.Split(' '))
                    {
                        if (DateTime.TryParse(part, out expiry))
                        {
                            cookie.Expires = expiry;
                            return true;
                        }
                    }
                }
            }
            else if (string.Compare(name, "HttpOnly", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$HttpOnly", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    cookie.HttpOnly = true;
                    return true;
                }
                else
                {
                    bool httpOnly;
                    if (bool.TryParse(value, out httpOnly))
                    {
                        cookie.HttpOnly = httpOnly;
                        return true;
                    }
                }

                return false;
            }
            else if (string.Compare(name, "Path", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$Path", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (value.IndexOfAny(Path.GetInvalidPathChars()) < 0)
                {
                    cookie.Path = value;
                    return true;
                }
            }
            else if (string.Compare(name, "Secure", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$Secure", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    cookie.Secure = true;
                    return true;
                }
                else
                {
                    bool secure;
                    if (bool.TryParse(value, out secure))
                    {
                        cookie.Secure = secure;
                        return true;
                    }
                }
            }
            else if (string.Compare(name, "Version", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(name, "$Version", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (string.IsNullOrEmpty(value))
                {
                    cookie.Version = 0;
                    return true;
                }
                else
                {
                    int cookieVersion;
                    if (int.TryParse(value, out cookieVersion))
                    {
                        cookie.Version = cookieVersion;
                        return true;
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(name))
                {
                    cookie.Name = name;
                    if (!string.IsNullOrEmpty(value))
                    {
                        cookie.Value = value;
                    }
                    return true;
                }
            }

            return false;
        }

        private class State
        {
            public State(string input)
            {
                this.Input = input;
            }

            public State(State lastState, int startIndex)
            {
                this.Last = lastState;
                this.Input = lastState.Input;
                this.Start = this.Index = startIndex;
            }

            public State Last { get; set; }
            public string Input { get; set; }
            public int Start { get; set; }
            public int Index { get; set; }

            public virtual State Process(Stack<Cookie> cookies)
            {
                if (this.Index + 1 >= this.Input.Length)
                {
                    return null;
                }

                return this;
            }
        }

        private class NewCookieState : State
        {
            private Uri uri;
            public NewCookieState(Uri uri, string input)
                : base(input)
            {
                this.uri = uri;
            }

            public NewCookieState(State last, int startIndex)
                : base(last, startIndex)
            {
            }

            public override State Process(Stack<Cookie> cookies)
            {
                if (base.Process(cookies) == null)
                {
                    return null;
                }

                cookies.Push(new Cookie()
                {
                    Path = "/",
                    Expired = false,
                    Expires = DateTime.MaxValue
                });

                return new ProcessNameState(this, this.Index);
            }
        }

        private class ProcessNameState : State
        {
            public ProcessNameState(State last, int startIndex)
                : base(last, startIndex)
            {
            }

            public override State Process(Stack<Cookie> cookies)
            {
                if (base.Process(cookies) == null)
                {
                    ProcessNameValue(cookies.Peek(), this.Input.Substring(this.Last.Start, this.Last.Index - this.Last.Start), string.Empty);
                    return null;
                }

                char c = this.Input[this.Index];
                if ((char.IsWhiteSpace(c) || c == '$') && this.Index == this.Start)
                {
                    ++this.Start;
                    ++this.Index;
                    return Process(cookies);
                }

                if (c == ',')
                {
                    return new NewCookieState(this, this.Index + 1);
                }

                if (c == '=')
                {
                    return new ProcessValueState(this, this.Index + 1);
                }

                if (c != '_' && !char.IsLetterOrDigit(c))
                {
                    if (cookies.Count > 1)
                        cookies.Pop();
                    this.Last.Index = this.Index;
                    return this.Last;
                }

                ++this.Index;
                return this;
            }
        }

        private class ProcessValueState : State
        {
            private bool isInDoubleQuote;

            public ProcessValueState(State last, int startIndex)
                : base(last, startIndex)
            {
            }

            public override State Process(Stack<Cookie> cookies)
            {
                if (base.Process(cookies) == null)
                {
                    ProcessNameValue(cookies.Peek(), this.Input.Substring(this.Last.Start, this.Last.Index - this.Last.Start), this.Input.Substring(this.Start));
                    return null;
                }

                char c = this.Input[this.Index];

                if (c == '\"')
                {
                    this.isInDoubleQuote = !this.isInDoubleQuote;
                    ++this.Index;
                    return this;
                }

                if (isInDoubleQuote)
                {
                    ++this.Index;
                    return this;
                }

                if (c == ',' || c == ';')
                {
                    ProcessNameValue(cookies.Peek(), this.Input.Substring(this.Last.Start, this.Last.Index - this.Last.Start), this.Input.Substring(this.Start, this.Index - this.Start));
                    if (c == ',')
                        return new NewCookieState(this, this.Index + 1);
                    else
                        return new ProcessNameState(this, this.Index + 1);
                }

                ++this.Index;
                return this;
            }
        }
    }
}