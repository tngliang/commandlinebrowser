﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Text;

    public class HttpSession : IDisposable
    {
        private Uri backUri;
        private Dictionary<string, Uri> requestUris;
        private CookieContainer cookies;
        private string cookiePersistenceFile;

        public HttpSession(bool loadCookies = true, string cookiePath = null)
        {
            this.requestUris = new Dictionary<string, Uri>(StringComparer.OrdinalIgnoreCase);
            this.cookies = new CookieContainer();
            if (loadCookies)
            {
                this.cookiePersistenceFile = LoadCookies(cookiePath);
            }
        }

        public void Dispose()
        {
            SaveCookies();
        }

        public void Clear()
        {
            this.cookies = new CookieContainer();
            this.requestUris.Clear();
        }

        public Uri this[string host]
        {
            get
            {
                return this.requestUris[host];
            }
        }

        public Uri Back
        {
            get
            {
                return this.backUri;
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (Uri uri in requestUris.Values)
            {
                foreach (Cookie cookie in cookies.GetCookies(uri))
                {
                    builder.AppendFormat("http://{0}|{1}", uri.Host, cookie.ToString());
                    builder.AppendLine();
                }
            }
            return builder.ToString();
        }

        public virtual void Apply(HttpWebRequest webRequest)
        {
            if (webRequest != null)
            {
                webRequest.CookieContainer = this.cookies;

                //StringBuilder builder = new StringBuilder();
                //foreach (Cookie cookie in cookies.Values)
                //{
                //	if (!string.IsNullOrEmpty(cookie.Name) && cookie.Expires >= DateTime.Now)
                //	{
                //		if (builder.Length > 0)
                //		{
                //			builder.Append("; ");
                //		}
                //		if (!string.IsNullOrEmpty(cookie.Name))
                //		{
                //			builder.AppendFormat("{0}={1}", cookie.Name, cookie.Value);
                //		}
                //		else
                //		{
                //			builder.Append(cookie.Value);
                //		}
                //	}
                //}
                //webRequest.Headers[HttpRequestHeader.Cookie] = builder.ToString();
            }
        }

        public void ReadResponse(HttpWebResponse webResponse)
        {
            //foreach (Cookie cookie in CookieParser.Parse(webResponse.ResponseUri, webResponse.Headers["Set-Cookie"]))
            //{
            //	this.SetCookie(cookie);
            //}

            //this.SetCookie(webResponse.ResponseUri, webResponse.Headers["Set-Cookie"]);
            foreach (Cookie cookie in webResponse.Cookies)
            {
                this.cookies.Add(webResponse.ResponseUri, cookie);
            }
            this.SaveCookies();
            requestUris[webResponse.ResponseUri.Host] = webResponse.ResponseUri;
            this.backUri = webResponse.ResponseUri;
        }

        public void SetCookie(Uri uri, string setCookieString, bool saveCookies)
        {
            string key = uri.Host;
            if (!requestUris.ContainsKey(key))
            {
                requestUris.Add(key, uri);
            }

            string[] parts = setCookieString.Split(';');
            if (parts.Length > 1)
            {
                foreach (Cookie cookie in CookieParser.Parse(uri, setCookieString))
                {
                    if (cookie != null)
                    {
                        this.cookies.Add(uri, cookie);
                    }
                }
            }
            else
            {
                this.cookies.SetCookies(uri, setCookieString);
            }
            
            if (saveCookies)
            {
                this.SaveCookies();
            }
        }

        private string LoadCookies(string cookiePath)
        {
            if (string.IsNullOrEmpty(cookiePath))
            {
                cookiePath = Environment.CurrentDirectory;
            }

            string cookiePersistenceFile = Path.Combine(cookiePath, "Cookies.txt");
            if (File.Exists(cookiePersistenceFile))
            {
                foreach (string line in File.ReadAllLines(cookiePersistenceFile))
                {
                    if (string.IsNullOrEmpty(line) || !line.StartsWith("http"))
                    {
                        continue;
                    }

                    int separatorIndex = line.IndexOf('|');
                    if (separatorIndex >= 0)
                    {
                        this.SetCookie(new Uri(line.Substring(0, separatorIndex)), line.Substring(separatorIndex + 1), false);
                    }
                }
            }

            return cookiePersistenceFile;
        }

        private void SaveCookies()
        {
            if (!string.IsNullOrEmpty(cookiePersistenceFile) && Directory.Exists(Path.GetDirectoryName(cookiePersistenceFile)))
            {
                File.WriteAllText(this.cookiePersistenceFile, this.ToString());
            }
        }
    }
}
