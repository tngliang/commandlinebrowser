﻿namespace System.IO.Compression
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Text;

    // Implements .NET Framework 4.5 System.IO.Compression Library (Readonly) for .NET Framework 4.0.
    public enum CompressionLevel
    {
        Fastest = 1,
        NoCompression = 2,
        Optimal = 0
    }

    public class ZipArchive : IDisposable
    {
        // Fields
        private byte[] archiveComment;
        private BinaryReader archiveReader;
        private Stream archiveStream;
        //private ZipArchiveEntry _archiveStreamOwner;
        private Stream backingStream;
        private long centralDirectoryStart;
        private List<ZipArchiveEntry> entries;
        private ReadOnlyCollection<ZipArchiveEntry> entriesCollection;
        private Dictionary<string, ZipArchiveEntry> entriesDictionary;
        private Encoding _entryNameEncoding;
        private long expectedNumberOfEntries;
        private bool isDisposed;
        private bool leaveOpen;
        private ZipArchiveMode mode;
        private uint numberOfThisDisk;
        private bool readEntries;

        // Methods
        public ZipArchive(Stream stream)
            : this(stream, ZipArchiveMode.Read, false, null)
        {
        }

        public ZipArchive(Stream stream, ZipArchiveMode mode)
            : this(stream, mode, false, null)
        {
        }

        public ZipArchive(Stream stream, ZipArchiveMode mode, bool leaveOpen)
            : this(stream, mode, leaveOpen, null)
        {
        }

        public ZipArchive(Stream stream, ZipArchiveMode mode, bool leaveOpen, Encoding entryNameEncoding)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }
            this.EntryNameEncoding = entryNameEncoding;
            this.Init(stream, mode, leaveOpen);
        }

        //internal void AcquireArchiveStream(ZipArchiveEntry entry);
        private void AddEntry(ZipArchiveEntry entry)
        {
            this.entries.Add(entry);
            string fullName = entry.FullName;
            if (!this.entriesDictionary.ContainsKey(fullName))
            {
                this.entriesDictionary.Add(fullName, entry);
            }
        }

        private void CloseStreams()
        {
            if (!this.leaveOpen)
            {
                this.archiveStream.Close();
                if (this.backingStream != null)
                {
                    this.backingStream.Close();
                }
                if (this.archiveReader != null)
                {
                    this.archiveReader.Close();
                }
            }
            else if (this.backingStream != null)
            {
                this.archiveStream.Close();
            }
        }

        //public ZipArchiveEntry CreateEntry(string entryName);
        //public ZipArchiveEntry CreateEntry(string entryName, CompressionLevel compressionLevel);
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public ReadOnlyCollection<ZipArchiveEntry> Entries
        {
            get
            {
                if (this.mode == ZipArchiveMode.Create)
                {
                    throw new NotSupportedException("EntriesInCreateMode=Cannot access entries in Create mode.");
                }
                this.ThrowIfDisposed();
                this.EnsureCentralDirectoryRead();
                return this.entriesCollection;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !this.isDisposed)
            {
                switch (this.mode)
                {
                    case ZipArchiveMode.Read:
                        break;

                    default:
                        throw new NotImplementedException();
                }
                this.CloseStreams();
                this.isDisposed = true;
            }
        }

        //private ZipArchiveEntry DoCreateEntry(string entryName, CompressionLevel? compressionLevel);
        private void EnsureCentralDirectoryRead()
        {
            if (!this.readEntries)
            {
                this.ReadCentralDirectory();
                this.readEntries = true;
            }
        }
        //public ZipArchiveEntry GetEntry(string entryName);

        private void Init(Stream stream, ZipArchiveMode mode, bool leaveOpen)
        {
            Stream stream2 = null;
            try
            {
                this.backingStream = null;
                switch (mode)
                {
                    case ZipArchiveMode.Read:
                        if (!stream.CanRead)
                        {
                            throw new ArgumentException("Cannot use read mode on a non-readable stream.");
                        }

                        if (!stream.CanSeek)
                        {
                            this.backingStream = stream;
                            stream2 = stream = new MemoryStream();
                            this.backingStream.CopyTo(stream);
                            stream.Seek(0L, SeekOrigin.Begin);
                        }
                        break;

                    case ZipArchiveMode.Create:
                        if (!stream.CanWrite)
                        {
                            throw new ArgumentException("Cannot use create mode on a non-writeable stream.");
                        }
                        break;

                    case ZipArchiveMode.Update:
                        if ((!stream.CanRead || !stream.CanWrite) || !stream.CanSeek)
                        {
                            throw new ArgumentException("Update mode requires a stream with read, write, and seek capabilities.");
                        }
                        break;

                    default:
                        throw new ArgumentOutOfRangeException("mode");
                }

                this.mode = mode;
                this.archiveStream = stream;
                //this._archiveStreamOwner = null;
                if (mode == ZipArchiveMode.Create)
                {
                    this.archiveReader = null;
                }
                else
                {
                    this.archiveReader = new BinaryReader(stream);
                }
                this.entries = new List<ZipArchiveEntry>();
                this.entriesCollection = new ReadOnlyCollection<ZipArchiveEntry>(this.entries);
                this.entriesDictionary = new Dictionary<string, ZipArchiveEntry>();
                this.readEntries = false;
                this.leaveOpen = leaveOpen;
                this.centralDirectoryStart = 0L;
                this.isDisposed = false;
                this.numberOfThisDisk = 0;
                this.archiveComment = null;
                switch (mode)
                {
                    case ZipArchiveMode.Read:
                        this.ReadEndOfCentralDirectory();
                        return;

                    case ZipArchiveMode.Create:
                        this.readEntries = true;
                        return;
                }
                if (this.archiveStream.Length == 0L)
                {
                    this.readEntries = true;
                }
                else
                {
                    this.ReadEndOfCentralDirectory();
                    this.EnsureCentralDirectoryRead();
                    foreach (ZipArchiveEntry entry in this.entries)
                    {
                        entry.ThrowIfNotOpenable(false, true);
                    }
                }
            }
            catch
            {
                if (stream2 != null)
                {
                    stream2.Close();
                }
                throw;
            }
        }

        //internal bool IsStillArchiveStreamOwner(ZipArchiveEntry entry);
        private void ReadCentralDirectory()
        {
            try
            {
                ZipCentralDirectoryFileHeader header;
                this.archiveStream.Seek(this.centralDirectoryStart, SeekOrigin.Begin);
                long num = 0L;
                bool saveExtraFieldsAndComments = false; //this.Mode == ZipArchiveMode.Update;
                while (ZipCentralDirectoryFileHeader.TryReadBlock(this.archiveReader, saveExtraFieldsAndComments, out header))
                {
                    this.AddEntry(new ZipArchiveEntry(this, header));
                    num += 1L;
                }
                if (num != this.expectedNumberOfEntries)
                {
                    throw new InvalidDataException("Number of entries expected in End Of Central Directory does not correspond to number of entries in Central Directory.");
                }
            }
            catch (EndOfStreamException exception)
            {
                throw new InvalidDataException("Central Directory is invalid.", exception);
            }
        }

        private void ReadEndOfCentralDirectory()
        {
            try
            {
                ZipEndOfCentralDirectoryBlock block;
                this.archiveStream.Seek(-18L, SeekOrigin.End);
                if (!ZipHelper.SeekBackwardsToSignature(this.archiveStream, 0x6054b50))
                {
                    throw new InvalidDataException("End of Central Directory record could not be found.");
                }
                long position = this.archiveStream.Position;
                ZipEndOfCentralDirectoryBlock.TryReadBlock(this.archiveReader, out block);
                if (block.NumberOfThisDisk != block.NumberOfTheDiskWithTheStartOfTheCentralDirectory)
                {
                    throw new InvalidDataException("Split or spanned archives are not supported.");
                }
                this.numberOfThisDisk = block.NumberOfThisDisk;
                this.centralDirectoryStart = block.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber;
                if (block.NumberOfEntriesInTheCentralDirectory != block.NumberOfEntriesInTheCentralDirectoryOnThisDisk)
                {
                    throw new InvalidDataException("Split or spanned archives are not supported.");
                }
                this.expectedNumberOfEntries = block.NumberOfEntriesInTheCentralDirectory;
                if (this.mode == ZipArchiveMode.Update)
                {
                    this.archiveComment = block.ArchiveComment;
                }
                if (((block.NumberOfThisDisk == 0xffff) || (block.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber == uint.MaxValue)) || (block.NumberOfEntriesInTheCentralDirectory == 0xffff))
                {
                    this.archiveStream.Seek(position - 0x10L, SeekOrigin.Begin);
                    if (ZipHelper.SeekBackwardsToSignature(this.archiveStream, 0x7064b50))
                    {
                        Zip64EndOfCentralDirectoryLocator locator;
                        Zip64EndOfCentralDirectoryRecord record;
                        Zip64EndOfCentralDirectoryLocator.TryReadBlock(this.archiveReader, out locator);
                        if (locator.OffsetOfZip64EOCD > 0x7fffffffffffffffL)
                        {
                            throw new InvalidDataException("Offset to Zip64 End Of Central Directory record cannot be held in an Int64.");
                        }
                        long offset = (long)locator.OffsetOfZip64EOCD;
                        this.archiveStream.Seek(offset, SeekOrigin.Begin);
                        if (!Zip64EndOfCentralDirectoryRecord.TryReadBlock(this.archiveReader, out record))
                        {
                            throw new InvalidDataException("Zip 64 End of Central Directory Record not where indicated.");
                        }
                        this.numberOfThisDisk = record.NumberOfThisDisk;
                        if (record.NumberOfEntriesTotal > 0x7fffffffffffffffL)
                        {
                            throw new InvalidDataException("Number of Entries cannot be held in an Int64.");
                        }
                        if (record.OffsetOfCentralDirectory > 0x7fffffffffffffffL)
                        {
                            throw new InvalidDataException("Offset to Central Directory cannot be held in an Int64.");
                        }
                        if (record.NumberOfEntriesTotal != record.NumberOfEntriesOnThisDisk)
                        {
                            throw new InvalidDataException("Split or spanned archives are not supported.");
                        }
                        this.expectedNumberOfEntries = (long)record.NumberOfEntriesTotal;
                        this.centralDirectoryStart = (long)record.OffsetOfCentralDirectory;
                    }
                }
                if (this.centralDirectoryStart > this.archiveStream.Length)
                {
                    throw new InvalidDataException("Offset to Central Directory cannot be held in an Int64.");
                }
            }
            catch (EndOfStreamException exception)
            {
                throw new InvalidDataException("Central Directory corrupt.", exception);
            }
            catch (IOException exception2)
            {
                throw new InvalidDataException("Central Directory corrupt.", exception2);
            }
        }
    
        //internal void ReleaseArchiveStream(ZipArchiveEntry entry);
        //internal void RemoveEntry(ZipArchiveEntry entry);
        internal void ThrowIfDisposed()
        {
            if (this.isDisposed)
            {
                throw new ObjectDisposedException(base.GetType().Name);
            }
        }

        //private void WriteArchiveEpilogue(long startOfCentralDirectory, long sizeOfCentralDirectory);
        //private void WriteFile();

        // Properties
        internal BinaryReader ArchiveReader
        {
            get
            {
                return this.archiveReader;
            }
        }
 
        internal Stream ArchiveStream
        {
            get
            {
                return this.archiveStream;
            }
        }

        internal Encoding EntryNameEncoding
        {
            get
            {
                return this._entryNameEncoding;
            }
            private set
            {
                if ((value != null) && ((value.Equals(Encoding.BigEndianUnicode) || value.Equals(Encoding.Unicode)) || (value.Equals(Encoding.UTF32) || value.Equals(Encoding.UTF7))))
                {
                    throw new ArgumentException("EntryNameEncodingNotSupported=The specified entry name encoding is not supported.", "entryNameEncoding");
                }
                this._entryNameEncoding = value;
            }
        }

        public ZipArchiveMode Mode
        {
            get
            {
                return this.mode;
            }
        }

        internal uint NumberOfThisDisk
        {
            get
            {
                return this.numberOfThisDisk;
            }
        }
    }

    public class ZipArchiveEntry
    {
        // Fields
        private ZipArchive archive;
        private List<ZipGenericExtraField> cdUnknownExtraFields;
        //private byte[] compressedBytes;
        private long compressedSize;
        private CompressionLevel? compressionLevel;
        private uint crc32;
        //private bool currentlyOpenForWrite;
        private readonly int diskNumberStart;
        private bool everOpenedForWrite;
        private byte[] fileComment;
        private BitFlagValues generalPurposeBitFlag;
        private DateTimeOffset lastModified;
        //private List<ZipGenericExtraField> lhUnknownExtraFields;
        private long offsetOfLocalHeader;
        private readonly bool originallyInArchive;
        //private Stream outstandingWriteStream;
        private CompressionMethodValues storedCompressionMethod;
        private string storedEntryName;
        private byte[] storedEntryNameBytes;
        private long? storedOffsetOfCompressedData;
        private MemoryStream storedUncompressedData;
        private long uncompressedSize;
        private ZipVersionNeededValues versionToExtract;
        private const ushort DefaultVersionToExtract = 10;

        // Methods
        internal ZipArchiveEntry(ZipArchive archive, ZipCentralDirectoryFileHeader cd)
        {
            this.archive = archive;
            this.originallyInArchive = true;
            this.diskNumberStart = cd.DiskNumberStart;
            this.versionToExtract = (ZipVersionNeededValues)cd.VersionNeededToExtract;
            this.generalPurposeBitFlag = (BitFlagValues)cd.GeneralPurposeBitFlag;
            this.CompressionMethod = (CompressionMethodValues)cd.CompressionMethod;
            this.lastModified = new DateTimeOffset(ZipHelper.DosTimeToDateTime(cd.LastModified));
            this.compressedSize = cd.CompressedSize;
            this.uncompressedSize = cd.UncompressedSize;
            this.offsetOfLocalHeader = cd.RelativeOffsetOfLocalHeader;
            this.storedOffsetOfCompressedData = null;
            this.crc32 = cd.Crc32;
            //this._compressedBytes = null;
            this.storedUncompressedData = null;
            //this._currentlyOpenForWrite = false;
            this.everOpenedForWrite = false;
            //this._outstandingWriteStream = null;
            this.FullName = this.DecodeEntryName(cd.Filename);
            //this._lhUnknownExtraFields = null;
            this.cdUnknownExtraFields = cd.ExtraFields;
            this.fileComment = cd.FileComment;
            this.compressionLevel = null;
        }

        internal ZipArchiveEntry(ZipArchive archive, string entryName)
        {
            this.archive = archive;
            this.originallyInArchive = false;
            this.diskNumberStart = 0;
            this.versionToExtract = ZipVersionNeededValues.Default;
            this.generalPurposeBitFlag = 0;
            this.CompressionMethod = CompressionMethodValues.Deflate;
            this.lastModified = DateTimeOffset.Now;
            this.compressedSize = 0L;
            this.uncompressedSize = 0L;
            this.offsetOfLocalHeader = 0L;
            this.storedOffsetOfCompressedData = null;
            this.crc32 = 0;
            //this._compressedBytes = null;
            this.storedUncompressedData = null;
            //this._currentlyOpenForWrite = false;
            this.everOpenedForWrite = false;
            //this._outstandingWriteStream = null;
            this.FullName = entryName;
            this.cdUnknownExtraFields = null;
            //this._lhUnknownExtraFields = null;
            this.fileComment = null;
            this.compressionLevel = null;
            if (this.storedEntryNameBytes.Length > 0xffff)
            {
                throw new ArgumentException("Entry names cannot require more than 2^16 bits.");
            }
            if (this.archive.Mode == ZipArchiveMode.Create)
            {
                throw new NotImplementedException();
            }
        }

        internal ZipArchiveEntry(ZipArchive archive, ZipCentralDirectoryFileHeader cd, CompressionLevel compressionLevel)
            : this(archive, cd)
        {
            this.compressionLevel = new CompressionLevel?(compressionLevel);
        }

        internal ZipArchiveEntry(ZipArchive archive, string entryName, CompressionLevel compressionLevel)
            : this(archive, entryName)
        {
            this.compressionLevel = new CompressionLevel?(compressionLevel);
        }

        public Stream Open()
        {
            this.ThrowIfInvalidArchive();
            switch (this.archive.Mode)
            {
                case ZipArchiveMode.Read:
                    return this.OpenInReadMode(true);

                case ZipArchiveMode.Create:
                    throw new NotImplementedException();
            }
            throw new NotImplementedException();
        }

        public string FullName
        {
            get
            {
                return this.storedEntryName;
            }
            private set
            {
                bool flag;
                if (value == null)
                {
                    throw new ArgumentNullException("FullName");
                }
                this.storedEntryNameBytes = this.EncodeEntryName(value, out flag);
                this.storedEntryName = value;
                if (flag)
                {
                    this.generalPurposeBitFlag = (BitFlagValues)((ushort)(this.generalPurposeBitFlag | BitFlagValues.UnicodeFileName));
                }
                else
                {
                    this.generalPurposeBitFlag = (BitFlagValues)((ushort)(((int)this.generalPurposeBitFlag) & 0xf7ff));
                }
                if (ZipHelper.EndsWithDirChar(value))
                {
                    this.VersionToExtractAtLeast(ZipVersionNeededValues.ExplicitDirectory);
                }
            }
        }

        public DateTimeOffset LastWriteTime
        {
            get
            {
                return this.lastModified;
            }
            set
            {
                this.ThrowIfInvalidArchive();
                if (this.archive.Mode == ZipArchiveMode.Read)
                {
                    throw new NotSupportedException("Cannot modify read-only archive.");
                }
                if ((this.archive.Mode == ZipArchiveMode.Create) && this.everOpenedForWrite)
                {
                    throw new IOException("Cannot modify entry in Create mode after entry has been opened for writing.");
                }
                if ((value.DateTime.Year < 0x7bc) || (value.DateTime.Year > 0x83b))
                {
                    throw new ArgumentOutOfRangeException("value", "The DateTimeOffset specified cannot be converted into a Zip file timestamp.");
                }
                this.lastModified = value;
            }
        }

        public long Length
        {
            get
            {
                if (this.everOpenedForWrite)
                {
                    throw new InvalidOperationException("Length properties are unavailable once an entry has been opened for writing.");
                }
                return this.uncompressedSize;
            }
        }

        public string Name
        {
            get
            {
                return Path.GetFileName(this.FullName);
            }
        }

        //public ZipArchive Archive { get; }
        //public long CompressedLength {  get; }
        //public void Delete();

        //private void CloseStreams();
        
        private string DecodeEntryName(byte[] entryNameBytes)
        {
            // This item is obfuscated and can not be translated.
            Encoding encoding;
            if (((ushort)(this.generalPurposeBitFlag & BitFlagValues.UnicodeFileName)) == 0)
            {
                Encoding expressionStack_32_0;
                if (this.archive != null)
                {
                    Encoding expressionStack_25_0;
                    Encoding entryNameEncoding = this.archive.EntryNameEncoding;
                    if (entryNameEncoding != null)
                    {
                        encoding = Encoding.Default;
                        return encoding.GetString(entryNameBytes);
                    }
                    else
                    {
                        expressionStack_25_0 = entryNameEncoding;
                    }
                    expressionStack_25_0 = Encoding.Default;
                    expressionStack_32_0 = Encoding.Default;
                }
                else
                {
                    expressionStack_32_0 = Encoding.Default;
                }
                encoding = expressionStack_32_0;
            }
            else
            {
                encoding = Encoding.UTF8;
            }
            return encoding.GetString(entryNameBytes);
        }

        private byte[] EncodeEntryName(string entryName, out bool isUTF8)
        {
            Encoding entryNameEncoding;
            if ((this.archive != null) && (this.archive.EntryNameEncoding != null))
            {
                entryNameEncoding = this.archive.EntryNameEncoding;
            }
            else
            {
                entryNameEncoding = ZipHelper.RequiresUnicode(entryName) ? Encoding.UTF8 : Encoding.Default;
            }
            isUTF8 = (entryNameEncoding is UTF8Encoding) && entryNameEncoding.Equals(Encoding.UTF8);
            return entryNameEncoding.GetBytes(entryName);
        }

        //private CheckSumAndSizeWriteStream GetDataCompressor(Stream backingStream, bool leaveBackingStreamOpen, EventHandler onClose)

        private Stream GetDataDecompressor(Stream compressedStreamToRead)
        {
            CompressionMethodValues compressionMethod = this.CompressionMethod;
            if ((compressionMethod != CompressionMethodValues.Stored) && (compressionMethod == CompressionMethodValues.Deflate))
            {
                return new DeflateStream(compressedStreamToRead, CompressionMode.Decompress);
            }
            return compressedStreamToRead;
        }

        private bool IsOpenable(bool needToUncompress, bool needToLoadIntoMemory, out string message)
        {
            message = null;
            if (this.originallyInArchive)
            {
                if ((needToUncompress && (this.CompressionMethod != CompressionMethodValues.Stored)) && (this.CompressionMethod != CompressionMethodValues.Deflate))
                {
                    message = "The archive entry was compressed using an unsupported compression method.";
                    return false;
                }
                if (this.diskNumberStart != this.archive.NumberOfThisDisk)
                {
                    message = "Split or spanned archives are not supported.";
                    return false;
                }
                if (this.offsetOfLocalHeader > this.archive.ArchiveStream.Length)
                {
                    message = "A local file header is corrupt.";
                    return false;
                }
                this.archive.ArchiveStream.Seek(this.offsetOfLocalHeader, SeekOrigin.Begin);
                if (!ZipLocalFileHeader.TrySkipBlock(this.archive.ArchiveReader))
                {
                    message = "A local file header is corrupt.";
                    return false;
                }
                if ((this.OffsetOfCompressedData + this.compressedSize) > this.archive.ArchiveStream.Length)
                {
                    message = "A local file header is corrupt.";
                    return false;
                }
                if (needToLoadIntoMemory && (this.compressedSize > 0x7fffffffL))
                {
                    message = "Entries larger than 4GB are not supported in Update mode.";
                    return false;
                }
            }
            return true;
        }

        //internal bool LoadLocalHeaderExtraFieldAndCompressedBytesIfNeeded();

        private Stream OpenInReadMode(bool checkOpenable)
        {
            if (checkOpenable)
            {
                this.ThrowIfNotOpenable(true, false);
            }
            Stream compressedStreamToRead = new SubReadStream(this.archive.ArchiveStream, this.OffsetOfCompressedData, this.compressedSize);
            return this.GetDataDecompressor(compressedStreamToRead);
        }

        //private Stream OpenInUpdateMode();
        //private Stream OpenInWriteMode();
        //private bool SizesTooLarge();
        private void ThrowIfInvalidArchive()
        {
            if (this.archive == null)
            {
                throw new InvalidOperationException("Cannot modify deleted entry.");
            }
            this.archive.ThrowIfDisposed();
        }

        internal void ThrowIfNotOpenable(bool needToUncompress, bool needToLoadIntoMemory)
        {
            string str;
            if (!this.IsOpenable(needToUncompress, needToLoadIntoMemory, out str))
            {
                throw new InvalidDataException(str);
            }
        }

        //public override string ToString();
        //private void UnloadStreams();
        private void VersionToExtractAtLeast(ZipVersionNeededValues value)
        {
            if (this.versionToExtract < value)
            {
                this.versionToExtract = value;
            }
        }

        //internal void WriteAndFinishLocalEntry();
        //internal void WriteCentralDirectoryFileHeader();
        //private void WriteCrcAndSizesInLocalHeader(bool zip64HeaderUsed);
        //private void WriteDataDescriptor();
        //private bool WriteLocalFileHeader(bool isEmptyFile);
        //private void WriteLocalFileHeaderAndDataIfNeeded();

        private CompressionMethodValues CompressionMethod
        {
            get
            {
                return this.storedCompressionMethod;
            }
            set
            {
                if (value == CompressionMethodValues.Deflate)
                {
                    this.VersionToExtractAtLeast(ZipVersionNeededValues.ExplicitDirectory);
                }
                this.storedCompressionMethod = value;
            }
        }

        //internal bool EverOpenedForWrite { get; }

        private long OffsetOfCompressedData
        {
            get
            {
                if (!this.storedOffsetOfCompressedData.HasValue)
                {
                    this.archive.ArchiveStream.Seek(this.offsetOfLocalHeader, SeekOrigin.Begin);
                    if (!ZipLocalFileHeader.TrySkipBlock(this.archive.ArchiveReader))
                    {
                        throw new InvalidDataException("A local file header is corrupt.");
                    }
                    this.storedOffsetOfCompressedData = new long?(this.archive.ArchiveStream.Position);
                }
                return this.storedOffsetOfCompressedData.Value;
            }
        }

        private MemoryStream UncompressedData
        {
            get
            {
                if (this.storedUncompressedData == null)
                {
                    this.storedUncompressedData = new MemoryStream((int)this.uncompressedSize);
                    if (this.originallyInArchive)
                    {
                        Stream stream = this.OpenInReadMode(false);
                        try
                        {
                            stream.CopyTo(this.storedUncompressedData);
                        }
                        catch (InvalidDataException)
                        {
                            this.storedUncompressedData.Dispose();
                            this.storedUncompressedData = null;
                            //this._currentlyOpenForWrite = false;
                            this.everOpenedForWrite = false;
                            throw;
                        }
                        finally
                        {
                            if (stream != null)
                            {
                                stream.Dispose();
                            }
                        }
                    }
                    this.CompressionMethod = CompressionMethodValues.Deflate;
                }
                return this.storedUncompressedData;
            }
        }

        // Nested Types
        [Flags]
        private enum BitFlagValues : ushort
        {
            DataDescriptor = 8,
            UnicodeFileName = 0x800
        }

        private enum CompressionMethodValues : ushort
        {
            Deflate = 8,
            Stored = 0
        }

        private enum OpenableValues
        {
            Openable,
            FileNonExistent,
            FileTooLarge
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ZipCentralDirectoryFileHeader
    {
        public const uint SignatureConstant = 0x2014b50;
        public ushort VersionMadeBy;
        public ushort VersionNeededToExtract;
        public ushort GeneralPurposeBitFlag;
        public ushort CompressionMethod;
        public uint LastModified;
        public uint Crc32;
        public long CompressedSize;
        public long UncompressedSize;
        public ushort FilenameLength;
        public ushort ExtraFieldLength;
        public ushort FileCommentLength;
        public int DiskNumberStart;
        public ushort InternalFileAttributes;
        public uint ExternalFileAttributes;
        public long RelativeOffsetOfLocalHeader;
        public byte[] Filename;
        public byte[] FileComment;
        public List<ZipGenericExtraField> ExtraFields;
        public static bool TryReadBlock(BinaryReader reader, bool saveExtraFieldsAndComments, out ZipCentralDirectoryFileHeader header)
        {
            Zip64ExtraField field;
            header = new ZipCentralDirectoryFileHeader();
            if (reader.ReadUInt32() != 0x2014b50)
            {
                return false;
            }
            header.VersionMadeBy = reader.ReadUInt16();
            header.VersionNeededToExtract = reader.ReadUInt16();
            header.GeneralPurposeBitFlag = reader.ReadUInt16();
            header.CompressionMethod = reader.ReadUInt16();
            header.LastModified = reader.ReadUInt32();
            header.Crc32 = reader.ReadUInt32();
            uint num = reader.ReadUInt32();
            uint num2 = reader.ReadUInt32();
            header.FilenameLength = reader.ReadUInt16();
            header.ExtraFieldLength = reader.ReadUInt16();
            header.FileCommentLength = reader.ReadUInt16();
            ushort num3 = reader.ReadUInt16();
            header.InternalFileAttributes = reader.ReadUInt16();
            header.ExternalFileAttributes = reader.ReadUInt32();
            uint num4 = reader.ReadUInt32();
            header.Filename = reader.ReadBytes(header.FilenameLength);
            bool readUncompressedSize = num2 == uint.MaxValue;
            bool readCompressedSize = num == uint.MaxValue;
            bool readLocalHeaderOffset = num4 == uint.MaxValue;
            bool readStartDiskNumber = num3 == 0xffff;
            using (Stream stream = new SubReadStream(reader.BaseStream, reader.BaseStream.Position, (long)header.ExtraFieldLength))
            {
                if (saveExtraFieldsAndComments)
                {
                    header.ExtraFields = ZipGenericExtraField.ParseExtraField(stream);
                    field = Zip64ExtraField.GetAndRemoveZip64Block(header.ExtraFields, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber);
                }
                else
                {
                    header.ExtraFields = null;
                    field = Zip64ExtraField.GetJustZip64Block(stream, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber);
                }
            }
            if (saveExtraFieldsAndComments)
            {
                header.FileComment = reader.ReadBytes(header.FileCommentLength);
            }
            else
            {
                Stream baseStream = reader.BaseStream;
                baseStream.Position += header.FileCommentLength;
                header.FileComment = null;
            }
            header.UncompressedSize = !field.UncompressedSize.HasValue ? ((long)num2) : field.UncompressedSize.Value;
            header.CompressedSize = !field.CompressedSize.HasValue ? ((long)num) : field.CompressedSize.Value;
            header.RelativeOffsetOfLocalHeader = !field.LocalHeaderOffset.HasValue ? ((long)num4) : field.LocalHeaderOffset.Value;
            header.DiskNumberStart = !field.StartDiskNumber.HasValue ? num3 : field.StartDiskNumber.Value;
            return true;
        }
    }

    [StructLayout(LayoutKind.Sequential, Size = 1)]
    internal struct ZipLocalFileHeader
    {
        public const uint DataDescriptorSignature = 0x8074b50;
        public const uint SignatureConstant = 0x4034b50;
        public const int OffsetToCrcFromHeaderStart = 14;
        public const int OffsetToBitFlagFromHeaderStart = 6;
        public const int SizeOfLocalHeader = 30;
        
        public static List<ZipGenericExtraField> GetExtraFields(BinaryReader reader)
        {
            List<ZipGenericExtraField> list;
            reader.BaseStream.Seek(0x1aL, SeekOrigin.Current);
            ushort num = reader.ReadUInt16();
            ushort num2 = reader.ReadUInt16();
            reader.BaseStream.Seek((long)num, SeekOrigin.Current);
            using (Stream stream = new SubReadStream(reader.BaseStream, reader.BaseStream.Position, (long)num2))
            {
                list = ZipGenericExtraField.ParseExtraField(stream);
            }
            Zip64ExtraField.RemoveZip64Blocks(list);
            return list;
        }

        public static bool TrySkipBlock(BinaryReader reader)
        {
            if (reader.ReadUInt32() != 0x4034b50)
            {
                return false;
            }
            if (reader.BaseStream.Length < (reader.BaseStream.Position + 0x16L))
            {
                return false;
            }
            reader.BaseStream.Seek(0x16L, SeekOrigin.Current);
            ushort num = reader.ReadUInt16();
            ushort num2 = reader.ReadUInt16();
            if (reader.BaseStream.Length < ((reader.BaseStream.Position + num) + num2))
            {
                return false;
            }
            reader.BaseStream.Seek((long)(num + num2), SeekOrigin.Current);
            return true;
        }
    }

    internal class SubReadStream : Stream
    {
        // Fields
        private bool _canRead;
        private readonly long _endInSuperStream;
        private bool _isDisposed;
        private long _positionInSuperStream;
        private readonly long _startInSuperStream;
        private readonly Stream _superStream;

        // Methods
        public SubReadStream(Stream superStream, long startPosition, long maxLength)
        {
            this._startInSuperStream = startPosition;
            this._positionInSuperStream = startPosition;
            this._endInSuperStream = startPosition + maxLength;
            this._superStream = superStream;
            this._canRead = true;
            this._isDisposed = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !this._isDisposed)
            {
                this._canRead = false;
                this._isDisposed = true;
            }
            base.Dispose(disposing);
        }

        public override void Flush()
        {
            this.ThrowIfDisposed();
            throw new NotSupportedException("This stream from ZipArchiveEntry does not support writing.");
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            this.ThrowIfDisposed();
            this.ThrowIfCantRead();
            if (this._superStream.Position != this._positionInSuperStream)
            {
                this._superStream.Seek(this._positionInSuperStream, SeekOrigin.Begin);
            }
            if ((this._positionInSuperStream + count) > this._endInSuperStream)
            {
                count = (int) (this._endInSuperStream - this._positionInSuperStream);
            }
            int num = this._superStream.Read(buffer, offset, count);
            this._positionInSuperStream += num;
            return num;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            this.ThrowIfDisposed();
            throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
        }

        public override void SetLength(long value)
        {
            this.ThrowIfDisposed();
            throw new NotSupportedException("SetLength requires a stream that supports seeking and writing.");
        }

        private void ThrowIfCantRead()
        {
            if (!this.CanRead)
            {
                throw new NotSupportedException("This stream from ZipArchiveEntry does not support reading.");
            }
        }

        private void ThrowIfDisposed()
        {
            if (this._isDisposed)
            {
                throw new ObjectDisposedException(base.GetType().Name, "A stream from ZipArchiveEntry has been disposed.");
            }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            this.ThrowIfDisposed();
            throw new NotSupportedException("This stream from ZipArchiveEntry does not support writing.");
        }

        // Properties
        public override bool CanRead
        {
            get
            {
                return (this._superStream.CanRead && this._canRead);
            }
        }

        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        public override long Length
        {
            get
            {
                this.ThrowIfDisposed();
                return (this._endInSuperStream - this._startInSuperStream);
            }
        }

        public override long Position
        {
            get
            {
                this.ThrowIfDisposed();
                return (this._positionInSuperStream - this._startInSuperStream);
            }
            set
            {
                this.ThrowIfDisposed();
                throw new NotSupportedException("This stream from ZipArchiveEntry does not support seeking.");
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ZipGenericExtraField
    {
        private ushort _tag;
        private ushort _size;
        private byte[] _data;
        private const int SizeOfHeader = 4;
        public ushort Tag
        {
            get
            {
                return this._tag;
            }
        }

        public ushort Size
        {
            get
            {
                return this._size;
            }
        } 

        public byte[] Data
        {
            get
            {
                return this._data;
            }
        }

        //public void WriteBlock(Stream stream);
        public static bool TryReadBlock(BinaryReader reader, long endExtraField, out ZipGenericExtraField field)
        {
            field = new ZipGenericExtraField();
            if ((endExtraField - reader.BaseStream.Position) < 4L)
            {
                return false;
            }
            field._tag = reader.ReadUInt16();
            field._size = reader.ReadUInt16();
            if ((endExtraField - reader.BaseStream.Position) < field._size)
            {
                return false;
            }
            field._data = reader.ReadBytes(field._size);
            return true;
        }

        public static List<ZipGenericExtraField> ParseExtraField(Stream extraFieldData)
        {
            List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
            using (BinaryReader reader = new BinaryReader(extraFieldData))
            {
                ZipGenericExtraField field;
                while (TryReadBlock(reader, extraFieldData.Length, out field))
                {
                    list.Add(field);
                }
            }
            return list;
        }

        public static int TotalSize(List<ZipGenericExtraField> fields)
        {
            int num = 0;
            foreach (ZipGenericExtraField field in fields)
            {
                num += field.Size + 4;
            }
            return num;
        }

        //public static void WriteAllBlocks(List<ZipGenericExtraField> fields, Stream stream);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ZipEndOfCentralDirectoryBlock
    {
        public const uint SignatureConstant = 0x6054b50;
        public const int SizeOfBlockWithoutSignature = 0x12;
        public uint Signature;
        public ushort NumberOfThisDisk;
        public ushort NumberOfTheDiskWithTheStartOfTheCentralDirectory;
        public ushort NumberOfEntriesInTheCentralDirectoryOnThisDisk;
        public ushort NumberOfEntriesInTheCentralDirectory;
        public uint SizeOfCentralDirectory;
        public uint OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber;
        public byte[] ArchiveComment;
        //public static void WriteBlock(Stream stream, long numberOfEntries, long startOfCentralDirectory, long sizeOfCentralDirectory, byte[] archiveComment);
        public static bool TryReadBlock(BinaryReader reader, out ZipEndOfCentralDirectoryBlock eocdBlock)
        {
            eocdBlock = new ZipEndOfCentralDirectoryBlock();
            if (reader.ReadUInt32() != 0x6054b50)
            {
                return false;
            }
            eocdBlock.Signature = 0x6054b50;
            eocdBlock.NumberOfThisDisk = reader.ReadUInt16();
            eocdBlock.NumberOfTheDiskWithTheStartOfTheCentralDirectory = reader.ReadUInt16();
            eocdBlock.NumberOfEntriesInTheCentralDirectoryOnThisDisk = reader.ReadUInt16();
            eocdBlock.NumberOfEntriesInTheCentralDirectory = reader.ReadUInt16();
            eocdBlock.SizeOfCentralDirectory = reader.ReadUInt32();
            eocdBlock.OffsetOfStartOfCentralDirectoryWithRespectToTheStartingDiskNumber = reader.ReadUInt32();
            ushort count = reader.ReadUInt16();
            eocdBlock.ArchiveComment = reader.ReadBytes(count);
            return true;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Zip64EndOfCentralDirectoryLocator
    {
        public const uint SignatureConstant = 0x7064b50;
        public const int SizeOfBlockWithoutSignature = 0x10;
        public uint NumberOfDiskWithZip64EOCD;
        public ulong OffsetOfZip64EOCD;
        public uint TotalNumberOfDisks;
        public static bool TryReadBlock(BinaryReader reader, out Zip64EndOfCentralDirectoryLocator zip64EOCDLocator)
        {
            zip64EOCDLocator = new Zip64EndOfCentralDirectoryLocator();
            if (reader.ReadUInt32() != 0x7064b50)
            {
                return false;
            }
            zip64EOCDLocator.NumberOfDiskWithZip64EOCD = reader.ReadUInt32();
            zip64EOCDLocator.OffsetOfZip64EOCD = reader.ReadUInt64();
            zip64EOCDLocator.TotalNumberOfDisks = reader.ReadUInt32();
            return true;
        }

        //public static void WriteBlock(Stream stream, long zip64EOCDRecordStart);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Zip64EndOfCentralDirectoryRecord
    {
        private const uint SignatureConstant = 0x6064b50;
        private const ulong NormalSize = 0x2cL;
        public ulong SizeOfThisRecord;
        public ushort VersionMadeBy;
        public ushort VersionNeededToExtract;
        public uint NumberOfThisDisk;
        public uint NumberOfDiskWithStartOfCD;
        public ulong NumberOfEntriesOnThisDisk;
        public ulong NumberOfEntriesTotal;
        public ulong SizeOfCentralDirectory;
        public ulong OffsetOfCentralDirectory;

        public static bool TryReadBlock(BinaryReader reader, out Zip64EndOfCentralDirectoryRecord zip64EOCDRecord)
        {
            zip64EOCDRecord = new Zip64EndOfCentralDirectoryRecord();
            if (reader.ReadUInt32() != 0x6064b50)
            {
                return false;
            }
            zip64EOCDRecord.SizeOfThisRecord = reader.ReadUInt64();
            zip64EOCDRecord.VersionMadeBy = reader.ReadUInt16();
            zip64EOCDRecord.VersionNeededToExtract = reader.ReadUInt16();
            zip64EOCDRecord.NumberOfThisDisk = reader.ReadUInt32();
            zip64EOCDRecord.NumberOfDiskWithStartOfCD = reader.ReadUInt32();
            zip64EOCDRecord.NumberOfEntriesOnThisDisk = reader.ReadUInt64();
            zip64EOCDRecord.NumberOfEntriesTotal = reader.ReadUInt64();
            zip64EOCDRecord.SizeOfCentralDirectory = reader.ReadUInt64();
            zip64EOCDRecord.OffsetOfCentralDirectory = reader.ReadUInt64();
            return true;
        }

        //public static void WriteBlock(Stream stream, long numberOfEntries, long startOfCentralDirectory, long sizeOfCentralDirectory);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct Zip64ExtraField
    {
        public const int OffsetToFirstField = 4;
        private const ushort TagConstant = 1;
        private ushort _size;
        private long? _uncompressedSize;
        private long? _compressedSize;
        private long? _localHeaderOffset;
        private int? _startDiskNumber;
        public ushort TotalSize
        {
            get
            {
                return (ushort)(this._size + 4);
            }
        }

        public long? UncompressedSize
        {
            get
            {
                return this._uncompressedSize;
            }
            set
            {
                this._uncompressedSize = value;
                this.UpdateSize();
            }
        }

        public long? CompressedSize
        {
            get
            {
                return this._compressedSize;
            }
            set
            {
                this._compressedSize = value;
                this.UpdateSize();
            }
        }

        public long? LocalHeaderOffset
        {
            get
            {
                return this._localHeaderOffset;
            }
            set
            {
                this._localHeaderOffset = value;
                this.UpdateSize();
            }
        }

        public int? StartDiskNumber
        {
            get
            {
                return this._startDiskNumber;
            }
        }

        private void UpdateSize()
        {
            this._size = 0;
            if (this._uncompressedSize.HasValue)
            {
                this._size = (ushort)(this._size + 8);
            }
            if (this._compressedSize.HasValue)
            {
                this._size = (ushort)(this._size + 8);
            }
            if (this._localHeaderOffset.HasValue)
            {
                this._size = (ushort)(this._size + 8);
            }
            if (this._startDiskNumber.HasValue)
            {
                this._size = (ushort)(this._size + 4);
            }
        }

        public static Zip64ExtraField GetJustZip64Block(Stream extraFieldStream, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber)
        {
            using (BinaryReader reader = new BinaryReader(extraFieldStream))
            {
                ZipGenericExtraField field2;
                while (ZipGenericExtraField.TryReadBlock(reader, extraFieldStream.Length, out field2))
                {
                    Zip64ExtraField field;
                    if (TryGetZip64BlockFromGenericExtraField(field2, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber, out field))
                    {
                        return field;
                    }
                }
            }
            return new Zip64ExtraField { _compressedSize = null, _uncompressedSize = null, _localHeaderOffset = null, _startDiskNumber = null };
        }

        private static bool TryGetZip64BlockFromGenericExtraField(ZipGenericExtraField extraField, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber, out Zip64ExtraField zip64Block)
        {
            bool flag;
            zip64Block = new Zip64ExtraField();
            zip64Block._compressedSize = null;
            zip64Block._uncompressedSize = null;
            zip64Block._localHeaderOffset = null;
            zip64Block._startDiskNumber = null;
            if (extraField.Tag != 1)
            {
                return false;
            }
            MemoryStream input = null;
            try
            {
                input = new MemoryStream(extraField.Data);
                using (BinaryReader reader = new BinaryReader(input))
                {
                    input = null;
                    zip64Block._size = extraField.Size;
                    ushort num = 0;
                    if (readUncompressedSize)
                    {
                        num = (ushort)(num + 8);
                    }
                    if (readCompressedSize)
                    {
                        num = (ushort)(num + 8);
                    }
                    if (readLocalHeaderOffset)
                    {
                        num = (ushort)(num + 8);
                    }
                    if (readStartDiskNumber)
                    {
                        num = (ushort)(num + 4);
                    }
                    if (num != zip64Block._size)
                    {
                        return false;
                    }
                    if (readUncompressedSize)
                    {
                        zip64Block._uncompressedSize = new long?(reader.ReadInt64());
                    }
                    if (readCompressedSize)
                    {
                        zip64Block._compressedSize = new long?(reader.ReadInt64());
                    }
                    if (readLocalHeaderOffset)
                    {
                        zip64Block._localHeaderOffset = new long?(reader.ReadInt64());
                    }
                    if (readStartDiskNumber)
                    {
                        zip64Block._startDiskNumber = new int?(reader.ReadInt32());
                    }
                    if (zip64Block._uncompressedSize < 0L)
                    {
                        throw new InvalidDataException("Uncompressed Size cannot be held in an Int64.");
                    }
                    if (zip64Block._compressedSize < 0L)
                    {
                        throw new InvalidDataException("Compressed Size cannot be held in an Int64.");
                    }
                    if (zip64Block._localHeaderOffset < 0L)
                    {
                        throw new InvalidDataException("Local Header Offset cannot be held in an Int64.");
                    }
                    if (zip64Block._startDiskNumber < 0)
                    {
                        throw new InvalidDataException("Start Disk Number cannot be held in an Int64.");
                    }
                    flag = true;
                }
            }
            finally
            {
                if (input != null)
                {
                    input.Close();
                }
            }
            return flag;
        }

        public static Zip64ExtraField GetAndRemoveZip64Block(List<ZipGenericExtraField> extraFields, bool readUncompressedSize, bool readCompressedSize, bool readLocalHeaderOffset, bool readStartDiskNumber)
        {
            Zip64ExtraField field = new Zip64ExtraField
            {
                _compressedSize = null,
                _uncompressedSize = null,
                _localHeaderOffset = null,
                _startDiskNumber = null
            };
            List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
            bool flag = false;
            foreach (ZipGenericExtraField field2 in extraFields)
            {
                if (field2.Tag == 1)
                {
                    list.Add(field2);
                    if (!flag && TryGetZip64BlockFromGenericExtraField(field2, readUncompressedSize, readCompressedSize, readLocalHeaderOffset, readStartDiskNumber, out field))
                    {
                        flag = true;
                    }
                }
            }
            foreach (ZipGenericExtraField field3 in list)
            {
                extraFields.Remove(field3);
            }
            return field;
        }

        public static void RemoveZip64Blocks(List<ZipGenericExtraField> extraFields)
        {
            List<ZipGenericExtraField> list = new List<ZipGenericExtraField>();
            foreach (ZipGenericExtraField field in extraFields)
            {
                if (field.Tag == 1)
                {
                    list.Add(field);
                }
            }
            foreach (ZipGenericExtraField field2 in list)
            {
                extraFields.Remove(field2);
            }
        }

        //public void WriteBlock(Stream stream);
    }

    public static class ZipFile
    {
        // Methods
        //public static void CreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName);
        //public static void CreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName, CompressionLevel compressionLevel, bool includeBaseDirectory);
        //public static void CreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName, CompressionLevel compressionLevel, bool includeBaseDirectory, Encoding entryNameEncoding);
        //private static void DoCreateFromDirectory(string sourceDirectoryName, string destinationArchiveFileName, CompressionLevel? compressionLevel, bool includeBaseDirectory, Encoding entryNameEncoding);
        //public static void ExtractToDirectory(string sourceArchiveFileName, string destinationDirectoryName);
        //public static void ExtractToDirectory(string sourceArchiveFileName, string destinationDirectoryName, Encoding entryNameEncoding);
        //private static bool IsDirEmpty(DirectoryInfo possiblyEmptyDir);
        public static ZipArchive Open(string archiveFileName, ZipArchiveMode mode)
        {
            return Open(archiveFileName, mode, null);
        }

        public static ZipArchive Open(string archiveFileName, ZipArchiveMode mode, Encoding entryNameEncoding)
        {
            FileMode open;
            FileAccess read;
            FileShare none;
            ZipArchive archive;
            switch (mode)
            {
                case ZipArchiveMode.Read:
                    open = FileMode.Open;
                    read = FileAccess.Read;
                    none = FileShare.Read;
                    break;

                case ZipArchiveMode.Create:
                case ZipArchiveMode.Update:
                    throw new NotImplementedException();    // Read only port for .NET Framework 4

                default:
                    throw new ArgumentOutOfRangeException("mode");
            }
            FileStream stream = null;
            try
            {
                stream = File.Open(archiveFileName, open, read, none);
                archive = new ZipArchive(stream, mode, false, entryNameEncoding);
            }
            catch
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
                throw;
            }
            return archive;
        }

        public static ZipArchive OpenRead(string archiveFileName)
        {
            return Open(archiveFileName, ZipArchiveMode.Read);
        }
    }

    internal static class ZipHelper
    {
        // Fields
        private const int BackwardsSeekingBufferSize = 0x20;
        private static readonly DateTime InvalidDateIndicator;
        internal const ushort Mask16Bit = 0xffff;
        internal const uint Mask32Bit = uint.MaxValue;
        internal const int ValidZipDate_YearMax = 0x83b;
        internal const int ValidZipDate_YearMin = 0x7bc;

        // Methods
        static ZipHelper()
        {
            InvalidDateIndicator = new DateTime(0x7bc, 1, 1, 0, 0, 0);
        }

        internal static uint DateTimeToDosTime(DateTime dateTime)
        {
            int num = (dateTime.Year - 0x7bc) & 0x7f;
            num = (num << 4) + dateTime.Month;
            num = (num << 5) + dateTime.Day;
            num = (num << 5) + dateTime.Hour;
            num = (num << 6) + dateTime.Minute;
            num = (num << 5) + (dateTime.Second / 2);
            return (uint)num;
        }

        internal static DateTime DosTimeToDateTime(uint dateTime)
        {
            int year = 0x7bc + ((int)(dateTime >> 0x19));
            int month = ((int)(dateTime >> 0x15)) & 15;
            int day = ((int)(dateTime >> 0x10)) & 0x1f;
            int hour = ((int)(dateTime >> 11)) & 0x1f;
            int minute = ((int)(dateTime >> 5)) & 0x3f;
            int second = (int)((dateTime & 0x1f) * 2);
            try
            {
                return new DateTime(year, month, day, hour, minute, second, 0);
            }
            catch (ArgumentOutOfRangeException)
            {
                return InvalidDateIndicator;
            }
            catch (ArgumentException)
            {
                return InvalidDateIndicator;
            }
        }

        internal static bool EndsWithDirChar(string test)
        {
            return (Path.GetFileName(test) == "");
        }

        internal static void ReadBytes(Stream stream, byte[] buffer, int bytesToRead)
        {
            int count = bytesToRead;
            int offset = 0;
            while (count > 0)
            {
                int num3 = stream.Read(buffer, offset, count);
                if (num3 == 0)
                {
                    throw new IOException("Zip file corrupt: unexpected end of stream reached.");
                }
                offset += num3;
                count -= num3;
            }
        }

        internal static bool RequiresUnicode(string test)
        {
            foreach (char ch in test)
            {
                if (ch > '\x007f')
                {
                    return true;
                }
            }
            return false;
        }

        private static bool SeekBackwardsAndRead(Stream stream, byte[] buffer, out int bufferPointer)
        {
            if (stream.Position >= buffer.Length)
            {
                stream.Seek((long)-buffer.Length, SeekOrigin.Current);
                ReadBytes(stream, buffer, buffer.Length);
                stream.Seek((long)-buffer.Length, SeekOrigin.Current);
                bufferPointer = buffer.Length - 1;
                return false;
            }
            int position = (int)stream.Position;
            stream.Seek(0L, SeekOrigin.Begin);
            ReadBytes(stream, buffer, position);
            stream.Seek(0L, SeekOrigin.Begin);
            bufferPointer = position - 1;
            return true;
        }

        internal static bool SeekBackwardsToSignature(Stream stream, uint signatureToFind)
        {
            int bufferPointer = 0;
            uint num2 = 0;
            byte[] buffer = new byte[0x20];
            bool flag = false;
            bool flag2 = false;
            while (!flag2 && !flag)
            {
                flag = SeekBackwardsAndRead(stream, buffer, out bufferPointer);
                while ((bufferPointer >= 0) && !flag2)
                {
                    num2 = (num2 << 8) | buffer[bufferPointer];
                    if (num2 == signatureToFind)
                    {
                        flag2 = true;
                    }
                    else
                    {
                        bufferPointer--;
                    }
                }
            }
            if (!flag2)
            {
                return false;
            }
            stream.Seek((long)bufferPointer, SeekOrigin.Current);
            return true;
        }
    }

    public enum ZipArchiveMode
    {
        Create = 1,
        Read = 0,
        Update = 2
    }

    internal enum ZipVersionNeededValues : ushort
    {
        Default = 10,
        Deflate = 20,
        ExplicitDirectory = 20,
        Zip64 = 0x2d
    }

    public static class ZipFileExtensions
    {
        // Methods
        //public static ZipArchiveEntry CreateEntryFromFile(this ZipArchive destination, string sourceFileName, string entryName);
        //public static ZipArchiveEntry CreateEntryFromFile(this ZipArchive destination, string sourceFileName, string entryName, CompressionLevel compressionLevel);
        //internal static ZipArchiveEntry DoCreateEntryFromFile(ZipArchive destination, string sourceFileName, string entryName, CompressionLevel? compressionLevel);
        public static void ExtractToDirectory(this ZipArchive source, string destinationDirectoryName)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (destinationDirectoryName == null)
            {
                throw new ArgumentNullException("destinationDirectoryName");
            }
            string fullName = Directory.CreateDirectory(destinationDirectoryName).FullName;
            foreach (ZipArchiveEntry entry in source.Entries)
            {
                string fullPath = Path.GetFullPath(Path.Combine(fullName, entry.FullName));
                if (!fullPath.StartsWith(fullName, StringComparison.OrdinalIgnoreCase))
                {
                    throw new IOException("Extracting Zip entry would have resulted in a file outside the specified destination directory.");
                }
                if (Path.GetFileName(fullPath).Length == 0)
                {
                    if (entry.Length != 0L)
                    {
                        throw new IOException("Zip entry name ends in directory separator character but contains data.");
                    }
                    Directory.CreateDirectory(fullPath);
                }
                else
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullPath));
                    entry.ExtractToFile(fullPath, false);
                }
            }
        }

        public static void ExtractToFile(this ZipArchiveEntry source, string destinationFileName)
        {
            source.ExtractToFile(destinationFileName, false);
        }

        public static void ExtractToFile(this ZipArchiveEntry source, string destinationFileName, bool overwrite)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }
            if (destinationFileName == null)
            {
                throw new ArgumentNullException("destinationFileName");
            }
            FileMode mode = overwrite ? FileMode.Create : FileMode.CreateNew;
            using (Stream stream = File.Open(destinationFileName, mode, FileAccess.Write, FileShare.None))
            {
                using (Stream stream2 = source.Open())
                {
                    stream2.CopyTo(stream);
                }
            }
            File.SetLastWriteTime(destinationFileName, source.LastWriteTime.DateTime);
        }
    }
}