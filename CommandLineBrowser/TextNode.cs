﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;

    public class TextNode
    {
        // Fields
        private List<TextNode> children;
        private string content;
        private int endIndex;
        private TextNode parent;
        private int startIndex;
        private string startPattern;
        private string endPattern;

        // Methods
        public TextNode(string content, string startPattern, string endPattern)
            : this(content, 0, string.IsNullOrEmpty(content) ? 0 : content.Length, startPattern, endPattern)
        {
        }

        public TextNode(TextNode parent, string startPattern, string endPattern)
            : this(parent, parent.startIndex, startPattern, endPattern)
        {
        }

        private TextNode(TextNode parent, int startIndex, string startPattern, string endPattern)
            : this(parent.content, startIndex, parent.endIndex, startPattern, endPattern)
        {
            this.parent = parent;
            if (this.parent == null || !this.parent.IsValid || this.startIndex >= this.parent.endIndex)
            {
                this.IsValid = false;
            }
        }

        private TextNode(string content, int startIndex, int endIndex, string startPattern, string endPattern)
        {
            this.content = content;
            this.startPattern = startPattern;
            this.endPattern = endPattern;

            this.startIndex = string.IsNullOrEmpty(startPattern) ? startIndex : this.content.IndexOf(startPattern, startIndex, StringComparison.OrdinalIgnoreCase);
            if (this.startIndex >= 0)
            {
                this.startIndex += startPattern.Length;
                if (this.startIndex <= endIndex)
                {
                    if (string.IsNullOrEmpty(endPattern))
                    {
                        this.endIndex = endIndex;
                    }
                    else
                    {
                        this.endIndex = this.content.IndexOf(endPattern, this.startIndex, StringComparison.OrdinalIgnoreCase);
                        if (this.endIndex > endIndex || this.endIndex < 0)
                        {
                            this.IsValid = false;
                        }
                    }
                }
                else
                {
                    this.IsValid = false;
                }
            }
            else
            {
                this.IsValid = false;
            }
        }

        public List<TextNode> ParseChildren(string pattern)
        {
            List<TextNode> list = new List<TextNode>();
            if (!this.IsValid)
            {
                return list;
            }
            int startIndex = this.startIndex;
            while (true)
            {
                TextNode item = new TextNode(this, startIndex, pattern, pattern);
                if (!item.IsValid)
                {
                    item = new TextNode(this, startIndex, pattern, string.Empty);
                }
                if (!item.IsValid)
                {
                    return list;
                }
                if (this.children == null)
                {
                    this.children = new List<TextNode>();
                }
                this.children.Add(item);
                list.Add(item);
                startIndex = item.endIndex;
            }
        }
        
        public TextNode ParseChild(string startPattern, string endPattern)
        {
            if (!this.IsValid)
            {
                return null;
            }
            TextNode item = new TextNode(this, startPattern, endPattern);
            if (item.IsValid)
            {
                if (this.children == null)
                {
                    this.children = new List<TextNode>();
                }
                this.children.Add(item);
            }
            return item;
        }

        public List<TextNode> ParseChildren(string startPattern, string endPattern)
        {
            List<TextNode> list = new List<TextNode>();
            if (!this.IsValid)
            {
                return list;
            }
            int startIndex = this.startIndex;
            while (true)
            {
                TextNode item = new TextNode(this, startIndex, startPattern, endPattern);
                if (!item.IsValid)
                {
                    return list;
                }
                if (this.children == null)
                {
                    this.children = new List<TextNode>();
                }
                this.children.Add(item);
                list.Add(item);
                startIndex = item.endIndex + endPattern.Length;
            }
        }

        public TextNode ParseNextChild(TextNode lastChild, string startPattern, string endPattern)
        {
            if (!this.IsValid)
            {
                return null;
            }
            if (lastChild == null)
            {
                return this.ParseChild(startPattern, endPattern);
            }
            if (!lastChild.IsValid)
            {
                return lastChild;
            }
            int startIndex = lastChild.EndIndex;
            TextNode item = new TextNode(this, startIndex, startPattern, endPattern);
            if (item.IsValid)
            {
                if (this.children == null)
                {
                    this.children = new List<TextNode>();
                }
                this.children.Add(item);
            }
            return item;
        }

        public override string ToString()
        {
            if (this.IsValid)
            {
                return this.content.Substring(this.startIndex, this.endIndex - this.startIndex);
            }
            return string.Empty;
        }

        // Properties
        public IEnumerable<TextNode> Children
        {
            get
            {
                return this.children;
            }
        }

        public int StartIndex
        {
            get
            {
                return this.startIndex - this.startPattern.Length;
            }
        }

        public int ContentStartIndex
        {
            get
            {
                return this.startIndex;
            }
        }

        public int ContentEndIndex
        {
            get
            {
                return this.endIndex;
            }
        }

        public int EndIndex
        {
            get
            {
                return this.endIndex + this.endPattern.Length;
            }
        }

        public bool IsValid
        {
            get
            {
                return ((!string.IsNullOrEmpty(this.content) && (this.startIndex >= 0)) && (this.endIndex >= this.startIndex));
            }
            set
            {
                if (value)
                {
                    throw new Exception();
                }
                this.endIndex = -1;
            }
        }

        public string RawValue
        {
            get
            {
                if (!this.IsValid)
                {
                    return string.Empty;
                }
                return this.content.Substring(this.startIndex, this.endIndex - this.startIndex);
            }
        }

        public string Value
        {
            get
            {
                if (!this.IsValid)
                {
                    return string.Empty;
                }

                return new TagStripper().Strip(this.content.Substring(this.startIndex, this.endIndex - this.startIndex));
            }
        }
    }

    public class TaggedNode : TextNode
    {
        private TaggedNode(TextNode node)
            : base(node, string.Empty, string.Empty)
        {
        }

        public TaggedNode(TextNode parent, string tagName)
            : base(parent, string.Format("<{0}", tagName), string.Format("</{0}>", tagName))
        {
        }

        public string Attribute(string name)
        {
            TextNode node = new TextNode(this, string.Empty, ">");
            if (!node.IsValid)
            {
                return null;
            }
                
            TextNode attrNameNode = new TextNode(node, name, "=");
            if (!attrNameNode.IsValid)
            {
                return null;
            }

            TextNode attrValueNode = this.ParseNextChild(attrNameNode, "\"", "\"");
            if (!attrNameNode.IsValid)
            {
                attrValueNode = this.ParseNextChild(attrNameNode, "'", "'");
            }

            if (!attrValueNode.IsValid)
            {
                return string.Empty;
            }

            return attrValueNode.Value.Trim();
        }

        public new string Value
        {
            get
            {
                TextNode valueNode = new TextNode(this, ">", string.Empty);
                if (!valueNode.IsValid)
                {
                    return null;
                }

                return valueNode.Value.Trim();
            }
        }

        public static List<TaggedNode> ParseChildren(TextNode parent, string tagName)
        {
            return ParseChildren(parent, tagName, null);
        }

        public static List<TaggedNode> ParseChildren(TextNode parent, string tagName, string attrName)
        {
            if (!parent.IsValid)
            {
                return new List<TaggedNode>();
            }

            List<TaggedNode> children = new List<TaggedNode>();
            foreach (TextNode childNode in parent.ParseChildren(string.Format("<{0}", tagName), string.Format("</{0}>", tagName)))
            {
                TaggedNode node = new TaggedNode(childNode);
                if (!string.IsNullOrEmpty(attrName) && string.IsNullOrEmpty(node.Attribute(attrName)))
                {
                    continue;
                }
                
                children.Add(node);
            }

            return children;
        }
    }
}
