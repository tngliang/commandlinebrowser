﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading;

    public class HttpRequest
    {
        public const string DefaultContentType = "application/x-www-form-urlencoded";
        public const string DefaultUserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
        public const int DefaultTimeout = 60000;
        public const int DefaultBlockLength = 8192;

        internal Logger Logger = Logger.Singleton;
        internal Func<Uri, HttpWebRequest> CreateWebRequest = uri => (HttpWebRequest)WebRequest.Create(uri);
        internal Func<HttpWebRequest, HttpWebResponse> GetWebResponse = request => (HttpWebResponse)request.GetResponse();
        private HttpSession session;
        private string referer;

        public HttpRequest()
            : this(new HttpSession())
        {
        }

        public HttpRequest(HttpSession session, string referer = null)
        {
            this.session = session;
            this.referer = referer;
        }

        public IEnumerable<string> ReadLines(Uri uri, bool isPost = false, IEnumerable<KeyValuePair<string, string>> parameters = null, string contentType = DefaultContentType, string userAgent = DefaultUserAgent, int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(uri, isPost, parameters, contentType, userAgent, timeout), ReadLines));
        }

        public string ReadToEnd(Uri uri, bool isPost = false, IEnumerable<KeyValuePair<string, string>> parameters = null, string contentType = DefaultContentType, string userAgent = DefaultUserAgent, int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(uri, isPost, parameters, contentType, userAgent, timeout), ReadAllText));
        }

        public TResult Read<TResult>(Uri uri,
            Func<HttpWebResponse, TResult> decoder,
            bool isPost = false,
            IEnumerable<KeyValuePair<string, string>> parameters = null,
            string contentType = DefaultContentType,
            string userAgent = DefaultUserAgent,
            int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(uri, isPost, parameters, contentType, userAgent, timeout), decoder));
        }

        private HttpWebRequest CreateRequest(Uri uri, bool isPost, IEnumerable<KeyValuePair<string, string>> parameters, string contentType, string userAgent, int timeout)
        {
            HttpWebRequest request = CreateWebRequest(uri);
            request.AllowAutoRedirect = true;
            request.ContentType = contentType;
            request.UserAgent = userAgent;
            request.Timeout = timeout;
            if (!string.IsNullOrEmpty(this.referer))
            {
                request.Referer = this.referer;
            }
            this.referer = uri.Host;
            if (session != null)
            {
                session.Apply(request);
            }

            if (isPost)
            {
                request.Method = "POST";
                SetParams(request, parameters);
            }
            return request;
        }

        private static void SetParams(HttpWebRequest request, IEnumerable<KeyValuePair<string, string>> parameters)
        {
            if (parameters != null)
            {
                StringBuilder builder = new StringBuilder();
                foreach (KeyValuePair<string, string> pair in parameters)
                {
                    if (builder.Length == 0)
                    {
                        builder.AppendFormat("{0}={1}", pair.Key, Uri.EscapeDataString(pair.Value));
                    }
                    else
                    {
                        builder.AppendFormat("&{0}={1}", pair.Key, Uri.EscapeDataString(pair.Value));
                    }
                }

                byte[] bytes = Encoding.UTF8.GetBytes(builder.ToString());
                request.ContentLength = bytes.Length;
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
            }
        }

        public HttpSession Session
        {
            get
            {
                return session;
            }
        }

        private T Read<T>(HttpWebRequest request, Func<HttpWebResponse, T> decoder)
        {
            Logger.LogInfo("Read", request.RequestUri);
            return decoder(GetRedirectableResponse(request, session));
        }

        private static IEnumerable<string> ReadLines(HttpWebResponse response)
        {
            try
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        while (!sr.EndOfStream)
                        {
                            yield return sr.ReadLine();
                        }
                    }
                }
            }
            finally
            {
                response.Close();
            }
        }

        private static string ReadAllText(HttpWebResponse response)
        {
            try
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            finally
            {
                response.Close();
            }
        }

        private HttpWebResponse GetRedirectableResponse(HttpWebRequest request, HttpSession session)
        {
            HttpWebResponse response = GetWebResponse(request);

            while (request.AllowAutoRedirect && response.StatusCode == HttpStatusCode.Found)
            {
                Uri newUri = new Uri(response.Headers[HttpResponseHeader.Location]);
                if (string.Compare(newUri.AbsoluteUri, request.Address.AbsoluteUri, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    break;
                }

                request = CreateRequest(newUri, false, null, request.ContentType, request.UserAgent, request.Timeout);
                response = GetRedirectableResponse(request, session);
            }

            session.ReadResponse(response);
            return response;
        }
    }
}
