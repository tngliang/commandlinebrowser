﻿namespace CommandLineBrowser
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using System.Threading;
    using System.Text;

    class Logger
    {
        static Logger()
        {
        }

        private Logger()
        {
        }

        [DebuggerNonUserCode]
        public void LogInfo(string message, params object[] parameters)
        {
            string formattedMessage = string.Format("{0} {1} {2}: {3} {4} params=({5})", Thread.CurrentThread.ManagedThreadId, DateTime.Now.ToString(), Helpers.ApplicationName, Caller, message, FormatParameters(parameters));
            Console.WriteLine(formattedMessage);
#if TRACE
            System.Diagnostics.Debug.Print(formattedMessage);
#endif
        }

        [DebuggerNonUserCode]
        public void LogError(string message, params object[] parameters)
        {
            string formattedMessage = string.Format("{0} {1} {2} ERROR: {3} {4} params=({5})", Thread.CurrentThread.ManagedThreadId, DateTime.Now.ToString(), Helpers.ApplicationName, Caller, message, FormatParameters(parameters));
            Console.Error.WriteLine(formattedMessage);
#if TRACE
            System.Diagnostics.Debug.Print(formattedMessage);
#endif
        }

        [DebuggerNonUserCode]
        public void LogException(string message, Exception ex, params object[] parameters)
        {
            string formattedMessage = string.Format("{0} {1} {2} EXCEPTION: {3} {4} params=({5})", Thread.CurrentThread.ManagedThreadId, DateTime.Now.ToString(), Helpers.ApplicationName, Caller, ex.Message, FormatParameters(parameters));
            Console.Error.WriteLine(formattedMessage);
#if TRACE
            System.Diagnostics.Debug.Print(formattedMessage);
#endif
        }

        private string Caller
        {
            [DebuggerNonUserCode]
            get
            {
                int skip = 2;
                for (StackFrame frame = new StackFrame(skip); frame != null; frame = new StackFrame(++skip))
                {
                    MethodBase method = frame.GetMethod();
                    if (method.DeclaringType != null && method.DeclaringType.Namespace != "System.Dynamic" && !method.DeclaringType.Name.StartsWith("Runtime"))
                    {
                        object[] nonUserCodeAttributes = method.GetCustomAttributes(typeof(DebuggerNonUserCodeAttribute), true);
                        if (nonUserCodeAttributes == null || nonUserCodeAttributes.Length == 0)
                        {
                            return FormatMethod(method);
                        }
                    }
                }

                return string.Empty;
            }
        }

        private static string FormatMethod(MethodBase method)
        {
            if (method.DeclaringType != null)
            {
                return string.Format("{0}.{1}", method.DeclaringType.FullName, method.Name);
            }
            else
            {
                return method.Name;
            }
        }

        private static string FormatParameters(params object[] parameters)
        {
            StringBuilder builder = new StringBuilder();
            foreach (object parameter in parameters)
            {
                if (builder.Length > 0)
                {
                    builder.Append(", ");
                }

                if (parameter == null)
                {
                    builder.Append("(null)");
                }
                else
                {
                    builder.Append(parameter.ToString());
                }
            }
            return builder.ToString();
        }

        public static Logger Singleton
        {
            get
            {
                return SingletonHolder.Instance;
            }
        }

        private sealed class SingletonHolder
        {
            internal static Logger Instance = new Logger();
        }
    }
}
