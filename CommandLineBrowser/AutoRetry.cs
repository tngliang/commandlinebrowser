﻿namespace CommandLineBrowser
{
    using System;
    using System.Threading;

    /// <summary>
    /// Generic class to help automatically retry an operation with ever increasing sleep.
    /// </summary>
    /// <typeparam name="T">Return type of the operation</typeparam>
    public class AutoRetry<T>
    {
        public const int DefaultMaxAutoRetry = 10;
        public const int DefaultSleepBetweenRetry = 100;

        internal Logger Logger = Logger.Singleton;

        private int maxRetry;
        private int sleepBetweenRetry;

        /// <summary>
        /// Constructor
        /// maxRetry = 10, sleepBetweenRetry = 100 millisecond means retry up to 10 times and pause for 100, 200, 300 ... 1000 milliseconds between each retry.
        /// maxRetry = 10, sleepBetweenRetry = 0 means immediately retry up to 10 times with no pauses
        /// </summary>
        /// <param name="maxRetry">Max amount of retry, default to 10</param>
        /// <param name="sleepBetweenRetry">Number of milliseconds to sleep between retry</param>
        public AutoRetry(int maxRetry = DefaultMaxAutoRetry, int sleepBetweenRetry = DefaultSleepBetweenRetry)
        {
            this.maxRetry = maxRetry;
            this.sleepBetweenRetry = sleepBetweenRetry;
        }

        /// <summary>
        /// Execute operation and automatically retry if exception is encountered.
        ///
        /// On failure the last exception caught from the last try is rethrown to the caller.
        /// </summary>
        /// <param name="action">Operation delegate</param>
        /// <returns>Operation Result</returns>
        public T Execute(Func<T> action)
        {
            Exception lastException = null;
            for (int retry = 0; retry < maxRetry; ++retry)
            {
                if (retry > 0)
                {
                    Thread.Sleep(sleepBetweenRetry);
                }

                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    Logger.LogInfo(ex.Message, retry);
                    lastException = ex;
                }
            }

            throw lastException;
        }
    }

    /// <summary>
    /// Static  for AutoRetry
    /// </summary>
    public static class AutoRetry
    {
        /// <summary>
        /// Supports directly invokation of AutoRetry.Run(operation);
        /// </summary>
        /// <typeparam name="T">Operation return type</typeparam>
        /// <param name="action">Operation delegate</param>
        /// <returns>Operation result</returns>
        public static T Run<T>(Func<T> action)
        {
            try
            {
                return new AutoRetry<T>().Execute(action);
            }
            catch (Exception ex)
            {
                Logger.Singleton.LogException("FAILED with max retries.", ex);
            }

            return default(T);
        }
    }
}
