﻿namespace CommandLineBrowser
{
    static class Debug
    {
        public static void Assert(bool condition, params string[] messages)
        {
#if TRACE
            System.Diagnostics.Debug.Assert(condition, string.Join(" ", messages));
#else
            if (!condition)
            {
                Logger.Singleton.LogError("ASSERT failed", messages);
            }
#endif
        }
    }
}
