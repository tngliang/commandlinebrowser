﻿namespace CommandLineBrowser
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Text;

    public static class Helpers
    {
        public const int DefaultMaxRetry = 10;

        private static string applicationName;
        public static string ApplicationName
        {
            get
            {
                if (string.IsNullOrEmpty(applicationName))
                {
                    Assembly entryAssembly = Assembly.GetEntryAssembly();
                    if (entryAssembly == null)
                    {
                        entryAssembly = Assembly.GetCallingAssembly();
                    }
                    applicationName = entryAssembly.GetName().Name;
                }
                return applicationName;
            }
        }

        public static void SaveTextFile(string filePath, string fileContent)
        {
            string backupFilePath = filePath + ".tmp";
            try
            {
                string directory = Path.GetDirectoryName(filePath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                if (File.Exists(filePath))
                {
                    File.Move(filePath, backupFilePath);
                }

                File.WriteAllText(filePath, fileContent, Encoding.ASCII);

                if (File.Exists(backupFilePath))
                {
                    File.Delete(backupFilePath);
                }

                return;
            }
            catch (Exception)
            {
                if (File.Exists(backupFilePath))
                {
                    File.Move(backupFilePath, filePath);
                }
                throw;
            }
        }

        /// <summary>
        /// Rename an existing disk file (in case it is locked)
        /// </summary>
        /// <param name="fileName">disk file name</param>
        /// <returns>new file name after rename</returns>
        public static string RenameIfExists(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            string newFileName = fileName;
            for (int i = 0; i < int.MaxValue; ++i)
            {
                newFileName = string.Format("{0}.{1}", fileName, i);
                if (!File.Exists(newFileName))
                {
                    break;
                }
            }

            File.Move(fileName, newFileName);
            return newFileName;
        }
    }
}
