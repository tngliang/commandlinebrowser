﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;

    class FtpBrowser
    {
        public const int DefaultTimeout = 300000;
        public const int DefaultBlockLength = 8192;

        private readonly char[] crLF = "\r\n".ToCharArray();

        internal Logger Logger = Logger.Singleton;
        internal Func<string, string, FtpWebRequest> CreateFtpWebRequest = (hostName, path) => (FtpWebRequest)FtpWebRequest.Create(string.Format("ftp://{0}/{1}", hostName, path));

        private string hostName;

        public FtpBrowser(string ftpServer)
        {
            if (string.IsNullOrEmpty(ftpServer))
            {
                throw new ArgumentNullException("ftpServer");
            }

            this.hostName = ftpServer;
        }

        public string HostName
        {
            get
            {
                return hostName;
            }
        }

        public IEnumerable<string> ListDirectoryShort(string directory)
        {
            string content = AutoRetry.Run(() => Read(CreateRequest(directory, WebRequestMethods.Ftp.ListDirectory, false, DefaultTimeout), sr => sr.ReadToEnd()).FirstOrDefault());
            if (!string.IsNullOrEmpty(content))
            {
                foreach (string item in content.Split(crLF))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        yield return Path.Combine(directory, Path.GetFileName(item)).Replace('\\', '/');
                    }
                }
            }
            yield break;
        }

        public IEnumerable<FtpEntry> ListDirectory(string directory)
        {
            string content = AutoRetry.Run(() => Read(CreateRequest(directory, WebRequestMethods.Ftp.ListDirectoryDetails, false, DefaultTimeout), sr => sr.ReadToEnd()).FirstOrDefault());
            if (!string.IsNullOrEmpty(content))
            {
                foreach (string item in content.Split(crLF))
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        FtpEntry info;
                        if (FtpEntry.TryParse(item, out info))
                        {
                            info.Path = Path.Combine(directory, info.Name).Replace('\\', '/');
                            yield return info;
                        }
                    }
                }
            }
            yield break;
        }

        public string ReadToEnd(string path, string method = WebRequestMethods.Ftp.DownloadFile, bool useBinary = false, int timeout = DefaultTimeout, Func<Stream, StreamReader> streamDecoder = null)
        {
            return AutoRetry.Run(() => Read(CreateRequest(path, method, useBinary, timeout), sr => sr.ReadToEnd(), streamDecoder).FirstOrDefault());
        }

        public IEnumerable<string> ReadLines(string path, string method = WebRequestMethods.Ftp.DownloadFile, bool useBinary = false, int timeout = DefaultTimeout, Func<Stream, StreamReader> streamDecoder = null)
        {
            return AutoRetry.Run(() => Read(CreateRequest(path, method, useBinary, timeout), sr => sr.ReadLine(), streamDecoder));
        }

        public IEnumerable<int> Read(string path, string method = WebRequestMethods.Ftp.DownloadFile, bool useBinary = false, int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(path, method, useBinary, timeout), sr => sr.Read()));
        }

        public IEnumerable<int> Read(string path, char[] buffer, int index, int count, string method = WebRequestMethods.Ftp.DownloadFile, bool useBinary = false, int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(path, method, useBinary, timeout), sr => sr.Read(buffer, index, count)));
        }

        public IEnumerable<int> ReadBlock(string path, char[] buffer, int index = 0, int count = DefaultBlockLength, string method = WebRequestMethods.Ftp.DownloadFile, bool useBinary = false, int timeout = DefaultTimeout)
        {
            return AutoRetry.Run(() => Read(CreateRequest(path, method, useBinary, timeout), sr => sr.ReadBlock(buffer, index, count)));
        }

        private FtpWebRequest CreateRequest(string path, string method, bool useBinary, int timeout)
        {
            FtpWebRequest request = CreateFtpWebRequest(this.HostName, path);
            request.Method = method;
            request.UseBinary = useBinary;
            request.Timeout = timeout;
            request.KeepAlive = true;
            request.UsePassive = true;
            return request;
        }

        private IEnumerable<T> Read<T>(FtpWebRequest request, Func<StreamReader, T> reader, Func<Stream, StreamReader> decoder = null)
        {
            Logger.LogInfo("Read", request.RequestUri);
            if (decoder == null)
            {
                decoder = s => new StreamReader(s, Encoding.UTF8);
            }

            return ReadStream(request, reader, decoder);
        }

        private IEnumerable<T> ReadStream<T>(FtpWebRequest request, Func<StreamReader, T> reader, Func<Stream, StreamReader> decoder)
        {
            WebResponse response = null;
            Stream responseStream = null;
            StreamReader sr = null;
            try
            {
                response = request.GetResponse();
                responseStream = response.GetResponseStream();
                sr = decoder(responseStream);

                while (!sr.EndOfStream)
                {
                    yield return reader(sr);
                }
            }
            finally
            {
                Close(response, responseStream, sr);
            }
            yield break;
        }

        private static void Close(WebResponse response, Stream responseStream, StreamReader sr)
        {
            if (sr != null)
            {
                try
                {
                    sr.Close();
                }
                catch (WebException ex)
                {
                    Logger.Singleton.LogInfo("Force-terminated web stream.", ex.Message, response.ResponseUri);
                }
            }
            if (responseStream != null)
            {
                try
                {
                    responseStream.Close();
                }
                catch (WebException ex)
                {
                    Logger.Singleton.LogInfo("Force-terminated web stream.", ex.Message, response.ResponseUri);
                }
            }
            if (response != null)
            {
                response.Close();
            }
        }
    }
}
