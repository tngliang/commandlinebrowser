﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    class TagStripper
    {
        public string Strip(string input, char Separator = ' ', char CompositeCharSubstitute = ' ', Dictionary<string, string> tagSubstitutes = null)
        {
            StringBuilder builder = new StringBuilder();
            StringBuilder tagBuilder = new StringBuilder();

            int backtrackCompositeIndex;

            States parserState = States.Normal;
            int i = 0;
            do
            {
                backtrackCompositeIndex = -1;
                for (; i < input.Length; ++i)
                {
                    char c = input[i];
                    switch (parserState)
                    {
                        case States.Normal:
                            if (c == '<')
                            {
                                tagBuilder.Clear();
                                parserState = States.InTag;
                            }
                            else if (c == '&')
                            {
                                backtrackCompositeIndex = i;
                                parserState = States.Composite;
                            }
                            else
                            {
                                builder.Append(c);
                            }
                            break;
                        case States.InTag:
                            if (c == '>')
                            {
                                if (tagSubstitutes == null)
                                {
                                    builder.Append(Separator);
                                }
                                else
                                {
                                    string[] tagParts = tagBuilder.ToString().Split(' ');
                                    if (tagParts.Length > 0 && tagSubstitutes.ContainsKey(tagParts[0]))
                                    {
                                        builder.Append(tagSubstitutes[tagParts[0]]);
                                    }
                                }
                                parserState = States.Normal;
                            }
                            else
                            {
                                tagBuilder.Append(c);
                            }
                            break;
                        case States.Composite:
                            if (c == '#')
                            {
                                parserState = States.CompositeOct;
                            }
                            else if (!char.IsLetterOrDigit(c))
                            {
                                i = BacktrackIncorrectComposite(builder, ref backtrackCompositeIndex, out parserState);
                            }
                            else
                            {
                                parserState = States.CompositeString;
                            }
                            break;
                        case States.CompositeString:
                            if (c == ';')
                            {
                                builder.Append(SubstituteCompositeChar(input.Substring(backtrackCompositeIndex, i - backtrackCompositeIndex), CompositeCharSubstitute));
                                backtrackCompositeIndex = -1;
                                parserState = States.Normal;
                            }
                            else if (!char.IsLetterOrDigit(c))
                            {
                                i = BacktrackIncorrectComposite(builder, ref backtrackCompositeIndex, out parserState);
                            }
                            break;
                        case States.CompositeOct:
                            if (char.ToLowerInvariant(c) == 'x')
                            {
                                parserState = States.CompositeHex;
                            }
                            else if (c == ';')
                            {
                                builder.Append(SubstituteCompositeChar(input.Substring(backtrackCompositeIndex, i - backtrackCompositeIndex), CompositeCharSubstitute));
                                backtrackCompositeIndex = -1;
                                parserState = States.Normal;
                            }
                            else if (!char.IsDigit(c))
                            {
                                i = BacktrackIncorrectComposite(builder, ref backtrackCompositeIndex, out parserState);
                            }
                            break;
                        case States.CompositeHex:
                            if (c == ';')
                            {
                                builder.Append(SubstituteCompositeChar(input.Substring(backtrackCompositeIndex, i - backtrackCompositeIndex), CompositeCharSubstitute));
                                backtrackCompositeIndex = -1;
                                parserState = States.Normal;
                            }
                            else if (!(char.IsDigit(c) || ('A' <= c && c <= 'F' || 'a' <= c && c <= 'f')))
                            {
                                i = BacktrackIncorrectComposite(builder, ref backtrackCompositeIndex, out parserState);
                            }
                            break;
                    }
                }
                if (backtrackCompositeIndex >= 0)
                {
                    i = BacktrackIncorrectComposite(builder, ref backtrackCompositeIndex, out parserState);
                }
            }
            while (backtrackCompositeIndex >= 0);

            return builder.ToString();
        }

        private int BacktrackIncorrectComposite(StringBuilder builder, ref int compositeBackTrackIndex, out States parserState)
        {
            builder.Append("&");
            int backTrackToIndex = compositeBackTrackIndex;
            compositeBackTrackIndex = -1;
            parserState = States.Normal;
            return backTrackToIndex;
        }

        private char SubstituteCompositeChar(string composite, char Separator)
        {
            if (string.Compare(composite, "&amp") == 0)
            {
                return '&';
            }

            if (string.Compare(composite, "&#147") == 0 || string.Compare(composite, "&#148") == 0 || string.Compare(composite, "&#8220") == 0 || string.Compare(composite, "&#8221") == 0)
            {
                return '\"';
            }

            return Separator;
        }

        private enum States
        {
            Normal,
            InTag,
            Composite,
            CompositeString,
            CompositeOct,
            CompositeHex
        }
    }
}
