﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;

    [Serializable]
    class Entity<T> : IEquatable<Entity<T>>
    {
        public bool IsValid { get; set; }

        public Entity()
        {
            this.IsValid = false;
        }

        //public Entity(IDataRecord record)
        //{
        //    this.IsValid = Load(record);
        //}

        //public bool Load(IEnumerable<IDataRecord> records)
        //{
        //    if (records == null)
        //    {
        //        return false;
        //    }

        //    foreach (IDataRecord record in records)
        //    {
        //        if (!Load(record))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        //private bool Load(IDataRecord record)
        //{
        //    if (record == null)
        //    {
        //        return false;
        //    }

        //    Dictionary<string, object> data = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        //    for (int i = 0; i < record.FieldCount; ++i)
        //    {
        //        data.Add(record.GetName(i), record.GetValue(i));
        //    }

        //    foreach (PropertyInfo info in EntitySingleton.Properties)
        //    {
        //        if (data.ContainsKey(info.Name))
        //        {
        //            object value = data[info.Name];
        //            if (value == null || value == DBNull.Value)
        //            {
        //                info.GetSetMethod().Invoke(this, new object[] { null });
        //            }
        //            else if (value.GetType() == info.PropertyType ||
        //                info.PropertyType.IsGenericType && info.PropertyType.GetGenericArguments().Length == 1 &&
        //                value.GetType() == info.PropertyType.GetGenericArguments()[0])
        //            {
        //                info.GetSetMethod().Invoke(this, new object[] { value });
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}

        public override bool Equals(object obj)
        {
            return ((IEquatable<Entity<T>>)this).Equals(obj as Entity<T>);
        }

        public override int GetHashCode()
        {
            object[] fields = new object[EntitySingleton.Properties.Length];
            for (int i = 0; i < fields.Length; ++i)
            {
                fields[i] = EntitySingleton.Properties[i].GetGetMethod().Invoke(this, new object[] { });
            }
            return HashFunction.GetHashCode(fields);
        }

        bool IEquatable<Entity<T>>.Equals(Entity<T> other)
        {
            if (this == null && other == null)
            {
                return true;
            }

            if (other == null)
            {
                return false;
            }

            foreach (PropertyInfo info in EntitySingleton.Properties)
            {
                object value1 = info.GetGetMethod().Invoke(this, new object[] { });
                object value2 = info.GetGetMethod().Invoke(other, new object[] { });

                if (value1 == null)
                {
                    return value2 == null;
                }
                else if (!value1.Equals(value2))
                {
                    return false;
                }
            }

            return true;
        }

        private sealed class EntitySingleton
        {
            public static readonly PropertyInfo[] Properties = typeof(T).GetProperties(
                BindingFlags.OptionalParamBinding | BindingFlags.SetProperty | BindingFlags.GetProperty |
                BindingFlags.InvokeMethod | BindingFlags.FlattenHierarchy | BindingFlags.NonPublic | BindingFlags.Public |
                BindingFlags.Static | BindingFlags.Instance | BindingFlags.IgnoreCase);
        }
    }
}
