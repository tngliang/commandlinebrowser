﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class StringExtension
    {
        public static string[] SplitAndTrim(this string input, StringSplitOptions options = StringSplitOptions.None, params char[] separators)
        {
            string[] parts = input.Split(separators, options);
            for (int i = 0; i < parts.Length; ++i)
            {
                parts[i] = parts[i].Trim();
            }
            return parts;
        }

        public static string Quote(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "\"\"";
            }
            if (input.StartsWith("\"") && input.EndsWith("\""))
            {
                return input;
            }
            return string.Format("\"{0}\"", input.Replace("\"", "\"\""));
        }

        public static string Unquote(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                input = input.Trim();
                if (input.StartsWith("\"") && input.EndsWith("\""))
                {
                    return input.Substring(1, input.Length - 2).Replace("\"\"", "\"");
                }
            }
            return input;
        }

        public static bool TryParsePrice(this string input, out double value)
        {
            if (string.IsNullOrEmpty(input))
            {
                value = 0.0;
                return true;
            }
            input = input.Trim();
            if (input.StartsWith("US$"))
            {
                input = input.Substring(3);
            }
            if (input.StartsWith("$"))
            {
                input = input.Substring(1);
            }
            if (input.StartsWith("-US$"))
            {
                input = string.Format("-{0}", input.Substring(4));
            }
            if (input.StartsWith("-$"))
            {
                input = string.Format("-{0}", input.Substring(2));
            }
            if (IsNAN(input))
            {
                value = double.NaN;
                return true;
            }
            long unit = 1L;
            int index = input.IndexOf("Mil", StringComparison.OrdinalIgnoreCase);
            if (index >= 0)
            {
                input = input.Substring(0, index).Trim();
                unit = 1000000;
            }
            index = input.IndexOf("Bil", StringComparison.OrdinalIgnoreCase);
            if (index >= 0)
            {
                input = input.Substring(0, index).Trim();
                unit = 1000000000;
            }
            double num;
            if (!double.TryParse(input, out num))
            {
                value = double.NaN;
                return false;
            }
            value = (num * unit);
            return true;
        }

        public static double AsPrice(this string input)
        {
            double value;
            if (TryParsePrice(input, out value))
            {
                return value;
            }
            else
            {
                throw new InvalidCastException(string.Format("'{0}' is not a price", input));
            }
        }

        public static bool TryParsePercentage(this string input, out double value)
        {
            string originalValue = input;
            if (string.IsNullOrEmpty(input))
            {
                value = 0.0;
                return true;
            }

            input = input.Trim();
            bool percentage = input.EndsWith("%");
            if (percentage)
            {
                input = input.Substring(0, input.Length - 1);
            }

            if (IsNAN(input))
            {
                value = double.NaN;
                return true;
            }

            double doubleValue;
            if (double.TryParse(input, out doubleValue))
            {
                value = percentage ? doubleValue / 100.0 : doubleValue;
                return true;
            }
            else
            {
                value = double.NaN;
                return false;
            }
        }

        public static double AsPercentage(this string input)
        {
            double value;
            if (TryParsePercentage(input, out value))
            {
                return value;
            }
            else
            {
                throw new InvalidCastException(string.Format("'{0}' is not a percentage", input));
            }
        }

        public delegate bool TryParseDelegate<T>(string input, out T value);
        public static bool TryParse<T>(this string input, TryParseDelegate<T> parser, out T value) where T : struct
        {
            if (string.IsNullOrEmpty(input))
            {
                value = default(T);
                return true;
            }
            input = input.Trim();
            if (IsNAN(input))
            {
                value = default(T);
                return true;
            }
            return parser(input, out value);
        }

        public static T As<T>(this string input, TryParseDelegate<T> parser) where T : struct
        {
            T value;
            if (TryParse(input, parser, out value))
            {
                return value;
            }
            else
            {
                throw new InvalidCastException(string.Format("'{0}' cannot be converted to {1}", input, typeof(T).Name)); 
            }
        }

        private static bool IsNAN(string input)
        {
            if (string.Compare(input, "NA", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "N/A", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "#N/A", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "ND", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "=NA()", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "Not Applicable", StringComparison.OrdinalIgnoreCase) == 0 ||
                string.Compare(input, "--", StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }
            return false;
        }
    }
}
