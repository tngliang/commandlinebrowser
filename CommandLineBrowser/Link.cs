﻿namespace CommandLineBrowser
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Link
    {
        private Link()
        {
        }

        public string Name { get; set; }
        public Uri Value { get; set; }
        public bool Post { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
        public string Referer { get; set; }

        public static Link Create(string url, Uri backUri = null, string name = null, bool isPost = false, Dictionary<string, string> parameters = null)
        {
            string baseUrl = null;
            string referer = null;

            if (backUri != null)
            {
                baseUrl = backUri.GetLeftPart(UriPartial.Authority);
                referer = backUri.ToString();
            }

            Link link = new Link() { Name = name, Post = isPost, Parameters = parameters, Referer = referer };
            if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                link.Value = new Uri(url, UriKind.Absolute);
                return link;
            }
            else if (Uri.IsWellFormedUriString(url, UriKind.Relative))
            {
                link.Value = new Uri(new Uri(baseUrl, UriKind.Absolute), url);
                return link;
            }
            else
            {
                string newUrl = ReformatBadUrl(url);
                if (string.Compare(newUrl, url, StringComparison.OrdinalIgnoreCase) != 0)
                {
                    return Create(newUrl, backUri, name, isPost, parameters);
                }

                throw new CommandLineBrowserException(string.Format("Bad url '{0}'", url));
            }
        }

        private static string ReformatBadUrl(string url)
        {
            States state = States.ExpectProtocol;
            StringBuilder urlBuilder = new StringBuilder();
            bool inQuote = false;
            bool inDoubleQuote = false;
            StringBuilder dataBuilder = new StringBuilder();
            foreach (char c in url)
            {
                switch (state)
                {
                    case States.ExpectProtocol:
                        urlBuilder.Append(c);
                        if (c == ':' || c == '/')
                        {
                            state = States.ExpectUrl;
                        }
                        break;
                    case States.ExpectUrl:
                        urlBuilder.Append(c);
                        if (c == '?')
                        {
                            state = States.ExpectParameterName;
                        }
                        break;
                    case States.ExpectParameterName:
                        urlBuilder.Append(c);
                        if (c == '=')
                        {
                            state = States.ExpectParameterValue;
                        }
                        break;
                    case States.ExpectParameterValue:
                        if (c == '&')
                        {
                            urlBuilder.AppendFormat("{0}&", Uri.EscapeDataString(Uri.UnescapeDataString(dataBuilder.ToString())));
                            inQuote = false;
                            inDoubleQuote = false;
                            dataBuilder.Clear();
                            state = States.ExpectParameterName;
                        }
                        else
                        {
                            if (c == '\'')
                            {
                                inQuote = !inQuote;
                            }
                            else if (c == '"')
                            {
                                inDoubleQuote = !inDoubleQuote;
                            }
                            else
                            {
                                dataBuilder.Append(c);
                            }
                        }
                        break;
                }
            }

            if (dataBuilder.Length > 0)
            {
                urlBuilder.Append(Uri.EscapeDataString(Uri.UnescapeDataString(dataBuilder.ToString())));
            }

            return urlBuilder.ToString();
        }

        private enum States
        {
            ExpectProtocol,
            ExpectUrl,
            ExpectParameterName,
            ExpectParameterValue,
        }
    }
}
