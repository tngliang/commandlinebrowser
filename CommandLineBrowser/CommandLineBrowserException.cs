﻿namespace CommandLineBrowser
{
    using System;

    public class CommandLineBrowserException : Exception
    {
        public CommandLineBrowserException(string message)
            : base(message)
        {
        }

        public CommandLineBrowserException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
