﻿namespace CommandLineBrowser
{
    using System;

    class FtpEntry
    {
        private static readonly char[] Whitespaces = new char[] { ' ', '\t', '\r', '\n' };

        public FtpEntry(string name)
        {
            this.Name = name;
        }

        public static bool TryParse(string listDirectoryDetailsLine, out FtpEntry fileInfo)
        {
            fileInfo = null;

            if (string.IsNullOrEmpty(listDirectoryDetailsLine))
            {
                return false;
            }

            if (listDirectoryDetailsLine.Length < 54)
            {
                return false;
            }

            string name = listDirectoryDetailsLine.Substring(54).Trim();
            if (string.IsNullOrEmpty(name))
            {
                return false;
            }

            if (listDirectoryDetailsLine[0] == 'd')
            {
                fileInfo = new FtpFolder(listDirectoryDetailsLine.Substring(54).Trim());
                return true;
            }
            else
            {
                string sizeString = listDirectoryDetailsLine.Substring(28, 14).Trim();
                long size;
                if (!TryParseSize(sizeString, out size))
                {
                    return false;
                }

                string dateString = listDirectoryDetailsLine.Substring(42, 12).Trim();
                DateTime modified;
                if (!TryParseDate(dateString, out modified))
                {
                    return false;
                }

                fileInfo = new FtpFile(listDirectoryDetailsLine.Substring(54).Trim()) { Size = size, LastModified = modified };
                return true;
            }
        }

        private static bool TryParseSize(string input, out long size)
        {
            return long.TryParse(input, out size);
        }

        private static bool TryParseDate(string input, out DateTime date)
        {
            date = DateTime.MinValue;

            if (string.IsNullOrEmpty(input))
            {
                return false;
            }

            string[] parts = input.Split(Whitespaces, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 3)
            {
                return false;
            }

            Months month;
            if (!Enum.TryParse(parts[0], true, out month))
            {
                return false;
            }

            int hour = 0;
            int minute = 0;

            int year;
            int splitter = parts[2].IndexOf(':');
            if (splitter >= 0)
            {
                if (!int.TryParse(parts[2].Substring(0, splitter), out hour))
                {
                    return false;
                }

                if (!int.TryParse(parts[2].Substring(splitter + 1, parts[2].Length - splitter - 1), out minute))
                {
                    return false;
                }

                year = DateTime.Today.Year;
            }
            else if (!int.TryParse(parts[2], out year))
            {
                return false;
            }

            int day;
            if (!int.TryParse(parts[1], out day))
            {
                return false;
            }

            date = new DateTime(year, (int)month, day, hour, minute, 0);
            return true;
        }

        public string Name
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        private enum Months : int
        {
            Jan = 1,
            Feb = 2,
            Mar = 3,
            Apr = 4,
            May = 5,
            Jun = 6,
            Jul = 7,
            Aug = 8,
            Sep = 9,
            Oct = 10,
            Nov = 11,
            Dec = 12
        }
    }

    class FtpFile : FtpEntry
    {
        public FtpFile(string name)
            : base(name)
        {
        }

        public long Size
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }
    }

    class FtpFolder : FtpEntry
    {
        public FtpFolder(string name)
            : base(name)
        {
        }
    }
}
