﻿namespace CommandLineBrowser
{
    public static class HashFunction
    {
        public static int GetHashCode(params object[] fields)
        {
            int code1 = 0x1505;
            int code2 = code1;
            foreach (object field in fields)
            {
                if (field != null)
                {
                    code1 = ((code1 << 5) + code1) ^ field.GetHashCode();
                    code2 = ((code2 << 5) + code2) ^ field.GetHashCode();
                }
                else
                {
                    code1 = ((code1 << 5) + code1);
                    code2 = ((code2 << 5) + code2);
                }
            }
            return code1 + code2 * 0x5d588b65;
        }
    }
}
