﻿namespace Fidelity
{
    using System;

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class TitleAttribute : Attribute
    {
        public TitleAttribute(string Title)
        {
            this.Title = Title;
        }

        public string Title { get; set; }
    }
}
