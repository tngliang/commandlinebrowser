﻿namespace Fidelity
{
    using System;

    public class FidelityException : Exception
    {
        public FidelityException(string message)
            : base(message)
        {
        }

        public FidelityException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
