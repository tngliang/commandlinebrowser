﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Option Summary: Fidelity Investments")]
    public class OptionSummary : Page
    {
        public OptionSummary(Uri backUri, string content)
            : base(backUri, content)
        {
        }
    }
}
