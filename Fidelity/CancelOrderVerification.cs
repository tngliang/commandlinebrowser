﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;
    [Title("Cancel Order Verification: Fidelity Investments")]
    public class CancelOrderVerification : Page
    {
        public CancelOrderVerification(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public Link AttemptToCancelOrder()
        {
            foreach (TextNode formNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<form", "</form>"))
            {
                TextNode verifyFormNode = new TextNode(formNode, "name=\"", "\"");
                if (!verifyFormNode.IsValid)
                {
                    continue;
                }

                TextNode actionNode = new TextNode(formNode, "action=\"", "\"");
                if (!actionNode.IsValid)
                {
                    throw new FidelityException("Attempt to cancel form is missing action URL");
                }

                Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                foreach (TextNode inputNode in formNode.ParseChildren("<input", ">"))
                {
                    TextNode nameNode = new TextNode(inputNode, "name=\"", "\"");
                    if (!nameNode.IsValid)
                    {
                        nameNode = new TextNode(inputNode, "name='", "'");
                    }
                    if (nameNode.IsValid)
                    {
                        TextNode valueNode = new TextNode(inputNode, "value=\"", "\"");
                        if (!valueNode.IsValid)
                        {
                            valueNode = new TextNode(inputNode, "value='", "'");
                        }

                        if (valueNode.IsValid)
                        {
                            parameters[nameNode.Value] = valueNode.Value;
                        }
                    }
                }

                return Link.Create(actionNode.Value, this.backUri, null, true, parameters);
            }

            throw new FidelityException("Missing attempt to cancel confirm form");
        }
    }
}
