﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using CommandLineBrowser;

    [Title("Option Chain: Fidelity Investments")]
    public class OptionChain : Page
    {
        private HashSet<StockOption> options;

        public OptionChain(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public static OptionChain Create(Session session)
        {
            string optionChainUrl = "https://researchtools.fidelity.com/ftgw/mloptions/goto/optionPairingResult";
            Uri backUri = new Uri("https://oltx.fidelity.com/ftgw/fbc/ofsummary/summary");
            OptionChain page = (OptionChain)session.Navigate(Link.Create(optionChainUrl, backUri));
            foreach (TextNode aNode in new TextNode(page.content, string.Empty, string.Empty).ParseChildren("<a", "</a>"))
            {
                TextNode chainPeakNode = new TextNode(aNode, "id=\"chainPeak\"", string.Empty);
                if (chainPeakNode.IsValid)
                {
                    string loginUrl = "https://login.fidelity.com/ftgw/Fas/Fidelity/RtlCust/Login/Init?AuthRedUrl=https%3A%2F%2Fresearchtools.fidelity.com%2Fftgw%2Fmloptions%2Fgoto%2FoptionPairingResult";
                    LogIntoFidelity login = (LogIntoFidelity)session.Navigate(Link.Create(loginUrl, backUri), false);
                    Link loginLink = login.FindLogin(session.UserName, session.Password);
                    return (OptionChain)session.Navigate(loginLink);
                }
            }

            return page;
        }

        public Link QuickSearch(string symbol, bool showAnalytics)
        {
            string searchUrl = string.Format("/ftgw/mloptions/goto/optionChain?symbol={0}&Search=Search&symbols={0}&showsanalytics={1}", symbol, showAnalytics ? "Y" : "N");
            return Link.Create(searchUrl, this.backUri);
        }

        public Link Search(string symbol, bool showAnalytics)
        {
            string searchUrl = "/ftgw/mloptions/goto/optionChain?roc=analytics";
            Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            parameters["FILTER_DAT"] = "5";
            parameters["FILTER_VOL"] = "Y";
            parameters["VOL_FILTER_TYPE"] = "Show All Options";
            parameters["analyticsType"] = "undefined";
            parameters["calls"] = "Y";
            parameters["dateSelected"] = string.Empty;
            parameters["dateMax"] = string.Empty;
            parameters["dateMin"] = string.Empty;
            parameters["exp1"] = string.Empty;
            parameters["exp2"] = string.Empty;
            parameters["moneyFilter"] = "All";
            parameters["priceMax"] = string.Empty;
            parameters["priceMin"] = string.Empty;
            parameters["puts"] = "Y";
            parameters["range"] = string.Empty;
            parameters["showsanalytics"] = showAnalytics ? "Y" : "N";
            parameters["showsanalytics2"] = showAnalytics ? "Y" : "N";
            parameters["showsymbols"] = "Y";
            parameters["showsymbols2"] = "Y";
            parameters["sortBy"] = "STRIKE_PRICE";
            parameters["sortDir"] = "A";
            parameters["sortTable"] = "P";
            parameters["strategy"] = "CallsPuts";
            parameters["symbols"] = symbol;
            parameters["volumeMax"] = string.Empty;
            parameters["showadjusted"] = "Y";
            parameters["chaintype"] = "std";
            return Link.Create(searchUrl, this.backUri, "searchForm", true, parameters);
        }

        public HashSet<StockOption> Options
        {
            get
            {
                if (this.options == null)
                {
                    TextNode rootNode = new TextNode(this.content, string.Empty, string.Empty);
                    if (!rootNode.IsValid)
                    {
                        throw new Exception(string.Format("Invalid option chain page content '{0}'", this.content));
                    }

                    this.options = ParseOptions(rootNode, ParseUnderlying(rootNode));
                }

                return this.options;
            }
        }

        private HashSet<StockOption> ParseOptions(TextNode rootNode, Stock underlying)
        {
            HashSet<StockOption> options = new HashSet<StockOption>();
            foreach (TextNode tableNode in rootNode.ParseChildren("<table", "</table>"))
            {
                TextNode idNode = new TextNode(tableNode, "id=\"fullTable\"", string.Empty);
                if (!idNode.IsValid)
                {
                    continue;
                }

                TextNode tbodyNode = new TextNode(tableNode, "<tbody", "</tbody>");
                if (!tbodyNode.IsValid)
                {
                    continue;
                }

                tbodyNode = new TextNode(tbodyNode, ">", string.Empty);
                if (!tbodyNode.IsValid)
                {
                    continue;
                }

                foreach (TextNode rowNode in tbodyNode.ParseChildren("<tr", "</tr>"))
                {
                    TextNode rowIdNode = new TextNode(rowNode, "id=\"", "\"");
                    if (!rowIdNode.IsValid || !rowIdNode.Value.StartsWith("N_", StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }
                    
                    TextNode rowBodyNode = new TextNode(rowNode, ">", string.Empty);
                    if (!rowBodyNode.IsValid)
                    {
                        continue;
                    }

                    TextNode cellNode = rowNode.ParseNextChild(rowIdNode, "<td", "</td>");
                    if (!cellNode.IsValid)
                    {
                        continue;
                    }

                    cellNode = new TextNode(cellNode, ">", string.Empty);
                    if (!cellNode.IsValid)
                    {
                        continue;
                    }

                    TextNode symbolNode = new TextNode(cellNode, "-", " ");
                    if (!symbolNode.IsValid)
                    {
                        continue;
                    }

                    string symbol = symbolNode.Value.Trim();
                    Regex regex = new Regex(@"([a-z|A-Z]*)([0-9][0-9])([0-9][0-9])([0-9][0-9])([C|c|P|p])([0-9|\.]*)");
                    Match symbolMatch = regex.Match(symbol);
                    if (!symbolMatch.Success)
                    {
                        continue;
                    }

                    cellNode = ParseOption(symbol, underlying, options, rowNode, cellNode, symbolMatch);
                    
                    cellNode = SkipActionStrikeAction(rowNode, cellNode);

                    symbolNode = new TextNode(cellNode, "-", " ");
                    if (!symbolNode.IsValid)
                    {
                        continue;
                    }

                    symbol = symbolNode.Value.Trim();
                    regex = new Regex(@"([a-z|A-Z]*)([0-9][0-9])([0-9][0-9])([0-9][0-9])([C|c|P|p])([0-9|\.]*)");
                    symbolMatch = regex.Match(symbol);
                    if (!symbolMatch.Success)
                    {
                        continue;
                    }

                    cellNode = ParseOption(symbol, underlying, options, rowNode, cellNode, symbolMatch);
                }
            }

            return options;
        }

        private static TextNode ParseOption(string symbol, Stock underlying, HashSet<StockOption> options, TextNode rowNode, TextNode cellNode, Match symbolMatch)
        {
            OptionType optionType = string.Compare(symbolMatch.Groups[5].Value, "c", StringComparison.OrdinalIgnoreCase) == 0 ?
                OptionType.Call :
                OptionType.Put;

            int year = int.Parse(symbolMatch.Groups[2].Value);
            if (year > (DateTime.Today.Year + 5) % 100)
            {
                year += 1900;
            }
            else
            {
                year += 2000;
            }

            DateTime expiration = new DateTime(
                year,
                int.Parse(symbolMatch.Groups[3].Value),
                int.Parse(symbolMatch.Groups[4].Value),
                18, 0, 0);

            double strike = double.Parse(symbolMatch.Groups[6].Value);
            StockOption option = new StockOption(underlying, optionType, strike, expiration)
            {
                Symbol = symbol
            };
            options.Add(option);
            
            // Last
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Last = cellNode.Value.Trim().AsPrice();
                }
            }

            // Change
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Change = cellNode.Value.Trim().AsPercentage();
                }
            }

            // Bid
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Bid = cellNode.Value.Trim().AsPrice();
                }
            }

            // Ask
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Ask = cellNode.Value.Trim().AsPrice();
                }
            }

            // Volume
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Volume = (int)cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Open Interest
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.OpenInterest = (int)cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Implied Volatility
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.ImpliedVolatility = cellNode.Value.Trim().AsPercentage();
                }
            }

            // Delta
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Delta = cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Gamma
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Gamma = cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Theta
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Theta = cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Vega
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Vega = cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }

            // Rho
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                    option.Rho = cellNode.Value.Trim().As<double>(double.TryParse);
                }
            }
            return cellNode;
        }

        private static TextNode SkipActionStrikeAction(TextNode rowNode, TextNode cellNode)
        {
            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                }
            }

            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                }
            }

            cellNode = rowNode.ParseNextChild(cellNode, "<td", "</td>");
            if (cellNode.IsValid)
            {
                cellNode = new TextNode(cellNode, ">", string.Empty);
                if (cellNode.IsValid)
                {
                }
            }

            return cellNode;
        }

        private static Stock ParseUnderlying(TextNode rootNode)
        {
            TextNode companyMainNode = new TextNode(rootNode, "company-main", "company-name");
            if (!companyMainNode.IsValid)
            {
                throw new FidelityException("Underlying not found on Option Chain page");
            }
            TextNode symbolNode = new TextNode(companyMainNode, ">", string.Empty);
            if (!symbolNode.IsValid)
            {
                throw new FidelityException("Underlying not found on Option Chain page");
            }

            Stock underlying = new Stock(symbolNode.Value.Trim());
            TextNode nameNode = rootNode.ParseNextChild(companyMainNode, ">", "<");
            if (nameNode.IsValid)
            {
                underlying.Name = nameNode.Value.Trim();
            }

            foreach (TextNode divNode in rootNode.ParseChildren("<div", "</div>"))
            {
                TextNode classNode = new TextNode(divNode, "class=\"", "\"");
                if (classNode.IsValid)
                {
                    switch (classNode.Value.Trim())
                    {
                        case "company-details":
                            foreach (TextNode tableNode in divNode.ParseChildren("<table", "</table>"))
                            {
                                ParseStockQuotes(underlying, tableNode);
                            }
                            break;
                        case "time-stamp":
                            ParseTimeStamp(underlying, divNode);
                            break;
                    }
                }
            }

            foreach (TextNode spanNode in rootNode.ParseChildren("<span", "</span>"))
            {
                TextNode classNode = new TextNode(spanNode, "class=\"", "\"");
                if (classNode.IsValid && string.Compare(classNode.Value, "time-stamp", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    ParseTimeStamp(underlying, spanNode);
                }
            }
            return underlying;
        }

        private static void ParseTimeStamp(Stock underlying, TextNode node)
        {
            TextNode asOfNode = new TextNode(node, "AS OF", string.Empty);
            if (asOfNode.IsValid)
            {
                Regex regex = new Regex(@"\s*([0-9]*)\s*([0-9]*)\s*([0-9]*)\s*([A|a|P|p][M|m])\s*ET\s*([0-9]*)\s*([0-9]*)\s*([0-9]*)\s*");
                Match match = regex.Match(asOfNode.Value.Trim());
                if (match.Success)
                {
                    underlying.Date = new DateTime(
                        int.Parse(match.Groups[7].Value),
                        int.Parse(match.Groups[5].Value),
                        int.Parse(match.Groups[6].Value),
                        int.Parse(match.Groups[1].Value) + (string.Compare(match.Groups[4].Value, "PM", StringComparison.OrdinalIgnoreCase) == 0 ? 12 : 0),
                        int.Parse(match.Groups[2].Value),
                        int.Parse(match.Groups[3].Value));
                }
            }
        }

        private static Stock ParseStockNameAndSymbol(TextNode node)
        {
            TextNode symbolNode = new TextNode(node, ">", string.Empty);
            if (!symbolNode.IsValid)
            {
                throw new FidelityException("Symbol not found");
            }

            TextNode h2Node = new TextNode(symbolNode, "<h2>", "</h2>");
            if (h2Node.IsValid)
            {
                symbolNode = h2Node;
            }

            Stock underlying = new Stock(symbolNode.Value);
            TextNode spanNode = new TextNode(node, "<span", "</span>");
            if (spanNode.IsValid)
            {
                node = spanNode;
            }

            TextNode classNode = new TextNode(node, "class=\"", "\"");
            if (classNode.IsValid && string.Compare(classNode.Value, "company-name", StringComparison.OrdinalIgnoreCase) == 0)
            {
                TextNode nameNode = new TextNode(spanNode, ">", string.Empty);
                if (nameNode.IsValid)
                {
                    underlying.Name = nameNode.Value.Trim();
                }
                else
                {
                    underlying.Name = Uri.UnescapeDataString(classNode.Value.Trim());
                }
            }

            return underlying;
        }

        private static void ParseStockQuotes(Stock underlying, TextNode tableNode)
        {
            foreach (TextNode rowNode in tableNode.ParseChildren("<tr", "</tr>"))
            {
                TextNode nameValueNode = new TextNode(rowNode, ">", string.Empty);
                if (!nameValueNode.IsValid)
                {
                    continue;
                }
                
                string nameValue = nameValueNode.Value.Trim();
                
                double bid;
                TextNode bidNode = new TextNode(nameValue, "Bid", "X");
                if (bidNode.IsValid && bidNode.Value.Trim().TryParsePrice(out bid))
                {
                    underlying.Bid = bid;
                }
                double ask;
                TextNode askNode = new TextNode(nameValue, "Ask", "X");
                if (askNode.IsValid && askNode.Value.Trim().TryParsePrice(out ask))
                {
                    underlying.Ask = ask;
                }

                TextNode cellNode = new TextNode(rowNode, "<td", "</td>");
                if (cellNode.IsValid)
                {
                    cellNode = new TextNode(cellNode, ">", string.Empty);
                }

                double iv30;
                if (nameValue.StartsWith("IV30", StringComparison.OrdinalIgnoreCase) &&
                    cellNode.IsValid && cellNode.Value.Trim().TryParse<double>(double.TryParse, out iv30))
                {
                    underlying.IV30 = iv30;
                }
                double iv60;
                if (nameValue.StartsWith("IV60", StringComparison.OrdinalIgnoreCase) &&
                    cellNode.IsValid && cellNode.Value.Trim().TryParse<double>(double.TryParse, out iv60))
                {
                    underlying.IV60 = iv60;
                }
                double iv90;
                if (nameValue.StartsWith("IV90", StringComparison.OrdinalIgnoreCase) &&
                    cellNode.IsValid && cellNode.Value.Trim().TryParse<double>(double.TryParse, out iv90))
                {
                    underlying.IV90 = iv90;
                }
                double pe;
                if (nameValue.StartsWith("P/E", StringComparison.OrdinalIgnoreCase) && 
                    cellNode.IsValid && cellNode.Value.Trim().TryParse<double>(double.TryParse, out pe))
                {
                    underlying.PriceToEarnings = pe;
                }
                TextNode sectorNode = new TextNode(nameValue, "Sector", string.Empty);
                if (sectorNode.IsValid)
                {
                    underlying.Sector = sectorNode.Value.Trim();
                }
                TextNode industryNode = new TextNode(nameValue, "Industry", string.Empty);
                if (industryNode.IsValid)
                {
                    underlying.Industry = industryNode.Value.Trim();
                }
            }
        }
    }
}
