﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using CommandLineBrowser;

    [Title("Balances: Fidelity Investments")]
    public class AccountBalance : Page
    {
        public AccountBalance(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public double TotalAccountValue
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Total\s+Account\s+Value");
            }
        }

        public double CashCore
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+(\(core\))?");
            }
        }

        public double CashCreditDebit
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+Credit(\s*\/?\s*)?(Debit)?");
            }
        }

        public double MarginCreditDebit
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Margin\s+Credit(\s*\/?\s*)?(Debit)?");
            }
        }

        public double ShortCredit
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Short\sCredit");
            }
        }

        public double HeldInCash
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Held\s+in\s+Cash");
            }
        }

        public double HeldInMargin
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Held\s+in\s+Margin");
            }
        }

        public double HeldShort
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Held\s+Short");
            }
        }

        public double HeldInOptions
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Held\s+in\s+Options");
            }
        }

        public double MarginBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Margin\s+Buying\s+Power");
            }
        }

        public double NonMarginBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Non\-Margin\s+Buying\s+Power");
            }
        }

        public double CommittedToOpenOrders
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Committed\s+to\s+Open\s+Orders");
            }
        }

        public double AvailableToWithdrawCashOnly
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+(Only)?");
            }
        }

        public double AvailableToWithdrawCashAndBorrowingOnMargin
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+\&\s+Borrowing\s+on\s+Margin");
            }
        }

        public double MarginEquity
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Margin\s+Equity");
            }
        }

        public double MarginEquityPercentage
        {
            get
            {
                return GetPercentage(" name=\"brokerageBalancesForm\"", @"Margin\s+Equity\s+Percentage");
            }
        }

        public double HouseSurplus
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"House\s+Surplus");
            }
        }

        public double ExchangeSurplus
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Exchange\s+Surplus");
            }
        }

        public double SMA
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"SMA");
            }
        }
        
        public double CashBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+Buying\s+Power");
            }
        }

        public double SettledCash
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Settled\s+Cash");
            }
        }

        public double CorporateBondsBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Corporate\s+Bonds");
            }
        }

        public double MunicipalBondsBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Municipal\s+Bonds");
            }
        }

        public double GovernmentBondsBuyingPower
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Government\s+Bonds");
            }
        }

        public double OptionsInTheMoney
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Options\s+in\s+the\s+Money");
            }
        }

        public double CashCoveredPutReserve
        {
            get
            {
                return GetPrice(" name=\"brokerageBalancesForm\"", @"Cash\s+Covered\s+Put\s+Reserve");
            }
        }

        private double GetPrice(string formTagName, string rowDescPattern)
        {
            return GetValue(formTagName, rowDescPattern, StringExtension.AsPrice);
        }

        private double GetPercentage(string formTagName, string rowDescPattern)
        {
            return GetValue(formTagName, rowDescPattern, StringExtension.AsPercentage);
        }

        private double GetValue(string formTagName, string rowDescPattern, Func<string, double> converter)
        {
            TextNode formNode = new TextNode(this.content, "<form" + formTagName, "</form>");
            if (!formNode.IsValid)
            {
                throw new FidelityException(string.Format("\"<form {0}\" not found", formTagName));
            }

            Regex rowDescRegex = new Regex(rowDescPattern);
            foreach (TextNode rowNode in formNode.ParseChildren("<tr", "</tr>"))
            {
                foreach (TextNode aNode in rowNode.ParseChildren("<a", "</a>"))
                {
                    if (rowDescRegex.Match(aNode.Value).Success)
                    {
                        return GetFirstValueAfterNode(rowNode, aNode, converter);
                    }
                }

                foreach (TextNode divNode in rowNode.ParseChildren("<div", "</div>"))
                {
                    if (rowDescRegex.Match(divNode.Value).Success)
                    {
                        return GetFirstValueAfterNode(rowNode, divNode, converter);
                    }
                }
            }

            return double.NaN;
        }

        private double GetFirstValueAfterNode(TextNode rowNode, TextNode node, Func<string, double> converter)
        {
            TextNode cellNode = rowNode.ParseNextChild(node, "<td", "</td>");
            if (!cellNode.IsValid)
            {
                throw new FidelityException("\"<td </td>\" not found");
            }

            TextNode cellValueNode = new TextNode(cellNode, ">", "<");
            if (!cellValueNode.IsValid)
            {
                throw new FidelityException("Value not found");
            }

            return converter(cellValueNode.Value);
        }
    }
}
