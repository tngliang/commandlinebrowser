﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Fidelity")]
    public class Fidelity : Page
    {
        private Dictionary<string, Link> links;
        private Dictionary<string, object> quotes;

        public Fidelity(Uri backUri, string content)
            : base(backUri, content)
        {
            this.links = null;
        }

        public Dictionary<string, Link> Links
        {
            get
            {
                if (this.links == null)
                {
                    Dictionary<string, Link> links = new Dictionary<string, Link>(StringComparer.OrdinalIgnoreCase);
                    foreach (TextNode aNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<a", "</a>"))
                    {
                        TextNode hrefNode = new TextNode(aNode, "href=\"", "\"");
                        TextNode aNameNode = new TextNode(aNode, ">", string.Empty);
                        if (hrefNode.IsValid && aNameNode.IsValid)
                        {
                            string name = string.Join(" ", aNameNode.Value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, ' ', '\t', '\r', '\n'));
                            string url = hrefNode.Value.Trim();
                            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(url) && !url.StartsWith("#") &&
                                Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute))
                            {
                                links[name] = Link.Create(url, this.backUri, name);
                            }
                        }
                    }
                    this.links = links;
                }

                return this.links;
            }
        }

        public Dictionary<string, object> Quotes
        {
            get
            {
                if (this.quotes == null)
                {
                    Dictionary<string, object> quotes = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                    foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
                    {
                        DateTime quoteDate;
                        TextNode asOfNode = new TextNode(tableNode, "As of:", "<");
                        if (asOfNode.IsValid && asOfNode.Value.Trim().TryParse(DateTime.TryParse, out quoteDate))
                        {
                            quotes["Date"] = quoteDate;
                        }

                        string key = null;
                        foreach (TextNode cellNode in tableNode.ParseChildren("<td", "</td>"))
                        {
                            TextNode classNode = new TextNode(cellNode, "class=\"smallData", ">");
                            TextNode alignNode = new TextNode(cellNode, "align=", ">");
                            TextNode valueNode = new TextNode(cellNode, ">", string.Empty);
                            if (classNode.IsValid && alignNode.IsValid && valueNode.IsValid)
                            {
                                string align = alignNode.Value.Unquote();
                                if (string.Compare(align, "left", StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    key = valueNode.Value.Trim();
                                }
                                else if (string.Compare(align, "right", StringComparison.OrdinalIgnoreCase) == 0)
                                {
                                    string valueString = valueNode.Value.Trim();
                                    DateTime date;
                                    double value;
                                    if (string.IsNullOrEmpty(valueString))
                                    {
                                        quotes[key] = string.Empty;
                                    }
                                    else if (valueString.TryParsePrice(out value))
                                    {
                                        quotes[key] = value;
                                    }
                                    else if (valueString.TryParsePercentage(out value))
                                    {
                                        quotes[key] = value;
                                    }
                                    else if (valueString.TryParse<double>(double.TryParse, out value))
                                    {
                                        quotes[key] = value;
                                    }
                                    else if (valueString.TryParse<DateTime>(DateTime.TryParse, out date))
                                    {
                                        quotes[key] = date;
                                    }
                                    else
                                    {
                                        quotes[key] = valueString;
                                    }
                                }
                            }
                        }
                    }
                    this.quotes = quotes;
                }

                return this.quotes;
            }
        }
    }
}
