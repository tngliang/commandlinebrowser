﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    
    using CommandLineBrowser;

    [Title("Cancel Order Confirmation: Fidelity Investments")]
    public class CancelOrderConfirmation : Page
    {
        public CancelOrderConfirmation(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public string CancellationOrderNumber
        {
            get
            {
                TextNode cancellationOrderNumber = new TextNode(this.content, "Cancellation Order Number:", "<p>");
                if (cancellationOrderNumber.IsValid)
                {
                    return cancellationOrderNumber.Value.Trim();
                }

                throw new FidelityException("Cancellation order number not found.");
            }
        }
    }
}
