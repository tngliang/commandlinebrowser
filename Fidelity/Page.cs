﻿namespace Fidelity
{
    using System;
    using System.Reflection;

    public class Page
    {
        protected Uri backUri;
        protected string content;
        public Page(Uri backUri, string content)
        {
            this.backUri = backUri;
            this.content = content;
        }

        public static Page Parse(string title, Uri backUri, string content)
        {
            foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
            {
                if (type.IsSubclassOf(typeof(Page)))
                {
                    foreach (TitleAttribute attribute in type.GetCustomAttributes(typeof(TitleAttribute), true))
                    {
                        if (attribute != null && string.Compare(attribute.Title, title, StringComparison.OrdinalIgnoreCase) == 0)
                        {
                            return (Page)type.GetConstructor(new Type[] { typeof(Uri), typeof(string) }).Invoke(new object[] { backUri, content });
                        }
                    }
                }
            }

            throw new FidelityException(string.Format("No handler found for page \"{0}\"", title));
        }
    }
}
