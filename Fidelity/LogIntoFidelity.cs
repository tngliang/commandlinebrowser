﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Log In to Fidelity.com")]
    public class LogIntoFidelity : Page
    {
        public LogIntoFidelity(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public Link FindLogin(string userName, string password)
        {
            foreach (TextNode loginFormNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<form", "</form>"))
            {
                TextNode formIdNode = new TextNode(loginFormNode, "id=\"", "\"");
                if (formIdNode.IsValid && string.Compare(formIdNode.Value, "Login", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    TextNode loginActionNode = new TextNode(loginFormNode, "action=\"", "\"");
                    if (loginActionNode.IsValid)
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                        parameters["DEVICE_PRINT"] = "version%3D1%26amp%3Bpm%5Ffpua%3Dmozilla%2F5%2E0%20%28windows%20nt%206%2E1%3B%20wow64%29%20applewebkit/537%2E31%20%28khtml%2C%20like%20gecko%29%20chrome/26%2E0%2E1410%2E64%20safari/537%2E31%7C5%2E0%20%28Windows%20NT%206%2E1%3B%20WOW64%29%20AppleWebKit/537%2E31%20%28KHTML%2C%20like%20Gecko%29%20Chrome/26%2E0%2E1410%2E64%20Safari/537%2E31%7CWin32%26amp%3Bpm%5Ffpsc%3D32%7C1280%7C800%7C760%26amp%3Bpm%5Ffpsw%3D%7Cpdf%7Cqt1%7Cqt2%7Cqt3%7Cqt4%7Cqt5%7Cqt6%26amp%3Bpm%5Ffptz%3D%2D7%26amp%3Bpm%5Ffpln%3Dlang%3Den%2DUS%7Csyslang%3D%7Cuserlang%3D%26amp%3Bpm%5Ffpjv%3D1%26amp%3Bpm%5Ffpco%3D1";
                        parameters["notSet-visible"] = "";
                        parameters["notSet"] = "";
                        parameters["SSN"] = userName;
                        parameters["SavedIdInd"] = "Y";
                        parameters["confirm"] = "on";
                        parameters["PIN"] = password;
                        return Link.Create(loginActionNode.Value, this.backUri, formIdNode.Value, true, parameters);
                    }
                }
            }

            throw new FidelityException("Login form not found.");
        }
    }
}
