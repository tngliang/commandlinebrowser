﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    
    using CommandLineBrowser;
    
    public class Account
    {
        private static readonly char[] Digits = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private Dictionary<string, Link> Links = new Dictionary<string, Link>(StringComparer.OrdinalIgnoreCase);
 
        public string Name { get; set; }
        public string Number { get; set; }
        public string Type { get; set; }

        public Link this[string linkName]
        {
            get
            {
                return this.Links[linkName];
            }
            set
            {
                this.Links[linkName] = value;
            }
        }

        public void AddLink(Link link)
        {
            string key = link.Name;
            if (this.Links.ContainsKey(key))
            {
                int index = key.LastIndexOfAny(Digits);
                if (index >= 0)
                {
                    key = string.Format("{0}{1}", key.Substring(0, index), int.Parse(key.Substring(index)) + 1);
                }
                else
                {
                    key = string.Format("{0}2", key);
                }
            }
            this.Links[key] = link;
        }
    }
}
