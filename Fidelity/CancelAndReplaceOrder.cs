﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    [Title("Cancel and Replace Order: Fidelity Investments")]
    public class CancelAndReplaceOrder : Page
    {
        public CancelAndReplaceOrder(Uri backUri, string content)
            : base(backUri, content)
        {
        }
    }
}
