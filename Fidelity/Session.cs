﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public enum Key
    {
        UserName = 0,
        Password = 1,
        Taxable = 2,
        IRA = 3,
        ROTH = 4,
        TaxableAcctNum = 5,
        IRAAcctNum = 6,
        ROTHAcctNum = 7
    }

    public class Session
    {
        public const string DefaultUrl = "https://oltx.fidelity.com/ftgw/fbc/ofsummary/summary";

        public string UserName { get; set; }
        public string Password { get; set; }
        private HttpSession session;

        public static string RegRead(Key key)
        {
            return (string)Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\CommandLineBrowser").GetValue(key.ToString(), string.Empty);
        }

        public Session(string userName, string password, HttpSession session)
        {
            this.UserName = string.IsNullOrEmpty(userName) ? RegRead(Key.UserName) : userName;
            this.Password = string.IsNullOrEmpty(password) ? RegRead(Key.Password) : password;
            this.session = session;
        }

        public Page Login()
        {
            return this.Navigate(Link.Create(DefaultUrl));
        }

        public Page Navigate(Link link, bool checkLogin = true)
        {
            HttpRequest request = new HttpRequest(this.session, link.Referer);
            string response = request.ReadToEnd(link.Value, link.Post, link.Parameters);
            
            TextNode titleNode;
            if (checkLogin)
            {
                titleNode = new TextNode(response, "<title>", "</title>");
                if (!titleNode.IsValid ||
                    titleNode.Value.IndexOf("Login", StringComparison.OrdinalIgnoreCase) >= 0 ||
                    titleNode.Value.IndexOf("Log In", StringComparison.OrdinalIgnoreCase) >= 0 ||
                    titleNode.Value.IndexOf("Timeout", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    response = ExecuteLogin(request);
                    titleNode = new TextNode(response, "<title>", "</title>");
                    if (!titleNode.IsValid || titleNode.Value.IndexOf("Login", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        return null;
                    }
                    if (Uri.Compare(link.Value, session.Back, UriComponents.AbsoluteUri, UriFormat.SafeUnescaped, StringComparison.OrdinalIgnoreCase) != 0)
                    {
                        response = request.ReadToEnd(link.Value, link.Post, link.Parameters);
                    }
                }
            }
            else
            {
                titleNode = new TextNode(response, "<title>", "</title>");
            }
            
            if (titleNode.IsValid)
            {
                return Page.Parse(titleNode.Value, session.Back, response);
            }
            else
            {
                return null;
            }
        }

        private string ExecuteLogin(HttpRequest request)
        {
            // Start fidelity.com main page.
            this.session.Clear();

            string response = request.ReadToEnd(new Uri("http://www.fidelity.com"));
            TextNode htmlNode = new TextNode(response, "<HTML", "</HTML>");
            if (!htmlNode.IsValid)
            {
                throw new FidelityException("Home page not found");
            }

            string loginUrl = null;
            foreach (TaggedNode aLink in TaggedNode.ParseChildren(htmlNode, "a", "href"))
            {
                loginUrl = aLink.Attribute("href");
                if (!string.IsNullOrEmpty(loginUrl) && loginUrl.IndexOf("login.fidelity.com", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    break;
                }
            }
            
            if (string.IsNullOrEmpty(loginUrl))
            {
                throw new FidelityException("Login url not found");
            }

            response = request.ReadToEnd(new Uri(loginUrl));
            TextNode loginFormNode = new TextNode(response, "<form id=\"Login\"", "</form>");
            if (!loginFormNode.IsValid)
            {
                throw new FidelityException("Login form not found");
            }

            TextNode loginActionNode = new TextNode(loginFormNode, "action=\"", "\"");
            if (loginActionNode.IsValid)
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                parameters["DEVICE_PRINT"] = "version%3D1%26amp%3Bpm%5Ffpua%3Dmozilla%2F5%2E0%20%28windows%20nt%206%2E1%3B%20wow64%29%20applewebkit/537%2E31%20%28khtml%2C%20like%20gecko%29%20chrome/26%2E0%2E1410%2E64%20safari/537%2E31%7C5%2E0%20%28Windows%20NT%206%2E1%3B%20WOW64%29%20AppleWebKit/537%2E31%20%28KHTML%2C%20like%20Gecko%29%20Chrome/26%2E0%2E1410%2E64%20Safari/537%2E31%7CWin32%26amp%3Bpm%5Ffpsc%3D32%7C1280%7C800%7C760%26amp%3Bpm%5Ffpsw%3D%7Cpdf%7Cqt1%7Cqt2%7Cqt3%7Cqt4%7Cqt5%7Cqt6%26amp%3Bpm%5Ffptz%3D%2D7%26amp%3Bpm%5Ffpln%3Dlang%3Den%2DUS%7Csyslang%3D%7Cuserlang%3D%26amp%3Bpm%5Ffpjv%3D1%26amp%3Bpm%5Ffpco%3D1";
                parameters["notSet-visible"] = "";
                parameters["notSet"] = "";
                parameters["SSN"] = this.UserName;
                parameters["SavedIdInd"] = "Y";
                parameters["confirm"] = "on";
                parameters["PIN"] = this.Password;
                Uri baseLoginUri = new Uri("https://login.fidelity.com");
                response = request.ReadToEnd(new Uri(baseLoginUri, loginActionNode.Value), true, parameters);
            }

            return response;
        }


    }
}
