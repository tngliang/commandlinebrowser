﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Get Quote for Symbol: Fidelity Investments")]
    public class GetQuoteForSymbol : Page
    {
        public GetQuoteForSymbol(Uri backUri, string content)
            : base(backUri, content)
        {
        }
    }
}
