﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Trade Stocks/ETFs - Standard Session: Fidelity Investments")]
    public class TradeStockETFs : Page
    {
        public TradeStockETFs(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public Link Trade(
            TradeAction tradeAction,
            string symbol,
            int quantity,
            OrderType orderType = OrderType.Market,
            bool marginTrade = false,
            double limit = double.NaN,
            double stop = double.NaN,
            TimeInForce timeInForce = TimeInForce.Day,
            SecurityType securityType = SecurityType.StockETF,
            TrailingValueType trailingValueType = TrailingValueType.Last,
            GTCExpTime gtcExpTime = GTCExpTime.FourPMET,
            GTCAlertDate gtcAlertDate = GTCAlertDate.EXP_ALERT_0,
            DateTime? gtcExpDate = null,
            bool allOrNone = false,
            bool skipOrderPreview = false,
            bool specificShares = false,
            bool keepInAccount = true,
            string settleCurrency = null)
        {
            if (string.IsNullOrEmpty(symbol))
            {
                throw new FidelityException("Symbol cannot be null");
            }

            if (quantity == 0)
            {
                throw new FidelityException("Quantity cannot be null");
            }

            TextNode formNode = new TextNode(this.content, "<form name=\"entryForm\"", "</form>");
            if (!formNode.IsValid)
            {
                throw new FidelityException("Unable to find entryForm");
            }

            TextNode actionNode = new TextNode(formNode, "action=\"", "\"");
            if (!actionNode.IsValid)
            {
                throw new FidelityException("Unable to find post action url for entryForm");
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (TextNode inputNode in formNode.ParseChildren("<input", ">"))
            {
                TextNode nameNode = new TextNode(inputNode, "name=\"", "\"");
                if (!nameNode.IsValid)
                {
                    nameNode = new TextNode(inputNode, "name='", "'");
                }
                if (nameNode.IsValid)
                {
                    TextNode valueNode = new TextNode(inputNode, "value=\"", "\"");
                    if (!valueNode.IsValid)
                    {
                        valueNode = new TextNode(inputNode, "value='", "'");
                    }

                    if (valueNode.IsValid)
                    {
                        parameters[nameNode.Value] = valueNode.Value;
                    }
                }
            }

            parameters["SYMBOL"] = symbol;

            switch (securityType)
            {
                case SecurityType.International:
                    parameters["ORDER_TYPE"] = "H";
                    break;
                case SecurityType.Option:
                    parameters["ORDER_TYPE"] = "O";
                    break;
                case SecurityType.MutualFund:
                    parameters["ORDER_TYPE"] = "M";
                    break;
                case SecurityType.FixedIncome:
                    parameters["ORDER_TYPE"] = "F";
                    break;
                case SecurityType.AfterHourStock:
                    parameters["ORDER_TYPE"] = "ECN";
                    break;
                case SecurityType.Currency:
                    parameters["ORDER_TYPE"] = "X";
                    break;
                case SecurityType.Basket:
                    parameters["ORDER_TYPE"] = "BT";
                    break;
                case SecurityType.StockETF:
                default:
                    parameters["ACCT_TYPE"] = marginTrade ? "M" : "C";
                    parameters["ORDER_TYPE"] = "E";
                    parameters["USE_VERIFY_IFRAME"] = "N";
                    break;
            }

            switch (tradeAction)
            {
                case TradeAction.Buy:
                    if (quantity > 0)
                    {
                        parameters["TRADE_TYPE"] = marginTrade ? "M" : "E";
                        parameters["TEMP_ORDER_ACTION"] = "B";
                        parameters["ORDER_ACTION"] = "B";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = quantity.ToString();
                    }
                    else
                    {
                        parameters["TRADE_TYPE"] = "S";
                        parameters["TEMP_ORDER_ACTION"] = "S";
                        parameters["ORDER_ACTION"] = "S";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = (-quantity).ToString();
                    }
                    break;
                case TradeAction.BuyToCover:
                    parameters["TRADE_TYPE"] = "S";
                    if (quantity > 0)
                    {
                        parameters["TEMP_ORDER_ACTION"] = "BH";
                        parameters["ORDER_ACTION"] = "BH";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = quantity.ToString();
                    }
                    else
                    {
                        parameters["TEMP_ORDER_ACTION"] = "SH";
                        parameters["ORDER_ACTION"] = "SH";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = (-quantity).ToString();
                    }
                    break;
                case TradeAction.Sell:
                    if (quantity > 0)
                    {
                        parameters["TRADE_TYPE"] = marginTrade ? "M" : "E";
                        parameters["TEMP_ORDER_ACTION"] = "S";
                        parameters["ORDER_ACTION"] = "S";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = quantity.ToString();
                    }
                    else
                    {
                        parameters["TRADE_TYPE"] = "S";
                        parameters["TEMP_ORDER_ACTION"] = "BH";
                        parameters["ORDER_ACTION"] = "BH";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = (-quantity).ToString();
                    }
                    break;
                case TradeAction.SellShort:
                    parameters["TRADE_TYPE"] = "S";
                    if (quantity > 0)
                    {
                        parameters["TEMP_ORDER_ACTION"] = "SH";
                        parameters["ORDER_ACTION"] = "SH";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = quantity.ToString();
                    }
                    else
                    {
                        parameters["TEMP_ORDER_ACTION"] = "BH";
                        parameters["ORDER_ACTION"] = "BH";
                        parameters["QTY_TYPE"] = "S";
                        parameters["QTY"] = (-quantity).ToString();
                    }
                    break;
                case TradeAction.SellAll:
                    parameters["TRADE_TYPE"] = marginTrade ? "M" : "E";
                    parameters["TEMP_ORDER_ACTION"] = "SA";
                    parameters["ORDER_ACTION"] = "S";
                    parameters["QTY_TYPE"] = "A";
                    break;
            }

            switch (orderType)
            {
                case OrderType.Market:
                    parameters["PRICE_TYPE"] = "M";
                    break;
                case OrderType.Limit:
                    parameters["PRICE_TYPE"] = "L";
                    if (double.IsNaN(limit))
                    {
                        return null;
                    }
                    parameters["AMOUNT"] = parameters["LIMIT_STOP_PRICE"] = Math.Round(limit, 2).ToString();
                    break;
                case OrderType.StopLoss:
                    parameters["PRICE_TYPE"] = "S";
                    if (double.IsNaN(stop))
                    {
                        return null;
                    }
                    parameters["AMOUNT"] = parameters["LIMIT_STOP_PRICE"] = stop.ToString();
                    break;
                case OrderType.StopLimit:
                    parameters["PRICE_TYPE"] = "SL";
                    if (double.IsNaN(stop))
                    {
                        return null;
                    }
                    parameters["STOP_PRICE"] = stop.ToString();
                    parameters["AMOUNT"] = parameters["LIMIT_STOP_PRICE"] = double.IsNaN(limit) ? Math.Round(stop, 2).ToString() : Math.Round(limit, 2).ToString();
                    break;
                case OrderType.TrailingStopLossDollar:
                    parameters["PRICE_TYPE"] = "TSD";
                    parameters["TRAILING_VALUE_IND"] = "D";
                    if (double.IsNaN(stop))
                    {
                        throw new FidelityException("Stop loss order requires stop price");
                    }
                    parameters["AMOUNT"] = parameters["TRAILING_VALUE"] = Math.Round(stop, 2).ToString();
                    break;
                case OrderType.TrailingStopLossPercent:
                    parameters["PRICE_TYPE"] = "TSP";
                    parameters["TRAILING_VALUE_IND"] = "P";
                    if (double.IsNaN(stop))
                    {
                        throw new FidelityException("Trailing stop loss order requires stop percentage");
                    }
                    else if (0 <= stop && stop < 1)
                    {
                        stop *= 100;
                    }
                    if (stop < 0 || stop > 100)
                    {
                        throw new FidelityException(string.Format("Invalid trailing stop loss percentage {0}", stop));
                    }
                    parameters["AMOUNT"] = parameters["TRAILING_VALUE"] = Math.Round(stop, 0).ToString();
                    break;
                case OrderType.TrailingStopLimitDollar:
                    parameters["PRICE_TYPE"] = "TLD";
                    parameters["TRAILING_VALUE_IND"] = "D";
                    if (double.IsNaN(stop))
                    {
                        throw new FidelityException("Trailing stop limit order requires stop price");
                    }
                    parameters["AMOUNT"] = parameters["TRAILING_VALUE"] = Math.Round(stop, 2).ToString();
                    break;
                case OrderType.TrailingStopLimitPercent:
                    parameters["PRICE_TYPE"] = "TLP";
                    parameters["TRAILING_VALUE_IND"] = "P";
                    if (double.IsNaN(stop))
                    {
                        throw new FidelityException("Trailing stop limit order requires stop percentage");
                    }
                    else if (0 <= stop && stop < 1)
                    {
                        stop *= 100;
                    }
                    if (stop < 0 || stop > 100)
                    {
                        throw new FidelityException(string.Format("Invalid trailing stop limit percentage {0}", stop));
                    }
                    parameters["AMOUNT"] = parameters["TRAILING_VALUE"] = Math.Round(stop, 0).ToString();
                    break;
            }

            if (orderType == OrderType.TrailingStopLimitDollar || orderType == OrderType.TrailingStopLimitPercent ||
                orderType == OrderType.TrailingStopLossDollar || orderType == OrderType.TrailingStopLossPercent)
            {
                switch (trailingValueType)
                {
                    case TrailingValueType.Last:
                        parameters["TRAILING_VALUE_TYPE"] = "T";
                        break;
                    case TrailingValueType.Bid:
                        parameters["TRAILING_VALUE_TYPE"] = "B";
                        break;
                    case TrailingValueType.Ask:
                        parameters["TRAILING_VALUE_TYPE"] = "A";
                        break;
                }
            }
            else
            {
                parameters["TRAILING_VALUE_TYPE"] = string.Empty;
            }

            switch (timeInForce)
            {
                case TimeInForce.Day:
                    parameters["TIME_IN_FORCE"] = "D";
                    break;
                case TimeInForce.FillOrKill:
                    parameters["TIME_IN_FORCE"] = "F";
                    break;
                case TimeInForce.ImmediateOrCancel:
                    parameters["TIME_IN_FORCE"] = "I";
                    break;
                case TimeInForce.OnOpen:
                    parameters["TIME_IN_FORCE"] = "O";
                    break;
                case TimeInForce.OnClose:
                    parameters["TIME_IN_FORCE"] = "C";
                    break;
                case TimeInForce.GoodUntilCancelled:
                    parameters["TIME_IN_FORCE"] = "G";
                    parameters["GTC_EXP_DATE"] = gtcExpDate == null ? DateTime.Today.AddDays(180).ToShortDateString() : gtcExpDate.Value.ToShortDateString();
                    parameters["GTC_EXP_ALERT_DATE"] = (gtcAlertDate == GTCAlertDate.EXP_ALERT_0 ? string.Empty : gtcAlertDate.ToString());
                    parameters["GTC_EXP_TIME"] = ((int)gtcExpTime).ToString();
                    break;
            }

            parameters["CONDITIONS"] = allOrNone ? "A" : string.Empty;
            parameters["SPECIFIC_SHARES"] = specificShares ? "Y" : "N";
            parameters["MONEY_TRANSFER"] = keepInAccount ? "BA" : "TP";
            parameters["SETTLE_CURR_CD"] = string.IsNullOrEmpty(settleCurrency) ? string.Empty : settleCurrency;

            if (skipOrderPreview)
            {
                parameters["SKIP_ORDER_PREVIEW"] = "Y";
                parameters["SOP"] = "on";
            }
            else
            {
                parameters["SS"] = "Y";
                parameters["SUBMIT_BUTTON"] = "Preview Order";
            }

            TextNode accountListNode = new TextNode(this.content, "<select name=\"accountList\"", "</select>");
            if (accountListNode.IsValid)
            {
                TextNode selectedOptionNode = new TextNode(accountListNode, "<option selected=\"selected\"", "</option>");
                if (selectedOptionNode.IsValid)
                {
                    TextNode valueNode = new TextNode(selectedOptionNode, "value=\"", "\"");
                    if (valueNode.IsValid)
                    {
                        parameters["accountList"] = valueNode.Value;
                    }
                }
            }
            parameters["INPUT_TIMESTAMP"] = OAuthBase.GenerateTimeStamp();

            return Link.Create(actionNode.Value, this.backUri, null, true, parameters);
        }

        public enum TradeAction
        {
            Buy,
            BuyToCover,
            Sell,
            SellShort,
            SellToCover,
            SellAll
        }

        public enum SecurityType
        {
            StockETF, // E
            International, // H
            MutualFund, // M
            Option, // O
            FixedIncome, // F
            Basket, // BT
            Currency, // X
            AfterHourStock, // ECN
        }

        public enum OrderType
        {
            Market,
            Limit,
            StopLoss,
            StopLimit,
            TrailingStopLossDollar,
            TrailingStopLossPercent,
            TrailingStopLimitDollar,
            TrailingStopLimitPercent
        }

        public enum TrailingValueType
        {
            Last,
            Bid,
            Ask
        }
        
        public enum TimeInForce
        {
            Day,
            GoodUntilCancelled,
            FillOrKill,
            ImmediateOrCancel,
            OnOpen,
            OnClose
        }

        public enum GTCAlertDate
        {
            EXP_ALERT_0,
            EXP_ALERT_1,
            EXP_ALERT_2,
            EXP_ALERT_3,
            EXP_ALERT_4,
            EXP_ALERT_5,
        }

        public enum GTCExpTime
        {
            TenAMET = 100000,
            TenThirtyAMET = 103000,
            ElevenAMET = 110000,
            ElevenThirtyAMET = 113000,
            TwelvePMET = 120000,
            TwelveThirtyPMET = 123000,
            OnePMET = 130000,
            OneThirtyPMET = 133000,
            TwoPMET = 140000,
            TwoThirtyPMET = 143000,
            ThreePMET = 150000,
            ThreeThirtyPMET = 153000,
            FourPMET = 160000,
        }
    }
}
