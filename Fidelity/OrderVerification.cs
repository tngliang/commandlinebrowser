﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    
    using CommandLineBrowser;

    [Title("Trade Stocks/ETFs - Order Verification: Fidelity Investments")]
    public class OrderVerification : TradeConfirmCommon
    {
        public OrderVerification(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public DateTime AsOfDate
        {
            get
            {
                return GetQuote<DateTime>("As of:", s => DateTime.Parse(s));
            }
        }

        public double Last
        {
            get
            {
                return GetQuote<double>("Last Price", s => s.AsPrice());
            }
        }

        public double Bid
        {
            get
            {
                return GetQuote<double>("Bid Price", s => s.AsPrice());
            }
        }

        public double Ask
        {
            get
            {
                return GetQuote<double>("Ask Price", s => s.AsPrice());
            }
        }

        public Link Confirm()
        {
            TextNode formNode = new TextNode(this.content, "<form name=\"trade_stock_verify\"", "</form>");
            if (!formNode.IsValid)
            {
                throw new Exception("Unable to find trade_stock_verify");
            }

            TextNode actionNode = new TextNode(formNode, "action=\"", "\"");
            if (!actionNode.IsValid)
            {
                throw new Exception("Unable to find post action url for trade_stock_verify");
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            foreach (TextNode inputNode in formNode.ParseChildren("<input", ">"))
            {
                TextNode nameNode = new TextNode(inputNode, "name=\"", "\"");
                if (!nameNode.IsValid)
                {
                    nameNode = new TextNode(inputNode, "name='", "'");
                }
                if (nameNode.IsValid)
                {
                    TextNode valueNode = new TextNode(inputNode, "value=\"", "\"");
                    if (!valueNode.IsValid)
                    {
                        valueNode = new TextNode(inputNode, "value='", "'");
                    }

                    if (valueNode.IsValid)
                    {
                        parameters[nameNode.Value] = valueNode.Value;
                    }
                }
            }

            parameters["INPUT_TIMESTAMP"] = OAuthBase.GenerateTimeStamp();

            return Link.Create(actionNode.Value, this.backUri, null, true, parameters);
        }

        private T GetQuote<T>(string quoteName, Func<string, T> valueParser)
        {
            return GetTableField<T>("Quote Information Table", quoteName, valueParser);
        }
    }
}
