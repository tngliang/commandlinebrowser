﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Trade Stocks/ETFs - Confirmation: Fidelity Investments")]
    public class TradeStockETFsConfirm : TradeConfirmCommon
    {
        public TradeStockETFsConfirm(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public string OrderConfirmationNumber
        {
            get
            {
                TextNode orderConfirmationNode = new TextNode(this.content, "Order Confirmation Number:", "Please");
                if (orderConfirmationNode.IsValid)
                {
                    return orderConfirmationNode.Value.Trim();
                }

                throw new FidelityException("Order Confirmation Number not found.");
            }
        }

        public Link OrderDetails()
        {
            foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
            {
                TextNode tableSummaryNode = new TextNode(tableNode, "summary=\"Link to the Order's page\"", string.Empty);
                if (!tableSummaryNode.IsValid)
                {
                    continue;
                }

                foreach (TextNode linkNode in tableSummaryNode.ParseChildren("<a", "</a>"))
                {
                    TextNode linkValueNode = new TextNode(linkNode, ">", string.Empty);
                    if (linkValueNode.IsValid && string.Compare(linkValueNode.Value.Trim(), "Orders", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        TextNode linkUrlNode = new TextNode(linkNode, "href=\"", "\"");
                        if (linkUrlNode.IsValid)
                        {
                            return Link.Create(linkUrlNode.Value, this.backUri, "Orders");
                        }
                    }
                }
            }

            throw new FidelityException("Orders link not found on confirmation page");
        }
    }
}
