﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using CommandLineBrowser;

    [Title("Portfolio Positions by Account: Fidelity Investments")]
    public class AccountPosition : Page
    {
        private static readonly char[] WhiteSpaces = new char[] { '\r', '\n', '\t', ' ' };
        private static string[] knownColumnNames = new string[]
            {
                "Symbol", "Description", "Quantity", "Curr", "Price", "Change", "Value", "Dollar", "Percent", "Dollar", "Percent", "Per Share", "Total"
            };

        public AccountPosition(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public SortedDictionary<string, Position> GetPositions()
        {
            SortedDictionary<string, Position> positions = new SortedDictionary<string, Position>(StringComparer.OrdinalIgnoreCase);
            foreach (Position position in ParsePositions())
            {
                positions[position.Symbol] = position;
            }
            return positions;
        }

        private IEnumerable<Position> ParsePositions()
        {
            TextNode tableNode = new TextNode(this.content, "<table id=\"positionsTable\"", "</table>");
            if (!tableNode.IsValid)
            {
                throw new Exception("\"<table id=\"positionsTable\" not found");
            }

            Dictionary<string, int> columns = new Dictionary<string,int>(StringComparer.OrdinalIgnoreCase);
            foreach (TextNode theadNode in tableNode.ParseChildren("<thead>", "</thead>"))
            {
                foreach (TextNode node in theadNode.ParseChildren("<th", "</th>"))
                {
                    foreach (TextNode contentNode in node.ParseChildren(">", "<"))
                    {
                        string colName = string.Join(" ", contentNode.Value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                        if (!string.IsNullOrEmpty(colName))
                        {
                            columns[colName] = -1;
                        }
                    }
                }
            }

            int index = 0;
            foreach (string colName in knownColumnNames)
            {
                if (columns.ContainsKey(colName))
                {
                    if (columns[colName] < 0)
                    {
                        columns[colName] = index;
                    }
                    ++index;
                }
            }

            foreach (TextNode tbodyNode in tableNode.ParseChildren("<tbody>", "</tbody>"))
            {
                foreach (TextNode rowNode in tbodyNode.ParseChildren("<tr", "</tr>"))
                {
                    Position position = new Position();

                    List<TextNode> colNodes = new List<TextNode>();
                    foreach (TextNode node in rowNode.ParseChildren("<td", "</td>"))
                    {
                        TextNode contentNode = new TextNode(node, ">", string.Empty);
                        if (contentNode.IsValid)
                        {
                            colNodes.Add(contentNode);
                        }
                    }

                    if (colNodes.Count > 0 && colNodes[0].IsValid)
                    {
                        position.Symbol = string.Join(" ", colNodes[0].Value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                    }
                    if (colNodes.Count > 1 && colNodes[1].IsValid)
                    {
                        TextNode node = new TextNode(colNodes[1], "id=\"fullDesc\"", string.Empty);
                        if (!node.IsValid)
                        {
                            node = new TextNode(colNodes[1], "id=\"shrtDesc\"", string.Empty);
                            if (!node.IsValid)
                            {
                                node = colNodes[1];
                            }
                        }

                        TextNode descNode = new TextNode(node, ">", "<");
                        if (descNode.IsValid)
                        {
                            position.Description = string.Join(" ", descNode.Value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                        }
                    }

                    string value;
                    if (TryGetValue(columns, colNodes, "Symbol", out value))
                    {
                        position.Symbol = string.Join(string.Empty, value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                    }
                    if (TryGetValue(columns, colNodes, "Description", out value))
                    {
                        position.Description = string.Join(" ", value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                    }
                    if (TryGetValue(columns, colNodes, "Curr", out value))
                    {
                        position.Currency = string.Join(" ", value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, WhiteSpaces));
                    }
                    if (TryGetValue(columns, colNodes, "Quantity", out value))
                    {
                        position.Quantity = value.As<double>(double.TryParse);
                    }
                    if (TryGetValue(columns, colNodes, "Price", out value))
                    {
                        position.Price = value.AsPrice();
                    }
                    if (TryGetValue(columns, colNodes, "Change", out value))
                    {
                        position.Change = value.AsPrice();
                    }     
                    if (TryGetValue(columns, colNodes, "Value", out value))
                    {
                        position.Value = value.AsPrice();
                    }
                    if (TryGetValue(columns, colNodes, "Dollar", out value))
                    {
                        position.ChangeSinceClose = value.AsPrice();
                    }
                    if (TryGetValue(columns, colNodes, "Percent", out value))
                    {
                        position.ChangeSinceClosePercent = value.AsPercentage();
                    }
                    if (TryGetValue(columns, colNodes, "Per Share", out value))
                    {
                        position.CostBasisPerShare = value.AsPrice();
                    } 
                    if (TryGetValue(columns, colNodes, "Total", out value))
                    {
                        position.CostBasis = value.AsPrice();
                    } 
                    yield return position;
                }
            }
        }

        private static bool TryGetValue(Dictionary<string, int> columns, List<TextNode> colNodes, string columnName, out string value)
        {
            if (columns.ContainsKey(columnName) && columns[columnName] >= 0 && colNodes.Count > columns[columnName] && colNodes[columns[columnName]].IsValid)
            {
                value = colNodes[columns[columnName]].Value.Trim();
                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }
    }
}
