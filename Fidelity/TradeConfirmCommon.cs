﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public class TradeConfirmCommon : Page
    {
        public TradeConfirmCommon(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public string Account
        {
            get
            {
                return GetDetails<string>("Account", s => s);
            }
        }

        public string Symbol
        {
            get
            {
                return GetDetails<string>("Symbol", s => s);
            }
        }

        public string Description
        {
            get
            {
                return GetDetails<string>("Description", s => s);
            }
        }

        public string Action
        {
            get
            {
                return GetDetails<string>("Action", s => s);
            }
        }

        public string Quantity
        {
            get
            {
                return GetDetails<string>("Quantity", s => s);
            }
        }

        public string OrderType
        {
            get
            {
                return GetDetails<string>("Order Type", s => s);
            }
        }

        public string TimeInForce
        {
            get
            {
                return GetDetails<string>("Time in Force", s => s);
            }
        }

        public string OrderExpiration
        {
            get
            {
                return GetDetails<string>("Order Expiration", s => s);
            }
        }

        public string Conditions
        {
            get
            {
                return GetDetails<string>("Conditions", s => s);
            }
        }

        public string TradeType
        {
            get
            {
                return GetDetails<string>("Trade Type", s => s);
            }
        }

        public double EstimatedValue
        {
            get
            {
                return GetEstimated("Estimated Order Value:");
            }
        }

        public double EstimatedCommission
        {
            get
            {
                return GetEstimated("Estimated Commission:");
            }
        }

        public double EstimatedValueAndCommission
        {
            get
            {
                return GetEstimated("Estimated Order Value (including commission):");
            }
        }

        protected double GetEstimated(string valueName)
        {
            foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
            {
                TextNode tableSummaryNode = new TextNode(tableNode, "summary=\"Estimated values and commissions for this trade\"", string.Empty);
                if (!tableSummaryNode.IsValid)
                {
                    continue;
                }

                TextNode estimatedNode = new TextNode(tableSummaryNode, valueName, string.Empty);
                if (!estimatedNode.IsValid)
                {
                    throw new FidelityException(string.Format("Unable to find {0}", valueName));
                }

                TextNode cellNode = new TextNode(estimatedNode, "<td", "</td>");
                if (!cellNode.IsValid)
                {
                    throw new FidelityException(string.Format("Unable to find {0} cell", valueName));
                }

                TextNode valueNode = new TextNode(cellNode, ">", string.Empty);
                if (!valueNode.IsValid)
                {
                    throw new FidelityException(string.Format("Unable to find {0} value", valueName));
                }

                return valueNode.Value.AsPrice();
            }

            throw new FidelityException("Unable to find Estimated values and commissions for this trade");
        }

        protected T GetDetails<T>(string fieldName, Func<string, T> valueParser)
        {
            return GetTableField<T>("Details of this Order", fieldName, valueParser);
        }

        protected T GetTableField<T>(string tableName, string fieldName, Func<string, T> valueParser)
        {
            foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
            {
                TextNode tableInfoNode = new TextNode(tableNode, string.Format("summary=\"{0}\"", tableName), string.Empty);
                if (!tableInfoNode.IsValid)
                {
                    continue;
                }

                TextNode tableBodyNode = new TextNode(tableInfoNode, "<tbody>", "</tbody>");
                if (!tableBodyNode.IsValid)
                {
                    tableBodyNode = tableInfoNode;
                }

                foreach (TextNode headerNode in tableBodyNode.ParseChildren("<th", "</th>"))
                {
                    TextNode headerValueNode = new TextNode(headerNode, ">", string.Empty);
                    if (!headerValueNode.IsValid ||
                        string.Compare(headerValueNode.Value.Trim(), fieldName, StringComparison.OrdinalIgnoreCase) != 0)
                    {
                        continue;
                    }

                    TextNode cellNode = tableInfoNode.ParseNextChild(headerNode, "<td", "</td>");
                    if (cellNode.IsValid)
                    {
                        TextNode valueNode = new TextNode(cellNode, ">", string.Empty);
                        if (valueNode.IsValid)
                        {
                            return valueParser(valueNode.Value.Trim());
                        }
                    }
                }

                foreach (TextNode rowNode in tableBodyNode.ParseChildren("<tr", "</tr>"))
                {
                    foreach (TextNode cellNode in rowNode.ParseChildren("<td", "</td>"))
                    {
                        TextNode cellNameNode = new TextNode(cellNode, fieldName, string.Empty);
                        if (cellNameNode.IsValid)
                        {
                            TextNode valueNode = new TextNode(cellNode, ">", string.Empty);
                            if (valueNode.IsValid)
                            {
                                return valueParser(valueNode.Value.Trim());
                            }
                        }
                    }
                }
            }

            throw new FidelityException(string.Format("Unable to find {0} in {1} Table", fieldName, tableName));
        }
    }
}
