﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public class Stock : IEquatable<Stock>
    {
        private string symbol;
        private string equityClass;

        public Stock(string symbol, string equityClass = "Common")
        {
            this.symbol = symbol;
            this.equityClass = equityClass;
        }

        public override bool Equals(object obj)
        {
            return ((IEquatable<StockOption>)this).Equals(obj as StockOption);
        }

        public override int GetHashCode()
        {
            return HashFunction.GetHashCode(this.symbol.ToUpperInvariant(), this.equityClass.ToLowerInvariant());
        }

        bool IEquatable<Stock>.Equals(Stock other)
        {
            if (this == null && other == null)
            {
                return true;
            }

            if (other == null)
            {
                return false;
            }

            return
                string.Compare(this.symbol, other.symbol, StringComparison.OrdinalIgnoreCase) == 0 &&
                string.Compare(this.equityClass, other.equityClass, StringComparison.OrdinalIgnoreCase) == 0;
        }

        public string Symbol
        {
            get
            {
                return this.symbol;
            }
        }

        public string EquityClass
        {
            get
            {
                return this.equityClass;
            }
        }

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public double Bid { get; set; }
        public double Ask { get; set; }
        public double IV30 { get; set; }
        public double IV60 { get; set; }
        public double IV90 { get; set; }
        public double PriceToEarnings { get; set; } 
        public string Sector { get; set; }
        public string Industry { get; set; }
    }

    public class StockOption : IEquatable<StockOption>
    {
        private Stock underlying;
        private OptionType type;
        private double strike;
        private DateTime expiration;
        private int quantity;

        public StockOption(Stock underlying, OptionType type, double strike, DateTime expiration, int quantity = 100)
        {
            this.underlying = underlying;
            this.type = type;
            this.strike = strike;
            this.expiration = expiration;
            this.quantity = quantity;
        }

        public override bool Equals(object obj)
        {
            return ((IEquatable<StockOption>)this).Equals(obj as StockOption);
        }

        public override int GetHashCode()
        {
            return HashFunction.GetHashCode(this.type, this.expiration, this.strike, this.quantity, this.underlying);
        }

        bool IEquatable<StockOption>.Equals(StockOption other)
        {
            if (this == null && other == null)
            {
                return true;
            }

            if (other == null)
            {
                return false;
            }

            return
                this.type == other.type &&
                this.expiration == other.expiration &&
                this.strike == other.strike &&
                this.quantity == other.quantity &&
                this.underlying == other.underlying;
        }

        public OptionType Type
        {
            get
            {
                return this.type;
            }
        }

        public DateTime Expiration
        {
            get
            {
                return this.expiration;
            }
        }

        public double Strike
        {
            get
            {
                return this.strike;
            }
        }

        public int Quantity
        {
            get
            {
                return this.quantity;
            }
        }

        public Stock Underlying
        {
            get
            {
                return this.underlying;
            }
        }

        public string Symbol { get; set; }
        public double Last { get; set; }
        public double Change { get; set; }
        public double Bid { get; set; }
        public double Ask { get; set; }
        public int Volume { get; set; }
        public int OpenInterest { get; set; }
        public double ImpliedVolatility { get; set; }
        public double Delta { get; set; }
        public double Gamma { get; set; }
        public double Theta { get; set; }
        public double Vega { get; set; }
        public double Rho { get; set; }
    }

    public enum OptionType
    {
        Call = 0,
        Put
    }
}
