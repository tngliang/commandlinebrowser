﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public class ActiveQuote
    {
        private Fidelity fidelity;
        public ActiveQuote(string symbol, Session session)
        {
            this.fidelity = (Fidelity)session.Navigate(Link.Create(string.Format("http://activequote.fidelity.com/webxpress/get_quote?QUOTE_TYPE=R&SID_VALUE_ID={0}&submit=Quote", symbol)));

            Link loginForRealTimeQuote;
            if (this.fidelity.Links.TryGetValue("Log in for Real-Time Quotes", out loginForRealTimeQuote))
            {
                LogIntoFidelity login = (LogIntoFidelity)session.Navigate(loginForRealTimeQuote);
                Link loginLink = login.FindLogin(session.UserName, session.Password);
                this.fidelity = (Fidelity)session.Navigate(loginLink);
            }
        }

        public object this[string name]
        {
            get
            {
                return this.fidelity.Quotes[name];
            }
        }

        public IDictionary<string, Link> Links
        {
            get
            {
                return this.fidelity.Links;
            }
        }

        public IDictionary<string, object> Quotes
        {
            get
            {
                return this.fidelity.Quotes;
            }
        }
    }
}
