﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public class OrderStatus
    {
        private Dictionary<string, Link> Links = new Dictionary<string, Link>(StringComparer.OrdinalIgnoreCase);
 
        public string OrderNumber { get; set; }
        public string Description { get; set; }
        public bool Open { get; set; }
        public bool Filled { get; set; }
        public bool VerifyCancelled { get; set; }

        public void SetValues(string content)
        {
            TextNode orderNumberNode = new TextNode(content, "Order Number:", string.Empty);
            if (orderNumberNode.IsValid)
            {
                this.OrderNumber = orderNumberNode.Value;
            }

            TextNode verifyCancelledNode = new TextNode(content, "Verified Canceled", string.Empty);
            if (verifyCancelledNode.IsValid)
            {
                this.VerifyCancelled = true;
            }

            TextNode filledNode = new TextNode(content, "Filled", string.Empty);
            if (filledNode.IsValid)
            {
                this.Filled = true;
            }

            TextNode openNode = new TextNode(content, "Open", string.Empty);
            if (openNode.IsValid)
            {
                this.Open = true;
            }
        }

        public Link this[string linkName]
        {
            get
            {
                return this.Links[linkName];
            }
            set
            {
                this.Links[linkName] = value;
            }
        }

        public bool ContainsLink(string name)
        {
            return this.Links.ContainsKey(name);
        }

        public void AddLink(Link link)
        {
            if (link.Name.IndexOf("quote", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                this.Links["Quote"] = link;
            }
            else if (link.Name.IndexOf("replace", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                this.Links["Replace"] = link;
            }
            else if (link.Name.IndexOf("cancel", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                this.Links["Cancel"] = link;
            }
            else
            {
                this.Links[link.Name] = link;
            }
        }
    }
}
