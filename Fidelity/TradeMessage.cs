﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    [Title("Trade: Message: Fidelity Investments")]
    public class TradeMessage : Page
    {
        public TradeMessage(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public string Message
        {
            get
            {
                foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
                {
                    TextNode summaryNode = new TextNode(tableNode, "summary=\"", "\"");
                    if (summaryNode.IsValid && string.Compare(summaryNode.Value, "Error Message", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        foreach (TextNode cellNode in tableNode.ParseChildren("<td", "</td>"))
                        {
                            TextNode ofMessageNode = new TextNode(cellNode, "class=\"", "\"");
                            if (ofMessageNode.IsValid && string.Compare(ofMessageNode.Value, "ofMessage") == 0)
                            {
                                TextNode messageNode = cellNode.ParseNextChild(ofMessageNode, ">", string.Empty);
                                if (messageNode.IsValid)
                                {
                                    return messageNode.Value.Trim();
                                }
                            }
                        }
                    }
                }

                throw new FidelityException("Error message not found.");
            }
        }
    }
}
