﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    using CommandLineBrowser;

    public class Position
    {
        private Dictionary<string, Link> Actions = new Dictionary<string, Link>(StringComparer.OrdinalIgnoreCase);

        public string Symbol { get; set; }
        public string Description { get; set; }
        public double Quantity { get; set; }
        public string Currency { get; set; }
        public double Price { get; set; }
        public double Change { get; set; }
        public double Value { get; set; }
        public double ChangeSinceClose { get; set; }
        public double ChangeSinceClosePercent { get; set; }
        public double ChangeSincePurchase { get; set; }
        public double ChangeSincePurchasePercent { get; set; }
        public double CostBasisPerShare { get; set; }
        public double CostBasis { get; set; }

        public Link this[string linkName]
        {
            get
            {
                return this.Actions[linkName];
            }
            set
            {
                this.Actions[linkName] = value;
            }
        }
    }

}
