﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;

    [Title("Trade Stocks - International: Fidelity Investments")]
    public class TradeInternational : Page
    {
        public TradeInternational(Uri backUri, string content)
            : base(backUri, content)
        {
        }
    }
}
