﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using CommandLineBrowser;
    
    [Title("Order Details: Fidelity Investments")]
    public class OrderDetails : Page
    {
        public OrderDetails(Uri backUri, string content)
            : base(backUri, content)
        {
        }

        public List<OrderStatus> OrderStatuses
        {
            get
            {
                List<OrderStatus> orderStatuses = new List<OrderStatus>();

                foreach (TextNode tableNode in new TextNode(this.content, string.Empty, string.Empty).ParseChildren("<table", "</table>"))
                {
                    TextNode tableSummaryNode = new TextNode(tableNode, "summary=\"Summary of Brokerage Order Status\"", string.Empty);
                    if (!tableSummaryNode.IsValid)
                    {
                        continue;
                    }

                    TextNode tableBodyNode = new TextNode(tableSummaryNode, "<tbody>", "</tbody>");
                    if (!tableBodyNode.IsValid)
                    {
                        tableBodyNode = tableSummaryNode;
                    }

                    foreach (TextNode rowNode in tableBodyNode.ParseChildren("<tr", "</tr>"))
                    {
                        OrderStatus status = new OrderStatus();
                        foreach (TextNode cellNode in rowNode.ParseChildren("<td", "</td>"))
                        {
                            TextNode valueNode = new TextNode(cellNode, ">", string.Empty);
                            if (valueNode.IsValid)
                            {
                                status.SetValues(string.Join(" ", valueNode.Value.SplitAndTrim(StringSplitOptions.RemoveEmptyEntries, ' ', '\t', '\n', '\r')));
                            }

                            foreach (TextNode aLinkNode in cellNode.ParseChildren("<a", "</a>"))
                            {
                                TextNode popupQuoteLinkNode = new TextNode(aLinkNode, "javascript:popWin('", "')");
                                if (popupQuoteLinkNode.IsValid)
                                {
                                    TextNode linkTitleNode = new TextNode(aLinkNode, "title=\"", "\"");
                                    TextNode linkNameNode = new TextNode(aLinkNode, ">", string.Empty);
                                    status.AddLink(Link.Create(popupQuoteLinkNode.Value, this.backUri,
                                        linkTitleNode.IsValid ?
                                            linkTitleNode.Value :
                                            linkNameNode.IsValid ?
                                                linkNameNode.Value :
                                                string.Empty));
                                }
                                else
                                {
                                    TextNode timestampedLinkNode = new TextNode(aLinkNode, "setTimeStamp('", "')");
                                    if (timestampedLinkNode.IsValid)
                                    {
                                        TextNode linkTitleNode = new TextNode(aLinkNode, "title=\"", "\"");
                                        TextNode linkNameNode = new TextNode(aLinkNode, ">", string.Empty);
                                        status.AddLink(Link.Create(
                                            string.Format("/ftgw/fbc/oftrade/{0}&INPUT_TIMESTAMP={1}",
                                                timestampedLinkNode.Value,
                                                OAuthBase.GenerateTimeStamp()),
                                            this.backUri,
                                            linkTitleNode.IsValid ?
                                                linkTitleNode.Value :
                                                linkNameNode.IsValid ?
                                                    linkNameNode.Value :
                                                    string.Empty));
                                    }
                                    else
                                    {
                                        TextNode hrefNode = new TextNode(aLinkNode, "href=\"", "\"");
                                        if (hrefNode.IsValid)
                                        {
                                            string url = hrefNode.Value;
                                            if (!string.IsNullOrEmpty(url) && !url.StartsWith("#"))
                                            {
                                                TextNode linkTitleNode = new TextNode(aLinkNode, "title=\"", "\"");
                                                TextNode linkNameNode = new TextNode(aLinkNode, ">", string.Empty);
                                                status.AddLink(Link.Create(
                                                    string.Format("/ftgw/fbc/oftrade/{0}", url),
                                                    this.backUri,
                                                    linkTitleNode.IsValid ?
                                                    linkTitleNode.Value :
                                                    linkNameNode.IsValid ?
                                                        linkNameNode.Value :
                                                        string.Empty));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(status.OrderNumber))
                        {
                            orderStatuses.Add(status);
                        }
                    }
                }

                return orderStatuses;
            }
        }
    }
}
