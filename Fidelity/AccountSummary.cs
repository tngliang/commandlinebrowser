﻿namespace Fidelity
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;

    using CommandLineBrowser;

    [Title("Portfolio Summary: Fidelity Investments")]
    public class AccountSummary : Page
    {
        private Dictionary<string, IDictionary<string, Account>> accounts;

        public AccountSummary(Uri backUri, string content)
            : base(backUri, content)
        {
            this.accounts = new Dictionary<string, IDictionary<string,Account>>(StringComparer.OrdinalIgnoreCase);
        }

        public IDictionary<string, Account> InvestmentAccounts
        {
            get
            {
                return GetAccounts("Investment Accounts");
            }
        }

        public IDictionary<string, Account> RetirementAccounts
        {
            get
            {
                return GetAccounts("Retirement Accounts");
            }
        }

        public IDictionary<string, Account> EducationAccounts
        {
            get
            {
                return GetAccounts("Education Accounts");
            }
        }

        public IDictionary<string, Account> StockPlanAccounts
        {
            get
            {
                return GetAccounts("Stock Plans");
            }
        }

        private IDictionary<string, Account> GetAccounts(string accountType)
        {
            IDictionary<string, Account> accountCollection;
            if (!this.accounts.TryGetValue(accountType, out accountCollection))
            {
                accountCollection = new Dictionary<string, Account>(StringComparer.OrdinalIgnoreCase);
                foreach (Account account in ParseAccounts(accountType))
                {
                    accountCollection.Add(account.Name, account);
                }
                accounts[accountType] = accountCollection;
            }
            return accountCollection;
        }
        
        private IEnumerable<Account> ParseAccounts(string accountType)
        {
            TextNode root = new TextNode(this.content, string.Empty, string.Empty);
            TextNode node = new TextNode(root, accountType, "</tr>");
            if (!node.IsValid)
            {
                yield break;
            }

            Account account = null;
            for (TextNode accountNode = root.ParseNextChild(node, "<tr>", "</tr>");
                accountNode.IsValid && TryParseAccount(accountNode, out account);
                accountNode = root.ParseNextChild(accountNode, "<tr>", "</tr>"))
            {
                yield return account;
            }
        }

        private bool TryParseAccount(TextNode accountNode, out Account account)
        {
            account = new Account();
            TextNode accountPositionLinkNode = new TextNode(accountNode, "<a", "</a>");
            if (!accountPositionLinkNode.IsValid)
            {
                return false;
            }

            TextNode hRefNode = new TextNode(accountPositionLinkNode, "href=\"", "\"");
            if (!hRefNode.IsValid)
            {
                return false;
            }

            TextNode accountNumberNode = new TextNode(hRefNode, "ACCOUNT=", "&amp;");
            if (!accountNumberNode.IsValid)
            {
                accountNumberNode = new TextNode(hRefNode, "ACCOUNT=", string.Empty);
            }

            if (!accountNumberNode.IsValid)
            {
                return false;
            }

            account.Number = accountNumberNode.Value;

            TextNode accountNameNode = new TextNode(accountPositionLinkNode, ">", string.Empty);
            if (!accountNameNode.IsValid)
            {
                return false;
            }

            account.Name = accountNameNode.Value;
            Link link = ParseAccountLink(hRefNode);
            if (link != null)
            {
                account.AddLink(link);
            }

            TextNode accountTypeNode = new TextNode(accountNode, "class=\"account-name\">", "</");
            if (accountTypeNode.IsValid)
            {
                TextNode typeNode = new TextNode(accountTypeNode.Value, string.Empty, ":");
                if (typeNode.IsValid)
                {
                    account.Type = typeNode.Value;
                }
                else
                {
                    account.Type = accountTypeNode.Value;
                }
            }

            for (TextNode linkNode = accountNode.ParseNextChild(accountPositionLinkNode, "<a", "</a>");
                linkNode.IsValid;
                linkNode = accountNode.ParseNextChild(linkNode, "<a", "</a>"))
            {
                link = ParseAccountLink(new TextNode(linkNode, "href=\"", "\""));
                if (link != null)
                {
                    account.AddLink(link);
                }
            }

            return true;
        }

        private Link ParseAccountLink(TextNode hRefNode)
        {
            if (hRefNode.IsValid)
            {
                string url = hRefNode.Value;
                Match matchName = new Regex(@"\/([a-z|A-Z|.]+)\?").Match(url);
                if (matchName.Success)
                {
                    return Link.Create(url, this.backUri, matchName.Groups[1].Value);
                }
            }

            return null;
        }
    }
}
